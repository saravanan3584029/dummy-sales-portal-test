export { default as globalThemeConfig } from "./themeConfig";
export { default as globalSeoConfig } from "./seoConfig";
export { default as globalAppConfig } from "./appConfig";
