import { COLOR } from "@/constants/theme";
import { ThemeConfig } from "./types";

const footerContent = `
        Maecenas facilisis sodales magna eu eleifend. 
        In eget enim vel tellus bibendum congue a in risus.          
        Mauris a sollicitudin quam. In sit amet suscipit mauris.
        Morbi tincidunt viverra nisi, et bibendum risus consectetur vitae. 
        `;

const themeConfig: ThemeConfig = {
  logo: {
    src: "/images/wit-logo.png",
    alt: "logo",
    size: { width: "8.75rem", height: "6.25rem" },
  },
  footer: {
    content: footerContent,
    style: {
      "footer-md-width": "50%",
    },
  },
  style: {
    colors: {
      "text-primary": COLOR.RED,
    },
    fontSize: {
      "2xs": "0.512rem", // 8.19px
      xs: "0.64rem", // 10.24px
      sm: "0.8rem", // 12.80px
      m: "0.875rem", //14px
      base: "1rem", // 16px
      lg: "1.25rem", // 20px
      xl: "1.563rem", // 25px
      "2xl": "1.953rem", // 31.25px
    },
    lineHeight: {
      "2xs": "0.687rem", // 11px
      xs: "0.875rem", // 14px
      sm: "1.125rem", // 18px
      m: "1.25rem", // 20px
      base: "1.437rem", // 23px
      lg: "1.75rem", // 28px
      xl: "2.187rem", // 35px
      "2xl": "2.75rem", // 44px
    },
    fontFamily: {
      "family-Greycliff": "Greycliff CF",
      "family-Inter": "Inter",
    },
  },
};
export default themeConfig;
