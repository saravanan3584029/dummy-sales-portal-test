import { globalThemeConfig, globalSeoConfig } from "@/config/index";
import { AppConfig } from "@/config/types";

const appConfig: AppConfig = {
  themeConfig: globalThemeConfig,
  seoConfig: globalSeoConfig,
};

export default appConfig;
