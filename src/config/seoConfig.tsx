import { SeoConfig } from "@/config/types";

export const seoConfig: SeoConfig = {
  title: "Sales portal",
  titleTemplate: "%s | WitArist",
  additionalMetaTags: [
    {
      name: "keywords",
      content: "",
    },
  ],
  description: "",
  canonical: "",
};

export default seoConfig;
