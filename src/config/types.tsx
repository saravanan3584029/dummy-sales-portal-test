export type ThemeConfig = {
  logo: Logo;
  footer: Footer;
  style: any;
};
export interface Logo {
  src: string;
  alt: string;
  size: LogoSize;
  style?: string;
}

export interface LogoSize {
  width: string;
  height?: string;
}

export interface Footer {
  content: string;
  style?: Record<string, string>;
}

export type SeoConfig = {
  title?: string;
  titleTemplate?: string;
  additionalMetaTags?: any;
  description?: string;
  canonical?: string;
};
export interface Route {
  id: number;
  link?: string;
  label: string;
  subNav?: Array<Route>;
  icon?: string;
}

export type Routes = {
  routes: Array<Route>;
};

export type AppConfig = {
  themeConfig: ThemeConfig;
  seoConfig: SeoConfig;
  routeConfig?: Routes;
};

export type ButtonProps = {
  type?: "button" | "submit" | "reset";
  disabled?: boolean;
  size?: "small" | "medium" | "large";
  variant?: "primary" | "secondary" | "tertiary" | "link" | "disable";
  className?: string;
  onClick?: (...args: unknown[]) => unknown;
  children: React.ReactNode | string;
  precedingText?: string;
  loading?: boolean;
  icon?: string;
  visible?: boolean;
};

export interface PopupData {
  title: string;
  description: JSX.Element | string;
  content?: any;
  btnArray: ButtonProps[];
  deviceIcon?: string;
  classAdditions?: any;
  head?: any;
  image?: string;
}

export type CardProps = {
  noBorder?: Boolean;
  data: [] | {};
  config: any;
  gridCols?: Number;
  columns?: Number;
  cardContainer?: any;
  className?: string;
  showDetailsIcon?: Boolean;
  showPopup?: any;
  detailsPopup?: any;
  showDetailView?: any;
  showCheckBox?: boolean;
  cardStyle?:string;
  selectData?: any;
  checkedData?: any;
};
