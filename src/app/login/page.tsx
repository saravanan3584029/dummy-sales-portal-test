"use client"; // pages/login.js
import { useState } from "react";
import { useRouter } from "next/navigation";
import { useDispatch } from "react-redux";
import { SetUserName, SetAuth, SetUserId } from "@/store/user";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const router = useRouter();
  const dispatch = useDispatch();

  const handleLogin = async (e: any) => {
    e.preventDefault();
    try {
      const response = await fetch("/api/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email, password }),
      });
      if (response.status === 200) {
        const data = await response.json();
        if (data.success) {
          localStorage.setItem("user_name", data.user.name);
          localStorage.setItem("user_role", data.user.userType);
          localStorage.setItem("user_id", data.user._id);
          // dispatch(SetUserName(data.user.name));
          // dispatch(SetUserId(data.user._id));
          // dispatch(SetAuth(true));
          if (data.user.userType === "admin") {
            router.push("/dashboard");
          } else {
            router.push("/vendors");
          }
        } else {
          setError(data.unAuthorized);
        }
      } else {
        setError("Invalid credentials. Please try again.");
      }
    } catch (error) {
      setError("An error occurred. Please try again.");
    }
  };

  return (
    <div className="min-h-screen w-full h-full flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded shadow-md m-auto">
        <h2 className="text-2xl font-semibold mb-4 text-red-900">Login</h2>
        {error && <div className="text-red-500 mb-4">{error}</div>}
        <form onSubmit={handleLogin}>
          <div className="mb-4">
            <label
              htmlFor="email"
              className="block text-sm font-medium text-gray-600"
            >
              Email
            </label>
            <input
              type="email"
              id="email"
              className="text-gray-500 w-full p-2 border rounded"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="password"
              className="block text-sm font-medium text-gray-600"
            >
              Password
            </label>
            <input
              type="password"
              id="password"
              className="text-gray-500 w-full p-2 border rounded"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <button
            type="submit"
            className="w-full bg-blue-500 text-white py-2 rounded hover:bg-blue-600"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
}
