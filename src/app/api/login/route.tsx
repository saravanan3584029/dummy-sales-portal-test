import { NextRequest,NextResponse } from "next/server";
import { connect } from "@/dbConfig/dbConfig";
import User from "@/server/models/userModel";



connect();
// pages/api/login.js
export  async function POST(req:NextRequest, res:NextResponse)  {
    const reqBody = await req.json();
    const { email, password  } = reqBody;
    try {
      
      const user = await User.findOne({ email });
  
      if (!user) {
        return NextResponse.json({
          success: false,
          message: "Exist",
          unAuthorized: "User does not exist! Please register first.",
        });
      }
      if(user.password !== password){
        return NextResponse.json({
          success: false,
          message: "Password",
          unAuthorized: "Password is incorrect!",
        });
      }
      return NextResponse.json({
        message: "Login successful",
        success: true,
      user:user
      });;
    } catch (error: any) {
      return NextResponse.json({ error: "Internal Server Error" }, { status: 500 });
    }
    
  };
  