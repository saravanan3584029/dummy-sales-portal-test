import { NextRequest, NextResponse } from "next/server";
import { connect } from "@/dbConfig/dbConfig";
import mongoose from "mongoose";
import FileUpload from "@/server/models/fileUpload";
import { ERROR_MESSAGE } from "@/constants/errorMessage";
import { getContentTypeFromExtension } from "@/server/common";

connect();

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const { id } = await req.json();
    if (!id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.FILEID_REQUIRED,
      });
    }
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);

    if (Array.isArray(id)) {
      const files = await FileUpload.find({ _id: { $in: id } });
      if (files.length === 0) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }

      const filesData = [];
      for (let i = 0; i < files.length; i++) {
        const fileStream = gfs.openDownloadStream(
          new mongoose.Types.ObjectId(files[i].fileReference)
        );
        const extension = files[i].filename.split(".").pop();
        const contentType = getContentTypeFromExtension(extension);
        const fileBuffer: any = await streamToBuffer(fileStream);
        const base64Content = fileBuffer.toString("base64");

        filesData.push({
          content: base64Content,
          contentType: contentType,
          filename: files[i].filename,
        });
      }

      return NextResponse.json({ files: filesData });
    } else {
      const file = await FileUpload.findOne({ _id: id });
      if (!file) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }

      const fileStream: any = gfs.openDownloadStream(
        new mongoose.Types.ObjectId(file.fileReference)
      );
      const extension = file.filename.split(".").pop();
      const contentType = getContentTypeFromExtension(extension);
      const headers: any = {
        "Content-Type": contentType,
        "Content-Disposition": `${file.filename}`,
        "Content-Length": file.size,
      };
      // Create the response with headers
      const response = new NextResponse(fileStream, { headers });

      return response;
    }
  } catch (error: unknown) {
    return NextResponse.json({ error, status: 500 });
  }
}

function streamToBuffer(stream: any) {
  return new Promise((resolve, reject) => {
    const chunks: any = [];
    stream.on("data", (chunk: any) => chunks.push(chunk));
    stream.on("end", () => resolve(Buffer.concat(chunks)));
    stream.on("error", reject);
  });
}
