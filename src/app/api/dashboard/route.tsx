import { NextResponse, NextRequest } from "next/server";
import Client from "@/server/models/client";
import Vendor from "@/server/models/vendor";
import User from "@/server/models/userModel";
import type { NextApiRequest, NextApiResponse } from "next";
import Resource from "@/server/models/resource";
import Requirement from "@/server/models/requirement";

export async function GET(request: NextRequest, response: NextResponse) {
  try {
    const totalVendors = await Vendor.countDocuments().exec();
    const totalClients = await Client.countDocuments().exec();
    const totalUsers = await User.countDocuments().exec();
    const totalResource = await Resource.countDocuments().exec();
    const totalRequirement = await Requirement.countDocuments().exec();

    const totalCount = {
      vendor: totalVendors,
      client: totalClients,
      resource: totalResource,
      requirement: totalRequirement,
      user: totalUsers,
    };

    const dateToCheck = new Date(Date.now());
    const startDate = new Date(dateToCheck);
    startDate.setHours(0, 0, 0, 0);
    const endDate = new Date(dateToCheck);
    endDate.setHours(23, 59, 59, 999);

    const userSummary = await Promise.all([
      Vendor.aggregate([
        {
          $match: {
            createdAt: {
              $gte: startDate,
              $lte: endDate,
            },
          },
        },
        {
          $group: {
            _id: "$accountManager",
            vendorsCount: { $sum: 1 },
          },
        },
      ]).exec(),
      Client.aggregate([
        {
          $match: {
            createdAt: {
              $gte: startDate,
              $lte: endDate,
            },
          },
        },
        {
          $group: {
            _id: "$accountManager",
            clientsCount: { $sum: 1 },
          },
        },
      ]).exec(),

      Resource.aggregate([
        {
          $match: {
            createdAt: {
              $gte: startDate,
              $lte: endDate,
            },
          },
        },
        {
          $group: {
            _id: "$accountManager",
            resourcesCount: { $sum: 1 },
          },
        },
      ]).exec(),
      Requirement.aggregate([
        {
          $match: {
            createdAt: {
              $gte: startDate,
              $lte: endDate,
            },
          },
        },
        {
          $group: {
            _id: "$accountManager",
            requirementCount: { $sum: 1 },
          },
        },
      ]).exec(),

      User.find({}, { name: 1 }).exec(), // Fetch all users with only the name field
    ]);

    const combinedSummary: any = {};
    userSummary[0].forEach((entry: { _id: string; vendorsCount: number }) => {
      combinedSummary[entry._id] = {
        ...combinedSummary[entry._id],
        vendorsCount: entry.vendorsCount || 0,
      };
    });
    userSummary[1].forEach((entry: { _id: string; clientsCount: number }) => {
      combinedSummary[entry._id] = {
        ...combinedSummary[entry._id],
        clientsCount: entry.clientsCount || 0,
      };
    });
    userSummary[2].forEach((entry: { _id: string; resourcesCount: number }) => {
      combinedSummary[entry._id] = {
        ...combinedSummary[entry._id],
        resourceCount: entry.resourcesCount || 0,
      };
    });
    userSummary[3].forEach(
      (entry: { _id: string; requirementCount: number }) => {
        combinedSummary[entry._id] = {
          ...combinedSummary[entry._id],
          requirementCount: entry.requirementCount || 0,
        };
      }
    );

    //Add usernames to the summary
    userSummary[4].forEach((user: any) => {
      console.log(user.name);

      const userId = user.name;
      if (!combinedSummary[userId]) {
        combinedSummary[userId] = {
          username: user.name,
          vendorsCount: 0,
          clientsCount: 0,
          resourceCount: 0,
          requirementCount: 0,
        };
      } else {
        combinedSummary[userId].username = user.name;
      }
    });

    return NextResponse.json({
      count: totalCount,
      summary: combinedSummary,
      success: true,
    });
  } catch (err) {
    console.error(err);
    return NextResponse.json({ error: "An error occurred" }, { status: 500 });
  }
}

export async function POST(request: NextRequest, response: NextResponse) {
  try {
    const reqBody = await request.json();
    const { startDate, endDate } = reqBody;
    const dateStart = new Date(startDate);
    const dateEnd = new Date(endDate);

    if (!startDate || !endDate) {
      return NextResponse.json(
        { error: "startDate and endDate are required" },
        { status: 400 }
      );
    }
    const userSummary = await Promise.all([
      Vendor.aggregate([
        {
          $match: {
            createdAt: {
              $gte: dateStart,
              $lte: dateEnd,
            },
          },
        },
        {
          $group: {
            _id: "$accountManager",
            vendorsCount: { $sum: 1 },
          },
        },
      ]).exec(),
      Client.aggregate([
        {
          $match: {
            createdAt: {
              $gte: dateStart,
              $lte: dateEnd,
            },
          },
        },
        {
          $group: {
            _id: "$accountManager",
            clientsCount: { $sum: 1 },
          },
        },
      ]).exec(),

      Resource.aggregate([
        {
          $match: {
            createdAt: {
              $gte: dateStart,
              $lte: dateEnd,
            },
          },
        },
        {
          $group: {
            _id: "$accountManager",
            resourcesCount: { $sum: 1 },
          },
        },
      ]).exec(),
      Requirement.aggregate([
        {
          $match: {
            createdAt: {
              $gte: dateStart,
              $lte: dateEnd,
            },
          },
        },
        {
          $group: {
            _id: "$accountManager",
            requirementCount: { $sum: 1 },
          },
        },
      ]).exec(),

      User.find({}, { name: 1 }).exec(), // Fetch all users with only the name field
    ]);

    // Combine results for clients and vendors
    interface CombinedSummary {
      [key: string]: {
        vendorsCount?: number;
        clientsCount?: number;
        resourceCount?: number;
        requirementCount?: number;
        username?: string;
      };
    }

    const combinedSummary: CombinedSummary = {};
    userSummary[0].forEach((entry: { _id: string; vendorsCount: number }) => {
      combinedSummary[entry._id] = {
        ...combinedSummary[entry._id],
        vendorsCount: entry.vendorsCount || 0,
      };
    });
    userSummary[1].forEach((entry: { _id: string; clientsCount: number }) => {
      combinedSummary[entry._id] = {
        ...combinedSummary[entry._id],
        clientsCount: entry.clientsCount || 0,
      };
    });
    userSummary[2].forEach((entry: { _id: string; resourcesCount: number }) => {
      combinedSummary[entry._id] = {
        ...combinedSummary[entry._id],
        resourceCount: entry.resourcesCount || 0,
      };
    });
    userSummary[3].forEach(
      (entry: { _id: string; requirementCount: number }) => {
        combinedSummary[entry._id] = {
          ...combinedSummary[entry._id],
          requirementCount: entry.requirementCount || 0,
        };
      }
    );

   // Add usernames to the summary
    userSummary[4].forEach((user: any) => {
      const userId = user.name;
      if (!combinedSummary[userId]) {
        combinedSummary[userId] = {
          username: user.name,
          vendorsCount: 0,
          clientsCount: 0,
          resourceCount: 0,
          requirementCount: 0,
        };
      } else {
        combinedSummary[userId].username = user.name;
      }
    });

    return NextResponse.json({
      response: combinedSummary,
    });
  } catch (err) {
    console.error(err);
    return NextResponse.json({ error: "An error occurred" }, { status: 500 });
  }
}
