import { NextRequest, NextResponse } from "next/server";
import User from "@/server/models/userModel";
export async function GET(req: NextRequest, res: NextResponse) {
  try {
    const response = await User.find();
    return NextResponse.json({
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}
