import { ERROR_MESSAGE } from "@/constants/errorMessage";
import Client from "@/server/models/client";
import Resource from "@/server/models/resource";
import userModel from "@/server/models/userModel";
import { NextRequest, NextResponse } from "next/server";

export async function GET(
  req: NextRequest,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  try {
    const { id } = params;
    if (!id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const user = await userModel.findOne({ _id: id });

    if (!user) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.DATA_NOT_FOUND,
      });
    }

    const client = await Client.find({
      accountManager: { $in: [user.name] },
    })
      .populate("ndaFile")
      .exec();
    return NextResponse.json({
      response: client,
      success: true,
    });
  } catch (error) {
    console.log("error", error);
    return NextResponse.json({ error, status: 500 });
  }
}
