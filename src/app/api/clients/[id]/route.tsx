import { ERROR_MESSAGE } from "@/constants/errorMessage";
import Client from "@/server/models/client";
import { NextRequest, NextResponse } from "next/server";

export async function GET(
  req: NextRequest,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  try {
    const { id } = params;

    if (!id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }

    const clients = await Client.findOne({ _id: id })
      .populate("ndaFile")
      .exec();

    return NextResponse.json({
      response: clients,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}
