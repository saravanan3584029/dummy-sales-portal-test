import { ERROR_MESSAGE } from "@/constants/errorMessage";
import Client from "@/server/models/client";
import ndaFile from "@/server/models/ndaFile";
import { SUCCESS_MESSAGE } from "@/server/validation/ValidationMessages";
import mongoose from "mongoose";
import { NextRequest, NextResponse } from "next/server";

import { connect } from "@/dbConfig/dbConfig";
import { fileUploadToGridFs } from "@/server/common";
import Requirement from "@/server/models/requirement";

connect();

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.formData();

    let file, requirementData: any, client;
    Array.from(data.entries()).forEach(([key, value]) => {
      if (key === "file") {
        file = value as File;
      } else if (key === "jsonData") {
        requirementData = JSON.parse(value as string);
      }
    });

    const upload = await fileUploadToGridFs(ndaFile, file, "ndaFileReference");

    client = {
      ndaFile: upload?._id,
      ...requirementData,
    };

    const insertData = await Client.create({ ...client });
    const response = await insertData.save();

    return NextResponse.json({
      message: SUCCESS_MESSAGE.CLIENT_CREATED,
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function GET(req: NextRequest, res: NextResponse) {
  try {
    const clients = await Client.find()
      .populate("ndaFile")
      .populate("requirements")
      .exec();

    return NextResponse.json({
      response: clients,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function PUT(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.formData();
    let file: any, input: any, UpdateData;
    Array.from(data.entries()).forEach(([key, value]) => {
      if (key === "file") {
        file = value as File;
      } else if (key === "jsonData") {
        input = JSON.parse(value as string);
      }
    });
    const { _id, ...remainingData } = input;
    if (!_id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const client = await Client.findOne({ _id: _id })
      .populate("ndaFile")
      .exec();

    if (!client) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.DATA_NOT_FOUND,
      });
    }
    if (file) {
      if (client?.ndaFile) {
        const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
        await ndaFile.deleteOne({ _id: client.ndaFile._id });
        await gfs.delete(
          new mongoose.Types.ObjectId(client.ndaFile.ndaFileReference)
        );
      }
      const upload = await fileUploadToGridFs(
        ndaFile,
        file,
        "ndaFileReference"
      );
      UpdateData = { ...remainingData, ndaFile: upload._id };
    } else {
      UpdateData = { ...remainingData };
    }
    const response = await Client.findByIdAndUpdate({ _id: _id }, UpdateData, {
      new: true,
      upsert: true,
      setDefaultsOnInsert: true,
    });
    return NextResponse.json({
      message: SUCCESS_MESSAGE.CLIENT_UPDATED,
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function DELETE(req: NextRequest, res: NextResponse) {
  try {
    const { id } = await req.json();
    if (!id || id.length === 0) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);

    if (typeof id === "string") {
      const vendordata: any = await Client.findOne({ _id: id })
        .populate("ndaFile")
        .exec();
      if (!vendordata) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }
      if (vendordata?.ndaFile) {
        await ndaFile.deleteOne({ _id: vendordata.ndaFile._id });
        await gfs.delete(
          new mongoose.Types.ObjectId(vendordata.ndaFile.ndaFileReference)
        );
      }
      await Client.findByIdAndDelete({ _id: id });
    } else {
      const vendorData = await Client.find({ _id: { $in: id } })
        .populate("ndaFile")
        .exec();
      if (vendorData.length === 0) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }
      const deleteVendor = async (vendor: any) => {
        if (vendor?.ndaFile) {
          const res = await ndaFile.deleteOne({ _id: vendor.ndaFile._id });
          await gfs.delete(
            new mongoose.Types.ObjectId(vendor.ndaFile.ndaFileReference)
          );
        }
        await Client.findByIdAndDelete({ _id: vendor._id });
      };

      // Use Promise.all to parallelize the deletion operations
      await Promise.all(vendorData.map(deleteVendor));
    }

    return NextResponse.json({
      message: SUCCESS_MESSAGE.VENDOR_DELETED,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}
