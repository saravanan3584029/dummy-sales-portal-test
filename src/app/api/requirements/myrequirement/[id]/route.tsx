import { ERROR_MESSAGE } from "@/constants/errorMessage";
import Requirement from "@/server/models/requirement";
import userModel from "@/server/models/userModel";
import { NextRequest, NextResponse } from "next/server";

export async function GET(
  req: NextRequest,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  try {
    const { id } = params;
    if (!id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const user = await userModel.findOne({ _id: id });

    if (!user) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.DATA_NOT_FOUND,
      });
    }

    const requirements = await Requirement.find({
      accountManager: { $in: [user.name] },
    })
      .populate("attachment")
      .exec();
    return NextResponse.json({
      response: requirements,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}
