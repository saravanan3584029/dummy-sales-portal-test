import { fileUploadToGridFs, generateUniquetId } from "@/server/common";
import Requirement from "@/server/models/requirement";
import { NextRequest, NextResponse } from "next/server";
import { connect } from "@/dbConfig/dbConfig";
import { SUCCESS_MESSAGE } from "@/server/validation/ValidationMessages";
import { ERROR_MESSAGE } from "@/constants/errorMessage";
import FileUpload from "@/server/models/fileUpload";
import mongoose from "mongoose";
import Client from "@/server/models/client";
connect();
export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.formData();
    const uniqueId = await generateUniquetId(Requirement, "REQ");
    let file, requirementData: any, requirement;
    Array.from(data.entries()).forEach(([key, value]) => {
      if (key === "file") {
        file = value as File;
      } else if (key === "jsonData") {
        requirementData = JSON.parse(value as string);
      }
    });

    const { clientName, ...remaining } = requirementData;

    const client = await Client.findOne({ clientName: clientName });

    if (file) {
      const upload = await fileUploadToGridFs(
        FileUpload,
        file,
        "fileReference"
      );
      requirement = {
        reqId: uniqueId,
        attachment: upload?._id,
        clientName: client._id,
        ...remaining,
      };
    } else {
      requirement = {
        reqId: uniqueId,
        clientName: client._id,
        ...remaining,
      };
    }

    const insertData = await Requirement.create({ ...requirement });

    const response = await insertData.save();
    if (response) {
      await Client.findByIdAndUpdate(
        { _id: client._id },
        { $push: { requirements: response._id } }
      );
    }

    return NextResponse.json({
      message: SUCCESS_MESSAGE.REQUIREMENT_CREATED,
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function GET(req: NextRequest, res: NextResponse) {
  try {
    const response = await Requirement.find()
      .populate("attachment")
      .populate("clientName")
      .exec();
    return NextResponse.json({
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function PUT(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.formData();
    let file: any, input: any, UpdateData;
    Array.from(data.entries()).forEach(([key, value]) => {
      if (key === "file") {
        file = value as File;
      } else if (key === "jsonData") {
        input = JSON.parse(value as string);
      }
    });

    const { _id, ...remainingData } = input;

    if (!_id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const requirement = await Requirement.findOne({ _id: _id })
      .populate("attachment")
      .exec();

    if (!requirement) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.DATA_NOT_FOUND,
      });
    }

    if (file) {
      if (requirement?.attachment) {
        const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
        await FileUpload.deleteOne({ _id: requirement.attachment._id });
        await gfs.delete(
          new mongoose.Types.ObjectId(requirement.attachment.fileReference)
        );
      }
      const upload = await fileUploadToGridFs(
        FileUpload,
        file,
        "fileReference"
      );
      UpdateData = { ...remainingData, attachment: upload._id };
    } else {
      UpdateData = { ...remainingData };
    }

    const response = await Requirement.findByIdAndUpdate(
      { _id: _id },
      UpdateData,
      {
        new: true,
        upsert: true,
        setDefaultsOnInsert: true,
      }
    );
    return NextResponse.json({
      message: SUCCESS_MESSAGE.REQUIREMENT_UPDATED,
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function DELETE(req: NextRequest, res: NextResponse) {
  const session = await mongoose.startSession();
  try {
    session.startTransaction();
    const { id } = await req.json();
    if (id.length === 0) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
    const requirementdata = await Requirement.find({ _id: { $in: id } })
      .populate("attachment")
      .exec();

    if (requirementdata.length !== id.length) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MULTIPLE_QUERY_ERROR,
      });
    }

    const deleteData = async (resource: any) => {
      try {
        const res = await FileUpload.deleteOne({
          _id: resource.attachment._id,
        });

        await gfs.delete(
          new mongoose.Types.ObjectId(resource.attachment.fileReference)
        );

        await Requirement.findByIdAndDelete({ _id: resource._id });
      } catch (error) {
        throw error;
      }
    };

    // Use Promise.all to parallelize the deletion operations
    await Promise.all(requirementdata.map(deleteData));
    await session.commitTransaction();
    session.endSession();

    return NextResponse.json({
      message: SUCCESS_MESSAGE.RECORD_DELETED,
      success: true,
    });
  } catch (error) {
    await session.abortTransaction();
    session.endSession();
    return NextResponse.json({ error, status: 500 });
  }
}
