import { connect } from "@/dbConfig/dbConfig";
import { NextRequest, NextResponse } from "next/server";
import { ERROR_MESSAGE } from "@/constants/errorMessage";
import { SUCCESS_MESSAGE } from "@/server/validation/ValidationMessages";
import Requirement from "@/server/models/requirement";
connect();

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const { postComment, _id } = await req.json();

    if (!_id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    let response;
    if (postComment.comment) {
      const vendor = await Requirement.findOne({ _id: _id });
      if (!vendor) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }
      const addComment = {
        message: postComment.comment,
        publishedBy: postComment.postedBy,
        publishedAt: new Date(),
      };

      response = await Requirement.findByIdAndUpdate(
        { _id: _id },
        { $push: { notes: addComment } },
        { new: true }
      );
    } else {
      const vendor = await Requirement.findOne({ "notes._id": _id });
      if (!vendor) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }
      const addReply = {
        message: postComment.reply,
        repliedBy: postComment.postedBy,
        repliedAt: new Date(),
      };

      response = await Requirement.findOneAndUpdate(
        { "notes._id": _id },
        { $push: { "notes.$.reply": addReply } },
        { new: true }
      );
    }
    return NextResponse.json({
      message: SUCCESS_MESSAGE.COMMENT_ADDED,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}
