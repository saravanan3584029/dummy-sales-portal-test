import { connect } from "@/dbConfig/dbConfig";
import Resource from "@/server/models/resource";
import { NextRequest, NextResponse } from "next/server";
import { ERROR_MESSAGE } from "@/constants/errorMessage";
import { SUCCESS_MESSAGE } from "@/server/validation/ValidationMessages";
connect();

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const { postComment, _id } = await req.json();

    if (!_id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    let response;
    if (postComment.comment) {
      const resource = await Resource.findOne({ _id: _id });
      if (!resource) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }
      const addComment = {
        message: postComment.comment,
        publishedBy: postComment.postedBy,
        publishedAt: new Date(),
      };

      response = await Resource.findByIdAndUpdate(
        { _id: _id },
        { $push: { notes: addComment } },
        { new: true }
      );
    } else {
      const resource = await Resource.findOne({ "notes._id": _id });
      if (!resource) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }
      const addReply = {
        message: postComment.reply,
        repliedBy: postComment.postedBy,
        repliedAt: new Date(),
      };

      response = await Resource.findOneAndUpdate(
        { "notes._id": _id },
        { $push: { "notes.$.reply": addReply } },
        { new: true }
      );
    }
    return NextResponse.json({
      message: SUCCESS_MESSAGE.COMMENT_ADDED,
      data: response,
      success: true,
    });
  } catch (error) {
    console.log("error", error);
    return NextResponse.json({ error, status: 500 });
  }
}
