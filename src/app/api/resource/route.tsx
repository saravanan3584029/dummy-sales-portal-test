import { connect } from "@/dbConfig/dbConfig";
import mongoose, { connection } from "mongoose";
import Resource from "@/server/models/resource";
import { NextRequest, NextResponse } from "next/server";
import FileUpload from "@/server/models/fileUpload";
import { ERROR_MESSAGE } from "@/constants/errorMessage";
import { SUCCESS_MESSAGE } from "@/server/validation/ValidationMessages";
import { ResourceData } from "@/feature/resources/config/types";
import { fileUploadToGridFs } from "@/server/common";
import Vendor from "@/server/models/vendor";
connect();

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.formData();
    let file: any, resourceData: any;
    Array.from(data.entries()).forEach(([key, value]) => {
      if (key === "file") {
        file = value as File;
      } else if (key === "jsonData") {
        resourceData = JSON.parse(value as string);
      }
    });
    let fileupload: any;

    if (file) {
      fileupload = await fileUploadToGridFs(FileUpload, file, "fileReference");
    }

    const { vendorName, ...remaining } = resourceData;

    const vendor = await Vendor.findOne({ vendorName: vendorName });

    const resource = {
      ...remaining,
      resume: fileupload?._id,
      vendorName: vendor && vendor._id,
    };

    const insertData = await Resource.create({ ...resource });
    const response = await insertData.save();

    if (response) {
      await Vendor.findByIdAndUpdate(
        { _id: vendor._id },
        { $push: { resources: response._id } }
      );
    }

    return NextResponse.json({
      message: SUCCESS_MESSAGE.RESOURCE_CREATED,
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function GET(req: NextRequest, res: NextResponse) {
  try {
    const resources = await Resource.find()
      .populate("resume")
      .populate("vendorName")
      .exec();

    return NextResponse.json({
      response: resources,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function DELETE(req: NextRequest, res: NextResponse) {
  try {
    const { id } = await req.json();
    if (!id || id.length === 0) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);

    if (typeof id === "string") {
      const resourcedata: any = await Resource.findOne({ _id: id })
        .populate("resume")
        .exec();

      if (!resourcedata) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }

      await FileUpload.deleteOne({ _id: resourcedata.resume._id });

      await gfs.delete(
        new mongoose.Types.ObjectId(resourcedata.resume.fileReference)
      );

      await Resource.findByIdAndDelete({ _id: id });
    } else {
      const resourcedata = await Resource.find({ _id: { $in: id } })
        .populate("resume")
        .exec();

      if (resourcedata.length === 0) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }

      const deleteResource = async (resource: any) => {
        // Delete the associated FileUpload
        const res = await FileUpload.deleteOne({ _id: resource.resume._id });

        // Delete the file from GridFS
        await gfs.delete(
          new mongoose.Types.ObjectId(resource.resume.fileReference)
        );

        // Delete the Resource
        await Resource.findByIdAndDelete({ _id: resource._id });
      };

      // Use Promise.all to parallelize the deletion operations
      await Promise.all(resourcedata.map(deleteResource));
    }

    return NextResponse.json({
      message: SUCCESS_MESSAGE.RESOURCE_DELETED,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function PUT(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.formData();
    let file: any, input: any;
    Array.from(data.entries()).forEach(([key, value]) => {
      if (key === "file") {
        file = value as File;
      } else if (key === "jsonData") {
        input = JSON.parse(value as string);
      }
    });

    const { _id, vendorName, ...entry } = input;

    if (vendorName) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.VENDOR_CANNOT_BE_UPDATE,
      });
    }

    if (!_id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const resource = await Resource.findOne({ _id: _id });

    if (!resource) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.DATA_NOT_FOUND,
      });
    }
    let updateData;
    if (file) {
      if (resource?.resume) {
        const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
        await FileUpload.deleteOne({ _id: resource.resume._id });
        await gfs.delete(
          new mongoose.Types.ObjectId(resource.resume.fileReference)
        );
      }
      const upload = await fileUploadToGridFs(
        FileUpload,
        file,
        "fileReference"
      );
      updateData = { ...entry, resume: upload._id };
    } else {
      updateData = { ...entry };
    }

    await Resource.findByIdAndUpdate(_id, updateData, { new: true });
    return NextResponse.json({
      message: SUCCESS_MESSAGE.RESOURCE_UPDATED,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}
