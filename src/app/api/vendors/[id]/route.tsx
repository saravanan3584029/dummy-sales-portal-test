import { ERROR_MESSAGE } from "@/constants/errorMessage";
import Resource from "@/server/models/resource";
import Vendor from "@/server/models/vendor";
import { NextRequest, NextResponse } from "next/server";

export async function GET(
  req: NextRequest,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  try {
    const { id } = params;

    if (!id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }

    const vendor = await Vendor.findOne({ _id: id }).populate("ndaFile").exec();
    return NextResponse.json({
      response: vendor,
      success: true,
    });
  } catch (error) {
    console.log("error", error);
    return NextResponse.json({ error, status: 500 });
  }
}
