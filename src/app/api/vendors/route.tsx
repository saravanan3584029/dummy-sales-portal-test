// pages/api/login.js
import { NextRequest, NextResponse } from "next/server";
import { connect } from "@/dbConfig/dbConfig";
import mongoose from "mongoose";
import { fileUploadToGridFs } from "@/server/common";
import ndaFile from "@/server/models/ndaFile";
import { SUCCESS_MESSAGE } from "@/server/validation/ValidationMessages";
import Vendor from "@/server/models/vendor";
import { ERROR_MESSAGE } from "@/constants/errorMessage";

connect();

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.formData();

    let file, vendorData: any;
    Array.from(data.entries()).forEach(([key, value]) => {
      if (key === "file") {
        file = value as File;
      } else if (key === "jsonData") {
        vendorData = JSON.parse(value as string);
      }
    });

    const upload = await fileUploadToGridFs(ndaFile, file, "ndaFileReference");

    const vendor = {
      ...vendorData,
      ndaFile: upload._id,
    };

    const insertData = await Vendor.create({ ...vendor });
    const response = await insertData.save();
    return NextResponse.json({
      message: SUCCESS_MESSAGE.VENDOR_CREATED,
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error: error }, { status: 500 });
  }
}

export async function GET(req: NextRequest, res: NextResponse) {
  try {
    const response = await Vendor.find()
      .populate({
        path: "ndaFile",
      })
      .populate({
        path: "resources",
      })
      .exec();      

    return NextResponse.json({
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function PUT(req: NextRequest, res: NextResponse) {
  try {
    const data = await req.formData();
    let file: any, input: any, UpdateData;
    Array.from(data.entries()).forEach(([key, value]) => {
      if (key === "file") {
        file = value as File;
      } else if (key === "jsonData") {
        input = JSON.parse(value as string);
      }
    });

    const { _id, ...remainingData } = input;

    if (!_id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const vendor = await Vendor.findOne({ _id: _id })
      .populate("ndaFile")
      .exec();

    if (!vendor) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.DATA_NOT_FOUND,
      });
    }

    if (file) {
      if (vendor?.ndaFile) {
        const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
        await ndaFile.deleteOne({ _id: vendor.ndaFile._id });
        await gfs.delete(
          new mongoose.Types.ObjectId(vendor.ndaFile.ndaFileReference)
        );
      }
      const upload = await fileUploadToGridFs(
        ndaFile,
        file,
        "ndaFileReference"
      );
      UpdateData = { ...remainingData, ndaFile: upload._id };
    } else {
      UpdateData = { ...remainingData };
    }

    const response = await Vendor.findByIdAndUpdate({ _id: _id }, UpdateData, {
      new: true,
      upsert: true,
      setDefaultsOnInsert: true,
    });
    return NextResponse.json({
      message: SUCCESS_MESSAGE.VENDOR_UPDATED,
      response: response,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}

export async function DELETE(req: NextRequest, res: NextResponse) {
  try {
    const { id } = await req.json();
    if (!id || id.length === 0) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);

    if (typeof id === "string") {
      const vendordata: any = await Vendor.findOne({ _id: id })
        .populate("ndaFile")
        .exec();
      if (!vendordata) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }
      if (vendordata?.ndaFile) {
        await ndaFile.deleteOne({ _id: vendordata.ndaFile._id });
        await gfs.delete(
          new mongoose.Types.ObjectId(vendordata.ndaFile.ndaFileReference)
        );
      }
      await Vendor.findByIdAndDelete({ _id: id });
    } else {
      const vendorData = await Vendor.find({ _id: { $in: id } })
        .populate("ndaFile")
        .exec();
      if (vendorData.length === 0) {
        return NextResponse.json({
          status: 400,
          message: ERROR_MESSAGE.DATA_NOT_FOUND,
        });
      }
      const deleteVendor = async (vendor: any) => {
        if (vendor?.ndaFile) {
          const res = await ndaFile.deleteOne({ _id: vendor.ndaFile._id });
          await gfs.delete(
            new mongoose.Types.ObjectId(vendor.ndaFile.ndaFileReference)
          );
        }
        await Vendor.findByIdAndDelete({ _id: vendor._id });
      };

      // Use Promise.all to parallelize the deletion operations
      await Promise.all(vendorData.map(deleteVendor));
    }

    return NextResponse.json({
      message: SUCCESS_MESSAGE.VENDOR_DELETED,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}
