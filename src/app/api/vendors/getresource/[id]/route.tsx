import { ERROR_MESSAGE } from "@/constants/errorMessage";
import Resource from "@/server/models/resource";
import Vendor from "@/server/models/vendor";
import { NextRequest, NextResponse } from "next/server";

export async function GET(
  req: NextRequest,
  {
    params,
  }: {
    params: {
      id: string;
    };
  }
) {
  try {
    const { id } = params;
    if (!id) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.MISSING_PARAMETER,
      });
    }

    const vendor = await Vendor.findOne({ _id: id });

    if (!vendor) {
      return NextResponse.json({
        status: 400,
        message: ERROR_MESSAGE.DATA_NOT_FOUND,
      });
    }

    const resourceData = await Resource.find({
      _id: { $in: vendor.resources },
    });

    return NextResponse.json({
      response: resourceData,
      success: true,
    });
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
}
