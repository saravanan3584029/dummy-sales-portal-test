"use client";

import Icon from "@/lib/svg";

const LoaderSkeleton = () => {
  return (
    <div className="overlay">
      <div className="loader">
        <Icon type="elipse" width="60" height="60" fill="blue" />
      </div>
      <style jsx>
        {`
          .overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: rgba(0, 0, 0, 0.5);
            z-index: 40;
            cursor: pointer;
          }
          .loader {
            animation: spin 1s linear infinite;
          }

          @keyframes spin {
            0% {
              transform: rotate(0deg);
            }
            100% {
              transform: rotate(360deg);
            }
          }
        `}
      </style>
    </div>
  );
};

export default LoaderSkeleton;
