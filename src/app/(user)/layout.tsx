"use client";
import Content from "@/shared-components/components/Content";
import Sidebar from "@/shared-components/components/Sidebar";
import { Suspense } from "react";
import LoaderSkeleton from "../loading";
import { useDispatch, useSelector } from "react-redux";
import Toaster from "@/shared-components/ui/Toaster";
import Popup from "@/shared-components/components/Popup";
import { hidePopup } from "@/store/common";

export default function LoginLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const dispatch = useDispatch();

  const common: any = useSelector((state: any) => state.common);

  return (
    <>
      <div className="flex body-background h-screen ">
        <div className="sidebar text-white flex flex-col px-6">
          <Sidebar />
        </div>
        <div className="content">
          <Suspense fallback={<LoaderSkeleton />}></Suspense>
          {common.loader && <LoaderSkeleton />}
          <Content>{children}</Content>
          <Popup
            open={common.popup.visible}
            messageData={common.popup.messageData}
            classAdditions={common.popup.classAdditions}
            closePopup={() => {
              dispatch<any>(hidePopup());
            }}
          />
          <Toaster
            open={common.toaster.visible}
            messageData={common.toaster.messageData}
          />
        </div>
      </div>

      <style jsx>{`
        .body-background {
          background-color: #171718;
        }
        .sidebar {
          width: 19%;
        }
        .content {
          width: 81%;
        }
      `}</style>
    </>
  );
}
