import VendorDashboard from "@/feature/vendors/component";
import React from "react";

export default function page({ params }: { params: { id: string } }) {
  return <VendorDashboard id={params.id} />;
}
