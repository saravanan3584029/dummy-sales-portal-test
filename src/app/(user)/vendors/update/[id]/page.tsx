import VendorForm from "@/feature/vendors/component/VendorForm";
import React from "react";

export default function UpdateVendor({ params }: { params: { id: string } }) {
  return <VendorForm id={params.id} />;
}
