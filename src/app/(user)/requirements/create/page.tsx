import RequirementForm from "@/feature/requirements/component/RequirementForm";
import React, { Fragment } from "react";

export default function page() {
  return (
    <Fragment>
      <RequirementForm />
    </Fragment>
  );
}
