import RequirementForm from "@/feature/requirements/component/RequirementForm";

const UpdateResource = ({ params }: { params: { id: string } }) => {
  return (
    <>
      <RequirementForm id={params.id} />
    </>
  );
};

export default UpdateResource;
