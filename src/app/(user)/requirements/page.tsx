import RequirementDashboard from "@/feature/requirements/component";
import { Fragment } from "react";

const Requirement = () => {
  return (
    <Fragment>
      <RequirementDashboard />
    </Fragment>
  );
};

export default Requirement;
