import RequirementDashboard from "@/feature/requirements/component";
import { Fragment } from "react";

const Requirement = ({ params }: { params: { id: string } }) => {
  return (
    <Fragment>
      <RequirementDashboard id={params.id} />
    </Fragment>
  );
};

export default Requirement;
