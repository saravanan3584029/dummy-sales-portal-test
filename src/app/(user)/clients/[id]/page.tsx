import Clients from "@/feature/clients/components";
import { Fragment } from "react";

const MyClients = ({ params }: { params: { id: string } }) => {
  return (
    <Fragment>
      <Clients id={params?.id} />
    </Fragment>
  );
};

export default MyClients;
