// "use client";
// import SideBar from "@/shared-components/components/SideBar";
// import { useDispatch, useSelector } from "react-redux";
// import { SetSelectedMenuItem } from "@/store/sidebarslice";
// import { useEffect, useState } from "react";
// import ClientCard from "@/components//ClientCard";
// import { get } from "http";
// import Link from "next/link";
// import DetailModal from "@/components/DetailModal";

// export default function Page() {
//   const [clientData, setClientData] = useState([]);
//   const [filterClientData, setFilterClientData] = useState([]);
//   const [inputValue, setinputValue] = useState("");
//   const id=useSelector((state:any)=>state.user.userId);
//   // const userId=useSelector((state:any)=>state.user.userId);
//   const getClients = async () => {
//     const response = await fetch("/api/getclients");
//     if (response.status === 200) {
//       const data = await response.json();
//       console.log(data);
//       setFilterClientData(data);
//       setClientData(data);
//     }
//   };

//   const [selectedClient, setSelectedClient] = useState(null);

//   const handleSearch = (e:any) => {
//     setinputValue(e.target.value);
//     const filteredData = clientData.filter((client:any) => {
//       console.log(client)
//       return client.clientName.toLowerCase().includes(e.target.value.toLowerCase());
//     });
//     setFilterClientData(filteredData);
//   };

//   const handleClientCardClick = (vendor:any) => {
//     setSelectedClient(vendor);
//   };

//   const closeModal = () => {
//     setSelectedClient(null);
//   };

//   useEffect(() => {
//     console.log("useEffect");
//     getClients();
//   }, []);
//   return (
//     <>
//       <div className="flex flex-col bg-white w-full h-full">
//         <div id="NewRootRoot" className="border-solid h-[30%]  text-black  flex flex-row justify-between items-start border-t-0 border-b border-x-0">
//           <div className="bg-white flex flex-col gap-8  shrink-0 items-start py-0 w-full">
//             <div className="self-stretch flex flex-col gap-3 items-start">
//               <div
//                 id="TopBarclientBase"
//                 className="bg-white self-stretch flex flex-row justify-between items-center pb-5 px-6"
//               >
//                 <div className="flex flex-col mt-5 gap-5 w-1/2 items-start">
//                   <div className="self-stretch flex flex-col gap-4 items-start">
//                     <div className="flex flex-row gap-4 w-1/2 items-center">
//                       <button
//                         id="Button6"
//                         className="self-start flex flex-col justify-center w-8 shrink-0 h-8 items-center"
//                       >
//                         <img
//                           src="https://file.rendit.io/n/iWmen2RGxtsemeevvDPG.svg"
//                           id="Icon"
//                           className="w-4"
//                         />
//                       </button>
//                       <div className="flex flex-row justify-between w-56 items-center">
//                         <div className="text-xs font-['Inter'] leading-[14px] text-[#aaaaaf]">
//                           Dashboard
//                         </div>
//                         <img
//                           src="https://file.rendit.io/n/p9ILn7dKm8bOHtVhS0lh.svg"
//                           className="self-start w-4 shrink-0"
//                         />
//                         <div className="text-xs font-['Inter'] leading-[14px] text-[#aaaaaf]">
//                           Clients
//                         </div>
//                         <img
//                           src="https://file.rendit.io/n/p9ILn7dKm8bOHtVhS0lh.svg"
//                           className="self-start w-4 shrink-0"
//                         />
//                         <div className="text-xs font-['Inter'] leading-[14px] text-[#84848c]">
//                           All Clients
//                         </div>
//                       </div>
//                     </div>
//                     <div className="self-stretch flex flex-row gap-6 items-start">
//                       <div className="text-2xl font-['Inter'] text-black font-bold -mt-1 leading-[32px]">
//                         Clients
//                       </div>
//                       <div className="flex flex-row justify-between w-[250px] items-start">
//                         <Link href="/clients/create">

//                         <button
//                           id="Button8"
//                           className="flex flex-row justify-center gap-2 h-8 items-center px-3 py-2"
//                         >
//                           <img
//                             src="https://file.rendit.io/n/h9TbLKWf9NiWPLqSKUXp.svg"
//                             id="Plus1"
//                             className="w-4 shrink-0"
//                           />
//                           <button
//                             id="Button7"
//                             className="text-center text-sm font-['Inter'] font-semibold leading-[16px] text-[#1a68f3]"
//                           >
//                             Add Client
//                           </button>
//                         </button>
//                         </Link>
//                         <Link href={`/clients/${id}`}>
//                         <button
//                           id="Button10"
//                           className="flex flex-col justify-center h-8 items-center"
//                         >
//                           <button
//                             id="Button9"
//                             className="text-center text-sm font-['Inter'] font-semibold leading-[16px] text-[#1a68f3] mx-3"
//                           >
//                             My Clients
//                           </button>
//                         </button>
//                         </Link>

//                         {/* <button
//                           id="Button12"
//                           className="flex flex-row justify-center gap-2 h-8 items-center px-3 py-2"
//                         >
//                           <img
//                             src="https://file.rendit.io/n/hKEukTfLzKdYNpSz075H.svg"
//                             id="Resource"
//                             className="w-4 shrink-0"
//                           />
//                           <button
//                             id="Button11"
//                             className="text-center text-sm font-['Inter'] font-semibold leading-[16px] text-[#1a68f3]"
//                           >
//                             All Resource{" "}
//                           </button>
//                         </button> */}
//                       </div>
//                     </div>
//                   </div>
//                   <div className="border-solid border-[#e3e3e4] bg-[#f4f4f4] flex flex-row gap-2 w-3/6  h-10 shrink-0 items-center px-2 border rounded-lg">
//                     <img
//                       src="https://file.rendit.io/n/9vbwF1fW7fkrypKC4N6L.svg"
//                       id="Search"
//                       className="w-4 shrink-0"
//                     />
//                     <input
//                     value={inputValue}
//                    onChange={(e)=>handleSearch(e)}
//                     id="Input"
//                       className="text-sm font-['Inter'] font-semibold leading-[16px] text-[#84848c] bg-[#f4f4f4]  h-8 shrink-0"
//                       placeholder="Search by name"
//                     />
//                   </div>
//                 </div>
//                 <div className="self-end flex flex-row gap-2 items-start">
//                   <button
//                     id="ButtonToggle"
//                     className="flex flex-col w-24 shrink-0 items-start"
//                   >
//                     <button
//                       id="ButtonBase7"
//                       className="border-solid border-[#e3e3e4] shadow-[0px_2px_2px_0px_rgba(0,_0,_0,_0.03)] overflow-hidden bg-white flex flex-row justify-center gap-2 w-24 h-8 shrink-0 items-center border rounded-lg"
//                     >
//                       <img
//                         src="https://file.rendit.io/n/lZRVroqSEETcGXmZDR3m.svg"
//                         id="Bxsselectmultiple"
//                         className="w-4 shrink-0"
//                       />
//                       <button
//                         id="Button13"
//                         className="text-center text-sm font-['Inter'] font-semibold leading-[16px] text-[#39393d]"
//                       >
//                         Select
//                       </button>
//                     </button>
//                   </button>
//                   <button
//                     id="Button15"
//                     className="flex flex-col w-8 shrink-0 items-start"
//                   >
//                     <button
//                       id="ButtonBase8"
//                       className="border-solid border-[#e3e3e4] shadow-[0px_2px_2px_0px_rgba(0,_0,_0,_0.03)] overflow-hidden bg-white flex flex-col w-8 h-8 shrink-0 items-center py-2 border rounded-lg"
//                     >
//                       <img
//                         src="https://file.rendit.io/n/GtjtcB0mJL3lWrIorDBi.svg"
//                         id="Filter"
//                         className="w-4"
//                       />
//                     </button>
//                   </button>
//                 </div>
//               </div>
//               <div className="text-center text-xs font-['Inter'] leading-[14px] font-semibold text-[#66666e] ml-6">
//               {filterClientData.length} Clients
//             </div>
//               {/* <div className="flex flex-row ml-6 gap-2 items-start">
//                 <div className="relative flex flex-col mb-px w-24 shrink-0 items-end">
//                   <div
//                     id="ChipsFilter"
//                     className="w-24 h-8 bg-[#1553c2] absolute top-px left-0 flex flex-col items-start rounded-lg"
//                   >
//                     <button
//                       id="Button2"
//                       className="flex flex-col w-16 items-start"
//                     >
//                       <button
//                         id="ButtonBase"
//                         className="overflow-hidden bg-[#1553c2] flex flex-col w-16 h-8 shrink-0 items-center py-2 rounded-tl-lg rounded-bl-lg"
//                       >
//                         <button
//                           id="Button1"
//                           className="text-center text-sm font-['Inter'] font-semibold leading-[16px] text-white"
//                         >
//                           Onsite
//                         </button>
//                       </button>
//                     </button>
//                   </div>
//                   <button
//                     id="Button3"
//                     className="relative flex flex-col justify-center w-8 h-8 shrink-0 items-center"
//                   >
//                     <img
//                       src="https://file.rendit.io/n/Fs2BDydD8G5ePMmg78Lc.svg"
//                       id="Close1"
//                       className="w-4"
//                     />
//                   </button>
//                 </div>
//                 <button
//                   id="Button5"
//                   className="flex flex-col mt-px w-[114px] shrink-0"
//                 >
//                   <button
//                     id="ButtonBase2"
//                     className="overflow-hidden bg-[rgba(51,_113,_234,_0.1)] flex flex-row justify-center gap-2 h-8 shrink-0 items-center rounded-lg"
//                   >
//                     <img
//                       src="https://file.rendit.io/n/h9TbLKWf9NiWPLqSKUXp.svg"
//                       id="Plus"
//                       className="w-4 shrink-0"
//                     />
//                     <button
//                       id="Button4"
//                       className="text-center text-sm font-['Inter'] font-semibold leading-[16px] text-[#1a68f3]"
//                     >
//                       Add Filter
//                     </button>
//                   </button>
//                 </button>
//               </div> */}
//             </div>

//           </div>
//         </div>

//         <div className="h-[70%] bg-white overflow-auto mt-5  inset-x-0  m-auto  flex flex-wrap justify-start ">
//           {filterClientData.map((data: any, index: any) => (
//             <div
//             key={index}
//             onClick={() => handleClientCardClick(data)}
//             className="cursor-pointer"
//           >
//             <ClientCard key={index} client={data} />
//           </div>
//           ))}
//           {selectedClient && <DetailModal type={selectedClient} onClose={closeModal} />}
//         </div>
//       </div>
//     </>
//   );
// }

// "use client";

// import { useParams } from "next/navigation";
// import Image from "next/image";
// import { useState, useEffect } from "react";
// import { useRef } from "react";
// import UploadDocumentForm from "@/shared-components/components/UploadFormDocument";
// import SideBar from "@/shared-components/components/Sidebar";
// import { useSelector, useDispatch } from "react-redux";
// import { useRouter } from "next/navigation";
// import ClientCard from "@/components/ClientCard";
// import Link from "next/link";
// import DetailModal from "@/components/DetailModal";

// export default function Home() {
//   const params = useParams();
//   const [filterClientData, setfilterClientData] = useState([]);
//   const [inputValue, setinputValue] = useState("");

//   const id = params.id;
//   const [clientData, setClientData] = useState([]);
//   const [selectedClient, setSelectedClient] = useState(null);
//   const getClients = async () => {
//     const response = await fetch("/api/getclients/" + id);
//     if (response.status === 200) {
//       const data = await response.json();
//       setfilterClientData(data);
//       setClientData(data);
//     }
//   };
//   const handleSearch = (e: any) => {
//     setinputValue(e.target.value);
//     const filteredData = clientData.filter((client: any) => {
//       return client.clientName
//         .toLowerCase()
//         .includes(e.target.value.toLowerCase());
//     });
//     setfilterClientData(filteredData);
//   };

//   const handleClientCardClick = (vendor: any) => {
//     setSelectedClient(vendor);
//   };

//   const closeModal = () => {
//     setSelectedClient(null);
//   };
//   useEffect(() => {
//     getClients();
//   }, []);
//   return (
//     <>
//       <div className="flex flex-col h-full bg-white  w-full">
//         <div
//           id="TopBarVendorBaseRoot"
//           className="h-[30%] border-solid border-[#e3e3e4] bg-white flex flex-row justify-between pr-6  items-end border-t-0 border-b border-x-0"
//         >
//           <div className="flex flex-col justify-between gap-5 items-start mt-5 mb-0">
//             <div className="self-end flex flex-col ml-6 gap-4 w-[440px] items-start">
//               <div className="flex flex-row gap-4 w-3/5 items-center">
//                 <button
//                   id="Button1"
//                   className="self-start flex flex-col justify-center w-8 shrink-0 h-8 items-center"
//                 >
//                   <img
//                     src="https://file.rendit.io/n/WGuQDI7SLr84Yh6J9tpR.svg"
//                     id="Icon"
//                     className="w-4"
//                   />
//                 </button>
//                 <div className="flex flex-row justify-between w-56 items-center">
//                   <div className="text-xs font-['Inter'] leading-[14px] text-[#aaaaaf]">
//                     Dashboard
//                   </div>
//                   <img
//                     src="https://file.rendit.io/n/ArT39s2JYFqxFCoQlXiq.svg"
//                     className="self-start w-4 shrink-0"
//                   />
//                   <div className="text-xs font-['Inter'] leading-[14px] text-[#aaaaaf]">
//                     Clients
//                   </div>
//                   <img
//                     src="https://file.rendit.io/n/ArT39s2JYFqxFCoQlXiq.svg"
//                     className="self-start w-4 shrink-0"
//                   />
//                   <div className="text-xs font-['Inter'] leading-[14px] text-[#84848c]">
//                     All Clients
//                   </div>
//                 </div>
//               </div>
//               <div className="self-stretch flex flex-row gap-6 items-start">
//                 <div className="text-black text-2xl font-['Inter'] font-semibold leading-[32px]">
//                   My Clients
//                 </div>

//                 <div className="flex flex-row gap-2 w-3/5 items-start">
//                   <button
//                     id="Button3"
//                     className="flex flex-row justify-center gap-2 w-1/2 h-8 items-center"
//                   >
//                     <img
//                       src="https://file.rendit.io/n/mRxIELd1xrCXeWtgShju.svg"
//                       id="Plus"
//                       className="w-4 shrink-0"
//                     />
//                     <Link href={`/clients/create`}>
//                       <button
//                         id="Button2"
//                         className="text-center text-sm font-['Inter'] font-semibold leading-[16px] text-[#1a68f3]"
//                       >
//                         Add Clients{" "}
//                       </button>
//                     </Link>
//                   </button>
//                   <button
//                     id="Button5"
//                     className="flex flex-row justify-center gap-2 w-1/2 h-8 items-center"
//                   >
//                     <img
//                       src="https://file.rendit.io/n/79PJxJWRyViA7LK1qJ0F.svg"
//                       id="Client"
//                       className="w-4 shrink-0"
//                     />
//                     <Link href={`/clients`}>
//                       <button
//                         id="Button4"
//                         className="text-center text-sm font-['Inter'] font-semibold leading-[16px] text-[#1a68f3]"
//                       >
//                         All Clients
//                       </button>
//                     </Link>
//                   </button>
//                 </div>
//               </div>
//             </div>
//             <div className="border-solid border-[#e3e3e4] bg-white flex flex-row ml-6 gap-2 w-[280px] h-8 shrink-0 items-center px-2 border rounded-lg">
//               <img
//                 src="https://file.rendit.io/n/aZtYyE0kKFaDF6oMq5Zp.svg"
//                 id="Search"
//                 className="w-4 shrink-0"
//               />
//               <input
//                 value={inputValue}
//                 onChange={(e) => handleSearch(e)}
//                 id="Input"
//                 className="text-sm font-['Inter'] font-semibold leading-[16px] text-[#84848c] bg-white  shrink-0"
//                 placeholder="Search by name"
//               />
//             </div>
//             <div className="border-solid border-[#e3e3e4] flex flex-row justify-between w-[302px] h-12 shrink-0 items-start px-6 border-t-0 border-b border-x-0">
//               <div className="self-end flex flex-col mb-0 gap-4 w-4 shrink-0 h-8 items-start">
//                 <div
//                   id="Item"
//                   className="text-sm font-['Inter'] font-medium leading-[16px] text-[#171718]"
//                 >
//                   All
//                 </div>
//                 <div className="bg-[#4886f5] w-4 h-px shrink-0 rounded-none" />
//               </div>
//               <div
//                 id="Item1"
//                 className="text-sm font-['Inter'] font-medium leading-[16px] text-[#84848c] mt-4"
//               >
//                 Onsite
//               </div>
//               <div
//                 id="Item2"
//                 className="text-sm font-['Inter'] font-medium leading-[16px] text-[#84848c] mt-4"
//               >
//                 Hybrid
//               </div>
//               <div
//                 id="Item3"
//                 className="text-sm font-['Inter'] font-medium leading-[16px] text-[#84848c] mt-4"
//               >
//                 Remote
//               </div>
//             </div>
//           </div>
//           <div className="flex flex-row mb-16 gap-2 items-start">
//             <button
//               id="ButtonToggle"
//               className="flex flex-col w-24 shrink-0 items-start"
//             >
//               <button
//                 id="ButtonBase3"
//                 className="border-solid border-[#e3e3e4] shadow-[0px_2px_2px_0px_rgba(0,_0,_0,_0.03)] overflow-hidden bg-white flex flex-row justify-center gap-2 w-24 h-8 shrink-0 items-center border rounded-lg"
//               >
//                 <img
//                   src="https://file.rendit.io/n/Zxi9mimYx4yMTvGFUvUk.svg"
//                   id="Bxsselectmultiple"
//                   className="w-4 shrink-0"
//                 />
//                 <input
//                   value={inputValue}
//                   onChange={(e) => handleSearch(e)}
//                   id="Input"
//                   className="text-sm font-['Inter'] font-semibold leading-[16px] text-[#84848c] bg-[#f4f4f4] h-8 shrink-0"
//                   placeholder="Search by name"
//                 />
//               </button>
//             </button>
//             <button
//               id="Button8"
//               className="flex flex-col w-8 shrink-0 items-start"
//             >
//               <button
//                 id="ButtonBase4"
//                 className="border-solid border-[#e3e3e4] shadow-[0px_2px_2px_0px_rgba(0,_0,_0,_0.03)] overflow-hidden bg-white flex flex-col w-8 h-8 shrink-0 items-center py-2 border rounded-lg"
//               >
//                 <img
//                   src="https://file.rendit.io/n/cFsrsyb7XDCrAELOEDDi.svg"
//                   id="Filter"
//                   className="w-4"
//                 />
//               </button>
//             </button>
//           </div>
//         </div>
//         {/* <div className="flex flex-row justify-between w-full items-start">
// <VendorCard vendor={dummyVendorData} />;
// <VendorCard vendor={dummyVendorData} />;
// <VendorCard vendor={dummyVendorData} />;
// <VendorCard vendor={dummyVendorData} />;
// </div> */}
//         <div className="h-[70%] bg-white overflow-auto mt-5  inset-x-0  m-auto  flex flex-wrap justify-start">
//           {filterClientData.map((data: any, index: any) => (
//             <div
//               key={index}
//               onClick={() => handleClientCardClick(data)}
//               className="cursor-pointer"
//             >
//               <ClientCard key={index} client={data} />
//             </div>
//           ))}
//           {selectedClient && (
//             <DetailModal type={selectedClient} onClose={closeModal} />
//           )}
//         </div>
//       </div>
//     </>
//   );
// }

import Clients from "@/feature/clients/components";
import { Fragment } from "react";

const MyClients = () => {
  return (
    <Fragment>
      <Clients />
    </Fragment>
  );
};

export default MyClients;
