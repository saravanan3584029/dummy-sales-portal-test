'use client'

import CreateClientForm from "@/feature/clients/components/CreateClientForm";
import { useParams } from "next/navigation";

export default function Page(){
  const params = useParams()
  return <CreateClientForm columnNum={3} id={params?.id}/>
}