import ResourceForm from "@/feature/resources/components/ResourceForm";

const UpdateResource = ({params}:{params:{id:string}}) => {
  return (
    <>
      <ResourceForm id={params.id}/>
    </>
  );
};

export default UpdateResource;
