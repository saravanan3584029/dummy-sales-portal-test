import ResourceForm from "@/feature/resources/components/ResourceForm";

const AddResource = () => {
  return (
    <>
      <ResourceForm />
    </>
  );
};

export default AddResource;
