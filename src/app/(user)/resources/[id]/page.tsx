import React from "react";
import ResourceDashboard from "@/feature/resources/components/index";
export default function page({ params }: { params: { id: string } }) {
  return <ResourceDashboard id={params.id} />;
}
