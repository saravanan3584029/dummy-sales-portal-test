import "./globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { Providers } from "@/store/provider";
import { ReactNode } from "react";
import Theme from "@/shared-components/layout/app/theme";
import appConfig from "@/config/appConfig";

const inter = Inter({ subsets: ["latin"] });
const { themeConfig } = appConfig;

export const metadata: Metadata = {
  title: "SalesPortal",
  description: "SalesPortal for witarist it services",
};

interface LayoutProps {
  children: ReactNode;
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <Theme themeConfig={themeConfig} />
      <body className={inter.className}>
        <Providers>{children}</Providers>
      </body>
    </html>
  );
}
