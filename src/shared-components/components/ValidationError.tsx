import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";

const ValidationError = ({ error }: { error: string }) => {
  return (
    <div className="flex items-center gap-1 w-full mt-2 text-red-600">
      <Icon type="alert" width="20" height="20" fill={`${COLOR.RED}`} />
      <p className="text-sm font-semibold text-red-600">{error}</p>
    </div>
  );
};

export default ValidationError;
