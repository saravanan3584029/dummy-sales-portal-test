"use client";
import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import Icon from "@/lib/svg";

import { usePathname } from "next/navigation";

import { useDispatch, useSelector } from "react-redux";
import {
  SetSelectedSubmenuItem,
  SetSelectedMenuItem,
} from "@/store/sidebarslice";

const noOperation: React.KeyboardEventHandler<HTMLDivElement> = (e) => {
  // no operation;
  if (e && typeof e.preventDefault === "function") e.preventDefault();
};

type SubMenuProps = {
  item: any;
  closeMenu?: () => void;
  toggleSubMenu: () => void;
  openedSubMenu: string;
  openLinktitle?: string;
};

const SubMenu = ({
  item,
  toggleSubMenu,
  openedSubMenu,
}: SubMenuProps): JSX.Element => {
  let dropdownIcon: string = "";
  if (item.submenu?.length > 0 && openedSubMenu === item.label) {
    dropdownIcon = "chevron-down";
  }
  const dispatch = useDispatch();
  const router = useRouter();
  const sidebar: any = useSelector((state: any) => state.sidebar);

  const handleClick = (path: string, label: string) => {
    dispatch<any>(SetSelectedSubmenuItem(label));
    router.push(path);
  };
  const pathname = usePathname();

  useEffect(() => {}, [pathname]);
  return (
    <>
      <div
        onClick={(e) => {
          toggleSubMenu();
          dispatch<any>(SetSelectedSubmenuItem(item.label));
          router.push(item.url);
        }}
        onKeyDown={noOperation}
        role="button"
        tabIndex={0}
        className={`text-white flex justify-between items-start mx-2  ${
          openedSubMenu === item.label && " p-0"
        } `}
      >
        <div className="text-white flex gap-3  w-1/2 items-start">
          <Icon
            type={item.icon}
            width={item.width}
            height={item.height}
            fill={openedSubMenu !== item.label ? "#84848C" : "#f4f4f4"}
          />
          <div
            id="MenuItem3"
            className={`text-sm font-['Inter'] font-medium leading-[16px]  
         h-12 ${
           openedSubMenu !== item.label ? "text-[#84848C]" : "text-[#f4f4f4]"
         }`}
          >
            {item.label}
          </div>
        </div>
        <div className="">
          <div
            style={{
              transform: openedSubMenu === item.label ? "rotate(45deg)" : "",
              transition: "transform 0.2s ease-in-out",
              marginLeft: "3.1rem",
            }}
          >
            <Icon
              type="plus-white"
              fill={openedSubMenu !== item.label ? "#84848C" : ""}
            />
          </div>
        </div>
      </div>

      <div className="flex ">
        {openedSubMenu === item.label && (
          <div
            className="vertical-line ml-10 "
            style={{
              marginTop: "-1rem",
              marginBottom: "1rem",
              // style={{
              marginLeft: "2.5rem",
              // }}
            }}
          ></div>
        )}
        <div
          style={{
            marginTop: "-2rem",
          }}
        >
          {openedSubMenu === item.label &&
            item.submenu?.map((link: any, index: any) => {
              return (
                <div key={index}>
                  {link.url && (
                    <div
                      role="button"
                      tabIndex={0}
                      onClick={() => handleClick(link.url, link.label)}
                      onKeyDown={noOperation}
                      className={`${
                        sidebar.selectedSubmenuItem === link.label &&
                        "text-selected"
                      } my-4  pl-1 text-sm `}
                    >
                      <div
                        className={`flex flex-col justify-center w-full h-8 items-start rounded-lg`}
                        style={{
                          marginLeft: "1rem",
                        }}
                      >
                        <div
                          id="ListItems"
                          className={`flex justify-start text-sm font-['Inter'] font-medium leading-[16px] ${
                            pathname === link.url
                              ? "w-[170px] p-2 rounded-md border border-gray-600 bg-custom-gray text-white"
                              : "text-[#84848C]"
                          }`}
                        >
                          {link.label}
                        </div>
                      </div>
                      {/* </a> */}
                    </div>
                  )}
                </div>
              );
            })}
        </div>
      </div>
      <style jsx>{`
        .selected-option {
          border: 2px solid rgba(141, 177, 249, 0.09);
          border-radius: 8px;
          background: rgba(48, 54, 67, 0.9);
        }
        .vertical-line {
          border-left: 1px solid #7b869e;
        }
        .bg-custom-gray {
          background-color: #2d2d31;
        }
      `}</style>
    </>
  );
};

export default SubMenu;
