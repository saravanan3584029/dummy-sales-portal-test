"use client";
import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import Button from "@/shared-components/ui/Button";
import Form from "@/shared-components/ui/Form";
import validationUtils from "@/utils/validation/validationUtils";
import { useState, useEffect } from "react";
import ValidationError from "./ValidationError";

export const NestedForm = ({
  formData,
  formValues,
  touched,
  handleBlur,
  errors,
  updatedData,
  setUpdatedData,
  fieldDomRefs,
  handleChange,
  btnConfig,
  handleSubmit,
  nestedFormValues,
  nestedFormData,
  setNestedFormData,
}: {
  formData: any;
  handleBlur?: any;
  formValues?: any;
  touched?: any;
  errors?: any;
  updatedData?: any;
  setUpdatedData?: any;
  fieldDomRefs?: any;
  handleChange?: any;
  btnConfig?: any;
  handleSubmit?: any;
  nestedFormValues: any;
  nestedFormData: any;
  setNestedFormData: any;
}) => {
  const [values, setValues] = useState<any>();
  const [addField, setAddField] = useState<boolean>(false);
  const [editedIndex, setEditedIndex] = useState<number | null>(null);

  useEffect(() => {
    setValues([]);
  }, [nestedFormData, nestedFormValues]);

  const clickHandler = (data: any) => {
    if (data?.goal === "ADD") {
      if (editedIndex !== null) {
        // If there's an edited index, update the specific item in the array
        const updatedFormData = [...nestedFormData];
        updatedFormData[editedIndex] = nestedFormValues;

        setNestedFormData(updatedFormData);
        setEditedIndex(null);
      } else {
        setNestedFormData([...nestedFormData, nestedFormValues]);
      }
      setAddField(!addField);
    } else {
      setAddField(!addField);
    }
  };

  const editHandler = (item: any, index: number, goal: string) => {
    if (goal === "edit") {
      setAddField(false); // Open the form for editing
      setValues(item);
      setEditedIndex(index);
    } else if (goal === "remove") {
      const updatedFormData = [...nestedFormData];
      updatedFormData.splice(index, 1);
      setNestedFormData(updatedFormData);
    }
  };
  const check = nestedFormData?.filter((e: any) => e?.isPrimary);

  return (
    <>
      {nestedFormData?.map((item: any, index: number) => {
        return (
          <div key={index}
            className="flex items-center justify-between p-2 mb-4 border rounded"
            style={{ backgroundColor: "#F4F4F4", borderColor: "#E3E3E4)" }}
          >
            <div className="flex items-center justify-start gap-x-2">
              <Icon type={"profile"} />
              <div>{item?.name}</div>
              <p style={{ color: COLOR.GRAY_4 }}>
                {item?.isPrimary ? "Primary" : ""}
              </p>
            </div>
            <div className="flex items-center justify-start gap-x-4">
              <Icon
                type={"pencil"}
                width="18"
                onClick={() => editHandler(item, index, "edit")}
              />
              <Icon
                type={"multiple"}
                fill="red"
                onClick={() => editHandler(item, index, "remove")}
              />
            </div>
          </div>
        );
      })}

      {!addField ? (
        <>
          {(
            <div
              className="items-center gap-1 w-full mt-2 text-red-600 mb-4"
              style={{
                display: `${
                  check.length === 0 && Object.keys(errors).length > 0
                    ? "flex"
                    : "none"
                }`,
              }}
            >
              {
                <Icon
                  type="alert"
                  width="20"
                  height="20"
                  fill={`${COLOR.RED}`}
                />
              }
              <p className="text-sm font-semibold text-red-600">
                {"Please create at least one primary contact"}
              </p>
            </div>
          )}
          <div className="grid grid-cols-4 gap-6 border border-1 p-3 rounded">
            {Array.isArray(formData) &&
              formData?.map((el: any,index:number) => {
                return (
                  <div className={el?.className} key={index}>
                    <div>{el?.key}</div>
                    <Form
                      formData={el}
                      handleChange={handleChange}
                      formValues={values}
                      touched={touched}
                      handleBlur={handleBlur}
                      errors={errors}
                      updatedData={formData}
                      setUpdatedData={setUpdatedData}
                      fieldDomRefs={fieldDomRefs}
                    />
                  </div>
                );
              })}
            <div className="flex justify-start gap-2">
              {btnConfig?.map((ele: any, index: number) => {
                return (
                  <Button
                    className={ele?.class}
                    type={ele?.type}
                    onClick={() => clickHandler(ele)}
                    disabled={ele?.disabled}
                    size={ele?.size}
                    variant={ele?.variant}
                    visible={ele?.visible}
                    key={index}
                  >
                    {ele?.children}
                  </Button>
                );
              })}
            </div>
          </div>
        </>
      ) : (
        <>
          {(
            <div
              className="items-center gap-1 w-full mt-2 text-red-600 mb-4"
              style={{
                display: `${
                  check.length === 0 && Object.keys(errors).length > 0
                    ? "flex"
                    : "none"
                }`,
              }}
            >
              {
                <Icon
                  type="alert"
                  width="20"
                  height="20"
                  fill={`${COLOR.RED}`}
                />
              }
              <p className="text-sm font-semibold text-red-600">
                {"Please create at least one primary contact"}
              </p>
            </div>
          )}

          <Button
            type="button"
            onClick={() => setAddField(!addField)}
            disabled={false}
            variant="disable"
          >
            <Icon type="add" width="20" height="20" />
            <span className="ml-1">Add Contact</span>
          </Button>
        </>
      )}
    </>
  );
};
