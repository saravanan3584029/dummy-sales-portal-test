import { ButtonProps, PopupData } from "@/config/types";
import Button from "../ui/Button";
import Icon from "@/lib/svg";
import { hidePopup } from "@/store/common";
import { useDispatch } from "react-redux";
import Image from "next/image";

type PopupProps = {
  messageData: PopupData;
  open: boolean;
  classAdditions?: Record<any, any>;
  contentDivStyle?: string;
  closePopup?: () => void;
};

const Popup = ({
  messageData,
  open,
  classAdditions,
  contentDivStyle,
  closePopup,
}: PopupProps): JSX.Element => {
  const { title, description, content, btnArray, head, deviceIcon, image } =
    messageData;
  const dispatch = useDispatch();
  return (
    <>
      {open && (
        <div
          role="presentation"
          className={`fixed bg-black bg-opacity-50  inset-0 justify-center items-center flex outline-none focus:outline-none z-50 ${
            classAdditions && classAdditions.background
          }`}
          onClick={(): void => closePopup && closePopup()}
        >
          <div
            role="presentation"
            className={`
            ${
              classAdditions ? classAdditions.popupContainer : "w-11/12"
            } popup-container flex flex-col mx-3 my-4 rounded-lg`}
            onClick={(e): void => e.stopPropagation()}
            onKeyDown={(e): void => e.stopPropagation()}
          >
            <div
              className={`${
                classAdditions && classAdditions.popup
              }  flex flex-col bg-white outline-none`}
            >
              <div className="flex">
                <span className={head ? head.icon.className : ""}>
                  {" "}
                  <Icon
                    type={head ? head.icon.type : "cross-icon"}
                    fill={head ? head.icon.fill : ""}
                  />
                </span>
              </div>

              <div className={` text-base mx-3 my-4`}>
                {title && <p className={`font-bold`}>{title}</p>}

                {description && (
                  <p className="text-gray-400 my-3 mx-2 text-m">
                    {description}
                  </p>
                )}
              </div>
              <hr />

              {btnArray?.length > 0 && (
                <div
                  className={`flex items-center p-3 ${
                    content && content.length > 0
                      ? "justify-center"
                      : "justify-evenly"
                  }  ${classAdditions && classAdditions.buttonContainer}`}
                >
                  {btnArray.map(
                    (value: ButtonProps, index: number): JSX.Element => {
                      return (
                        <PopupButton
                          value={value}
                          closePopup={closePopup}
                          key={index.toString()}
                          buttonClass={classAdditions && classAdditions.button}
                        />
                      );
                    }
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      )}
      <style jsx>{`
        .popup-container {
          position: relative;
          z-index: 100;
          max-height: 500px;
          // overflow-y: scroll;
          -ms-overflow-style: none; /* Internet Explorer 10+ */
          scrollbar-width: none; /* Firefox */
        }
        .popup-container::-webkit-scrollbar {
          display: none;
        }
        // .popup {
        //   border-radius: 10px;
        // }
        .flex-child {
          flex: 1;
        }
        .flex-child:first-child {
          margin-right: 20px;
        }
        .device-icon {
          width: 70px;
          height: 70px;
          border-radius: 50%;
        }
        @media screen and (max-width: 768px) {
          .popup-container {
            max-height: 700px;
          }
        }
      `}</style>
    </>
  );
};

type PopupButtonProps = {
  value: ButtonProps;
  buttonClass?: string;
  closePopup?: () => void;
};

export const PopupButton = ({
  value,
  buttonClass = "",
  closePopup,
}: PopupButtonProps): JSX.Element => {
  const handleClick = () => {
    const onClick = value.onClick || closePopup;
    onClick && onClick();
  };

  return (
    <div className={`${buttonClass}`}>
      <Button
        className={value.className}
        type={value.type}
        onClick={handleClick}
        disabled={value.disabled}
        size={value.size}
        variant={value.variant}
        loading={value.loading}
      >
        {value.children}
      </Button>
    </div>
  );
};

export default Popup;
