/* eslint-disable react/no-children-prop */
import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import FormRender from "@/utils/form/formRender";
import React, { ChangeEvent, Fragment, useEffect, useState } from "react";
import Button from "../ui/Button";
import { filterFormBtnConfig } from "@/feature/resources/config/filterConfig";
import SearchBar from "./SearchBar";

const Filter = ({
  config,
  setPopup,
  setValues,
  values,
  filterHandler,
  btnConfig,
  selectedFilter,
  resetFilterHandler,
}: {
  config: any;
  setPopup: any;
  filterHandler?: any;
  setValues?: any;
  values?: any;
  btnConfig: any;
  selectedFilter: any;
  resetFilterHandler: any;
}) => {
  const [selected, setSelected] = useState<number | null>();
  const [selectedItem, setSelectedItem] = useState<any>();
  const [filterData, setFilterData] = useState<any>();
  const formBtnConfig = filterFormBtnConfig();
  const handleClick = (key: string, value: string, index: number) => {
    selectedFilter(key, value);
    setSelectedItem(index);
  };

  useEffect(() => {
    setSelectedItem([]);
  }, [selected]);

  const handleSubmit = (item: any, value: string) => {
    selectedFilter(value, item);
    setSelected(null);
  };
  return (
    <Fragment>
      <div
        role="presentation"
        className={`filter justify-center items-center flex outline-none focus:outline-none`}
        onClick={setPopup}
      />
      <div className="filter-popup bg-white font-medium border">
        {Object.keys(config)?.map((key, index): any => {
          const value = config[key].val;
          const option = config[key].options;
          const formData = config[key].form;
          const func = config[key].function;
          const optVal = config[key].optionValue;
          const search = config[key].searchBar;

          const getValue = () => {
            if (func) {
              return option
                ? func(option, value, handleClick, selectedItem)
                : "-";
            } else if (formData) {
              return (
                <FormRender
                  formData={formData}
                  submitForm={(data: any) => handleSubmit(data, value)}
                  btnConfig={formBtnConfig}
                  btnStyle={true}
                />
              );
            } else if (Array.isArray(option)) {
              const result = (filterData?.length > 0 ? filterData : option).map(
                (entry: any, index: number) => {
                  return (
                    <div
                      key={index}
                      className={`${
                        selectedItem === index ? "selectedItem" : "not-selected"
                      } px-3`}
                      onClick={() => handleClick(value, entry[optVal], index)}
                    >
                      {entry[optVal]}
                    </div>
                  );
                }
              );
              return <div className="text-start m-1">{result}</div>;
            }
          };
          const data = getValue();
          const handleSearchFilter = (event: ChangeEvent<HTMLInputElement>) => {
            const { value } = event.target;

            const filterdData = option.filter(
              (item: any) =>
                item[optVal].toLowerCase()?.indexOf(value.toLowerCase()) > -1
            );
            setFilterData(filterdData);
          };

          return (
            <div
              className={`py-2 px-6 cursor-pointer ${
                selected === index ? "selected" : "not-selected"
              }`}
              key={index}
              onClick={() => setSelected(index)}
            >
              {key}
              {selected === index && (
                <div className={`sub-popup`}>
                  {search ? (
                    <>
                      <div className="fixed mx-1 mt-0.5">
                        <SearchBar
                          type="text"
                          placeholder={search.placeHolder}
                          onChange={(event: any) => handleSearchFilter(event)}
                          width="200px"
                        />
                        <hr />
                      </div>
                      <div className="mt-11"> {data}</div>
                    </>
                  ) : (
                    data
                  )}
                </div>
              )}
            </div>
          );
        })}
        <div className="flex items-center gap-2">
          {btnConfig?.map((btn: any, index: number) => (
            <Button
              type={btn.type}
              size={btn.size}
              onClick={
                btn.type === "reset" ? resetFilterHandler : filterHandler
              }
              variant={btn.variant}
              className={btn.className}
              precedingText={btn.precedingText}
              loading={btn.loading}
              disabled={btn.disabled}
              key={index}
            >
              {btn.children}
            </Button>
          ))}
        </div>
      </div>

      <style>{`
             .filter {
                position:fixed;
                background-color:rgba(0, 0, 0, 0.5);
                inset: 0px;
                z-index:10;
              }
              
              .filter-popup {
                position:absolute;
                right:20px;
                z-index:20;
             }
              .selected {
              border-radius: 5px;
               //color:white;
                margin:5px;
                background-color:${COLOR.RED};
              }
              .not-selected {
                background-color:${COLOR.WHITE};
                color:black;
              }
              .selectedItem{
                border-radius: 5px;
                background-color:${COLOR.GREEN};
              }

              .sub-popup{
                width:100%;
                max-height:200px;
                position:absolute;
                z-index:20;
                right:220px;
                border-radius: 8px;
                background-color:white;
                overflow:scroll;
                cursor:pointer;
               
            }
            .sub-popup::-webkit-scrollbar {
              display: none;
            }

            `}</style>
    </Fragment>
  );
};

export default Filter;
