"use client";
import React, { useEffect, useState } from "react";
import SubMenu from "@/shared-components/components/SubMenu";
import { useDispatch, useSelector } from "react-redux";
import {
  SetSelectedSubmenuItem,
  SetSelectedMenuItem,
} from "@/store/sidebarslice";
import useSidebar from "@/constants/sidebar";
import { useRouter } from "next/navigation";
import Image from "next/image";
import logo from "../../../public/images/image 2.svg";
import profile from "../../../public/images/profile.png";
import { clearLocalStorage, getItemFromLocalStorage } from "@/utils";

export default function Sidebar() {
  const dispatch = useDispatch();
  const router = useRouter();
  const [name, setName] = useState<string>();
  const [id, setId] = useState<string>();
  const sidebar: any = useSelector((state: any) => state.sidebar);

  const Sidebar = useSidebar(id);
  useEffect(() => {
    setName(getItemFromLocalStorage("user_name"));
    setId(getItemFromLocalStorage("user_id"));
  }, []);
  const logoutHandler = () => {
    clearLocalStorage();
    router.push("/");
  };
  return (
    <>
      <div className="flex flex-1 flex-col gap-6 mt-4">
        <div className="flex flex-col gap-4">
          <div className="flex justify-between items-center">
            <Image src={logo} alt={"404"} />
            <div className="flex justfy-between items-center">
              <div>
                <button
                  id="ButtonBase"
                  className="overflow-hidden flex flex-col justify-center w-8 shrink-0 h-8 items-center rounded-lg"
                >
                  <img
                    src="https://file.rendit.io/n/kc8jALoaAn9pGulZVGp5.svg"
                    id="Noti"
                    className="w-4"
                  />
                </button>
              </div>
              <div>
                <button
                  id="ButtonBase1"
                  className="overflow-hidden flex flex-col justify-center w-8 shrink-0 h-8 items-center rounded-lg"
                >
                  <img
                    src="https://file.rendit.io/n/sBlvpGSOlkkzBqbCbgVl.svg"
                    id="Menuopen"
                    className="w-4"
                  />
                </button>
              </div>
            </div>
          </div>

          <div className="border-solid border-[#2d2d31] flex flex-row gap-2 h-8 shrink-0 items-center px-2 border rounded-lg">
            <img
              src="https:file.rendit.io/n/UKOQ1q3zCKeruPRR5vtW.svg"
              id="Search"
              className="w-4 shrink-0"
            />
            <div
              id="Input"
              className="text-sm font-['Inter'] font-medium leading-[16px] text-[#66666e]"
            >
              Search
            </div>
          </div>
        </div>

        <div>
          {Sidebar?.length > 0 &&
            Sidebar.map((item: any) => {
              return (
                <div key={item.label} className={"mb-2"}>
                  <SubMenu
                    item={item}
                    toggleSubMenu={() => {
                      dispatch<any>(SetSelectedSubmenuItem(""));
                      if (sidebar.selectedMenuItem) {
                        if (sidebar.selectedMenuItem !== item.label) {
                          dispatch<any>(SetSelectedMenuItem(item.label));
                          // setOpenedSubMenu(item.label);
                        } else {
                          dispatch<any>(SetSelectedMenuItem(""));
                        }
                      } else {
                        dispatch<any>(SetSelectedMenuItem(item.label));
                      }
                    }}
                    openedSubMenu={sidebar.selectedMenuItem}
                  />
                </div>
              );
            })}
        </div>
      </div>

      <div className=" flex justify-between border rounded-md p-2 mb-2">
        <div className="flex gap-3 items-center">
          <Image src={profile} alt={"profile"} width={25} height={25} />
          <div className="text-m ">{name}</div>
        </div>
        <div>
          <button
            id="Button1"
            className="flex flex-col justify-center w-8 shrink-0 h-8 items-center"
            onClick={logoutHandler}
          >
            <Image
              src={"https://file.rendit.io/n/0HI9jAFSd7rI1iyv4am4.svg"}
              alt={"logout"}
              width={20}
              height={20}
            />
          </button>
        </div>
      </div>
    </>
  );
}
