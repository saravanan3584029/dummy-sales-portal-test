import React from "react";
import { CardProps } from "@/config/types";
import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import Checkbox from "../ui/Checkbox";

const Card = ({
  data,
  config,
  columns,
  noBorder = false,
  gridCols,
  showDetailsIcon = false,
  showPopup,
  detailsPopup,
  showDetailView,
  showCheckBox,
  checkedData,
  selectData,
}: CardProps): JSX.Element => {
  const getData = () => {
    if (Array.isArray(data)) {
      return data;
    } else {
      return [data];
    }
  };

  const cardData = getData();  

  return (
    <>
      <div className={`my-4 card-container`}>
        {cardData.length > 0 ? (
          cardData.map((item: any, index: number) => {
            const checked = checkedData?.some(
              (key: { _id: string }) => item._id === key._id
            );
            return (
              <div
                className={`card-content  ${
                  showCheckBox && checked ? "bg-blue-100" : "bg-white"
                } items-center`}
                key={index}
              >
                <div
                  className={`absolute right-0 ${
                    showCheckBox ? "mt-3 mr-2" : " mt-6 mr-4 "
                  }`}
                >
                  {showCheckBox ? (
                    <div className={`${checked ? "checked" : ""}`}>
                      <Checkbox
                        name={"checkbox"}
                        className=""
                        onChange={() => selectData(item)}
                        checked={checked}
                        visible={true}
                      />
                    </div>
                  ) : (
                    <div className="flex items-center gap-4">
                      {showDetailsIcon && (
                        <>
                          <Icon
                            type="expand"
                            width="12"
                            height="12"
                            onClick={() => showDetailView(item)}
                          />
                          <Icon
                            type="more-vert"
                            width="18"
                            onClick={() => showPopup(item)}
                          />
                        </>
                      )}
                    </div>
                  )}
                  {detailsPopup && detailsPopup(item)}
                </div>

                <div
                  className={`grid ${
                    columns ? `grid-cols-${columns}` : "grid-cols-2"
                  } px-6 py-5`}
                >
                  {Object.keys(config).map((key) => {
                    const value = config[key].val;
                    const entry = config[key].entry;
                    const className = config[key].class;
                    const icon = config[key].icon;
                    const func = config[key].function;

                    const getValue = () => {
                      if (func) {
                        let entryData;
                        if (
                          item[value] &&
                          Array.isArray(item[value]) &&
                          entry
                        ) {
                          entryData =
                            item[value] &&
                            item[value]?.map((e: any) => {
                              return func(e?.[entry]);
                            });
                          return entryData.length > 0 ? entryData : func([]);
                        } else {
                          return item[value] ? func(item[value]) : "-";
                        }
                      } else if (Array.isArray(item[value])) {
                        const data = item[value].map(
                          (val: any, index: number) => {
                            return <div key={index}>{val}</div>;
                          }
                        );
                        return data.length > 0 ? data : "-";
                      } else if (typeof item[value] === "object" && entry) {
                        return item[value]?.[entry];
                      } else {
                        if (Array.isArray(value)) {
                          const val = value?.map((ele: any, index: number) => {
                            return <div key={index}>{item[ele]}</div>;
                          });
                          return val;
                        }
                        return item[value] || "-";
                      }
                    };
                    const actualData = getValue();

                    return (
                      !Array.isArray(config[key]) && (
                        <div className={` ${className} `}>
                          {actualData && icon ? (
                            <div className=" flex gap-2 items-center">
                              <span className={`${icon.class}`}>
                                {" "}
                                <Icon
                                  type={icon.type}
                                  width={icon.width}
                                  height={icon.height}
                                  fill={icon.fill}
                                  className={icon.class}
                                />
                              </span>
                              {actualData}
                            </div>
                          ) : (
                            actualData
                          )}
                        </div>
                      )
                    );
                  })}
                </div>
              </div>
            );
          })
        ) : (
          <div className="text-center">{"No result found"}</div>
        )}
      </div>

      <style>{`
       .card-container{
       
        display:grid;
        grid-template-columns: ${
          gridCols
            ? `repeat(${gridCols},minmax(0,1fr))`
            : "repeat(auto-fill, minmax(350px, 1fr))"
        };
        row-gap: 1rem;
        column-gap: 2rem;
       
      }
      
      .card-content{
        position:relative;
        border:1.5px solid ${COLOR.GRAY_1} ;
        border-radius: 8px;

      }
      .card-content:hover{
        box-shadow: 0 8px 13px rgba(0, 0, 0, 0.1); 
      }
     
      .card-header{
        position:absolute;
        left: 80%;
        margin-top:30px;
        z-index:5;
      }
      .checked{
        left: 90%;
        margin-top:0px;

      }
      
    .outline_bold{
      border-radius:8px;
      background-color:#F4F4F4;
      box-shadow: 0px 5px 0px 0px #E3E3E4;
       border: 1px solid  #E3E3E4;
    }
    
    `}</style>
    </>
  );
};

export default Card;
