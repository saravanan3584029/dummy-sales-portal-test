import Icon from "@/lib/svg";
import React, { useEffect, useState } from "react";
import Tabs from "@/shared-components/ui/Tabs";
import Notes from "@/shared-components/components/Note";
import { useDispatch, useSelector } from "react-redux";
import Card from "./Card";
import { vendorResourceConfig } from "@/feature/vendors/config/vendorDetailViewConfig";
import { clientResourceConfig } from "@/feature/clients/config/clientCardConfig";
import { hideLoader, showLoader } from "@/store/common";
import { setResourceTabData } from "@/store/vendor";

const DetailView = ({
  detailView,
  heading,
  setShowDetail,
  selectedItem,
  headerConfig = [],
  detailConfig = [],
  tabsConfig,
  handleExpand,
  handleDownload,
  handleComment,
  handleResourceData,
}: {
  detailView: Boolean;
  heading: string;
  setShowDetail?: any;
  selectedItem: any;
  handleDownload?: any;
  headerConfig?: any;
  detailConfig?: any;
  handleComment?: any;
  tabsConfig?: any;
  handleExpand?: any;
  handleResourceData?: any;
}) => {
  const [view, setView] = useState<Boolean>(false);

  const handleView = () => {
    setView(false);
    setShowDetail(!detailView);
  };



  return (
    <>
      {detailView && (
        <>
          <div
            role="presentation"
            className={`popup-wrapper justify-center items-center flex fixed inset-0 z-10 outline-none focus:outline-none`}
            onClick={handleView}
          />
          <div className="detail-view px-4 ">
            <div className="mx-3">
              <div className="flex items-center gap-2 mt-5">
                <div className="border rounded-md p-2">
                  <Icon type="close" width="18" onClick={handleView} />
                </div>
                <div className="font-semibold"> {heading}</div>
              </div>
              <br />
              <div className="grid grid-cols-3">
                {Object.keys(headerConfig)?.map((key, index): any => {
                  const value = headerConfig[key].val;
                  const entry = headerConfig[key].entry;
                  const className = headerConfig[key].class;
                  const func = headerConfig[key].function;

                  const getValue = () => {
                    if (func) {
                      return func(selectedItem[value]);
                    } else if (
                      typeof selectedItem[value] === "object" &&
                      entry
                    ) {
                      return selectedItem[value][entry];
                    } else {
                      return selectedItem[value];
                    }
                  };

                  const data = getValue();
                  return (
                    <div key={index} className={`${className}`}>
                      {data}
                    </div>
                  );
                })}
              </div>

              <div className="mt-5">
                <Tabs config={tabsConfig}>
                  {tabsConfig.map((el: any, index: any) => {
                    return (
                      <div key={index} content={el.name}>
                        <hr />

                        <div>
                          {el.name === "Details" && (
                            <div className="detail-view-body  mx-3 my-3 ">
                              {Object.keys(detailConfig).map(
                                (key, index): any => {
                                  const values = detailConfig[key].val;
                                  const entry = detailConfig[key].entry;

                                  const func = detailConfig[key].function;
                                  const line = detailConfig[key].line;
                                  const keyClass = detailConfig[key].key;
                                  const valueClass = detailConfig[key].value;
                                  const className = detailConfig[key].class;
                                  const getValue = (): any => {
                                    if (func) {
                                      return values
                                        ? func(
                                            selectedItem[values],
                                            handleExpand,
                                            handleDownload,
                                            setView,
                                            view
                                          )
                                        : "-";
                                    } else if (
                                      typeof selectedItem[values] ===
                                        "object" &&
                                      entry
                                    ) {
                                      return selectedItem[values][entry];
                                    } else {
                                      return selectedItem[values];
                                    }
                                  };
                                  const value = getValue();
                                  return (
                                    <div key={index}>
                                      <div
                                        className={`grid grid-cols-3 ${className}`}
                                      >
                                        <div
                                          className={`${keyClass} text-gray-400`}
                                        >
                                          {key}
                                        </div>
                                        <div className={`${valueClass}`}>
                                          {value}
                                        </div>
                                      </div>
                                      {line && <hr className="my-3" />}
                                    </div>
                                  );
                                }
                              )}
                            </div>
                          )}
                          {el.name === "Resources" && (
                            <>
                              <Card
                                data={selectedItem.resources}
                                config={vendorResourceConfig()}
                                columns={5}
                              />
                            </>
                          )}
                          {el.name === "Requirements" && (
                            <div className="cursor-pointer">
                              {selectedItem?.requirements?.length>0?
                              selectedItem?.requirements?.map(
                                (el: any, index: number) => {
                                  return (
                                    <div key={index}>
                                      <Card
                                        data={el}
                                        config={clientResourceConfig()}
                                        columns={8}
                                      />
                                    </div>
                                  );
                                }
                              ):(
                                <div className="mt-2 text-center">No Requiremenst Found</div>
                              )}
                            </div>
                          )}
                          {el.name === "Notes" && (
                            <Notes
                              handleComment={handleComment}
                              notesData={selectedItem}
                              width="100"
                            />
                          )}
                          {el.name === "Alignments" && el.name}
                          {el.name === "Resources Aligned" && el.name}
                        </div>
                      </div>
                    );
                  })}
                </Tabs>
              </div>
            </div>
          </div>
        </>
      )}

      <style>{`
 .detail-view{
    width:35%;
    height:100%;
    background-color:white;
    position:absolute;
    top:0;
    right:0;
    bottom:0;
    z-index:20;
    overflow:scroll;
  }
  `}</style>
    </>
  );
};

export default DetailView;
