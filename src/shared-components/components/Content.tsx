"use client";
const Content = ({ children }: { children: any }) => {
  return (
    <div className="content-children px-5 overflow-scroll">
      {children}
      <style jsx>{`
        .content-children {
          width:100%;
          height: 100%;
          background: white;
        }
        .content-children::-webkit-scrollbar {
          display: none;
        }
        .content-children {
          -ms-overflow-style: none; /* IE and Edge */
          scrollbar-width: none; /* Firefox */
        }
      `}</style>
    </div>
  );
};

export default Content;
