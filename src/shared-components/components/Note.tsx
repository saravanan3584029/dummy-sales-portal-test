/* eslint-disable react/no-children-prop */
import Icon from "@/lib/svg";
import React, { useState } from "react";
import { COLOR } from "@/constants/theme";
import Image from "next/image";
import url from "../../../public/images/profile.png";
import { getTimeDifference } from "@/utils";
import NotesForm from "../ui/NotesForm";
import {
  notesBtnconfig,
  notesPublishconfiguration,
  notesReplyConfiguration,
} from "@/feature/resources/config/detailViewConfig";
import { useSelector } from "react-redux";

export default function Notes({
  handleComment,
  notesData,
  width,
}: {
  notesData: any;
  width?: string;
  handleComment: any;
}) {
  const [isReplyOnClick, setIsReplyOnClick] = useState<boolean>(false);
  const [selectedReply, setSelectedReply] = useState<number | null>();
  const [showReplies, setShowReplies] = useState<boolean>(false);
  const [reply, setReply] = useState<number | null>();
  const resourceStore = useSelector((store: any) => store.resource.resource);

  return (
    <div className="notes mt-5">
      <div>
        <div className="flex items-center gap-2">
          <span className="font-bold">Notes</span>{" "}
          <Icon type={"comment"} width="15" height="18" />
          <span className="text-gray-400">Comments</span>
        </div>
        <NotesForm
          btnStyle="btn-publish"
          formStyle="form-note mt-4"
          noteConfig={notesPublishconfiguration}
          btnConfig={notesBtnconfig("publish")}
          submitForm={handleComment}
        />
      </div>
      <div className="mt-2">
        {notesData &&
          notesData.notes.map((entry: any, index: number) => {
            const formattedDate = entry?.publishedAt
              ? getTimeDifference(entry?.publishedAt)
              : "";
            return (
              <div key={index} className="">
                <div className="flex items-center gap-3 mt-2">
                  <div>
                    {" "}
                    <Image src={url} alt={"profile"} width={30} height={30} />
                  </div>
                  <div className="flex items-center gap-2 mb-4">
                    <div className="text-m">{entry.publishedBy}</div>
                    <div className="text-gray-400 text-sm">
                      {formattedDate} ago
                    </div>
                  </div>
                </div>
                <div className="note-data">
                  <div className=" text-gray-400"> {entry.message}</div>
                  <div>
                    {entry?.reply?.length > 0 ? (
                      <div className="flex items-center gap-2 text-sm font-medium ">
                        <div
                          className="flex item-center gap-2"
                          onClick={() => {
                            setShowReplies(!showReplies);
                            setSelectedReply(index);
                          }}
                        >
                          {selectedReply === index && showReplies ? (
                            <Icon type="chevron-up" width="15" height="15" />
                          ) : (
                            <Icon type="chevron-down" width="15" height="15" />
                          )}
                          <div> {entry.reply.length} Replies </div>
                        </div>
                        <div>
                          {index !== reply && (
                            <div className="flex items-center gap-2">
                              {" "}
                              <div> | </div>
                              <div
                                onClick={() => {
                                  setIsReplyOnClick(true);
                                  setReply(index);
                                }}
                              >
                                Reply
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    ) : (
                      <div>
                        {index !== reply && (
                          <div
                            onClick={() => {
                              setIsReplyOnClick(true);
                              setReply(index);
                            }}
                          >
                            Reply
                          </div>
                        )}
                      </div>
                    )}

                    {selectedReply === index &&
                      showReplies &&
                      entry.reply.map((reply: any, index: any) => {
                        const formattedDate = getTimeDifference(
                          reply.repliedAt
                        );
                        return (
                          <>
                            <div key={index} className="">
                              <div className="flex items-center gap-3 mt-2">
                                <div>
                                  <Image
                                    src={url}
                                    alt={"profile"}
                                    width={20}
                                    height={20}
                                  />
                                </div>
                                <div className="flex items-center gap-2 mb-4">
                                  <div className="text-m">
                                    {reply.repliedBy}
                                  </div>
                                  <div className="text-gray-400 text-sm">
                                    {formattedDate} ago
                                  </div>
                                </div>
                              </div>
                              <div className="note-data">
                                <div className=" text-gray-400">
                                  {reply.message}
                                </div>
                              </div>
                            </div>
                          </>
                        );
                      })}
                    {index === reply && isReplyOnClick && (
                      <div>
                        <div
                          role="presentation"
                          className={`reply-wrapper justify-center items-center flex outline-none focus:outline-none`}
                          onClick={() => {
                            setIsReplyOnClick(false);
                            setSelectedReply(null);
                            setReply(null);
                          }}
                        />
                        <NotesForm
                          btnStyle="btn-reply"
                          formStyle="reply-note mt-4"
                          noteConfig={notesReplyConfiguration}
                          btnConfig={notesBtnconfig("Add", "small")}
                          width={"80%"}
                          height={"90px"}
                          submitForm={(data: string) => {
                            handleComment(data, { id: entry._id });
                            setReply(null);
                          }}
                        />
                      </div>
                    )}
                  </div>
                </div>
              </div>
            );
          })}
      </div>
      <style>{`
      .notes{
        width:${width || "75"}%;
      }
      .form-note{
        height:140px;
        position:relative;
        background-color:${COLOR.GRAY_1};
        border-radius:8px;
      }
      .btn-publish{
        position:absolute;
        top:80px;
        right:15px;
      }

      .note-data{
        margin-top:-20px;
        margin-left:40px
      }
      .reply-wrapper {
        position:fixed;
        inset: 0 0 0 70%;
        z-index:10;
       }
      .reply-note{
        height:50px;
        position:relative;
        background-color:${COLOR.GRAY_1};
        border-radius:8px;
        z-index:20;
      }
      .btn-reply{
        position:absolute;
        top:10px;
        right:5px;

      }
      `}</style>
      <br /> <br />
    </div>
  );
}
