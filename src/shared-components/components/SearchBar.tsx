import Icon from "@/lib/svg";
import Input from "../ui/Input";
import { COLOR } from "@/constants/theme";

const SearchBar = ({
  name,
  type = "text",
  placeholder,
  className,
  onChange,
  onBlur,
  width,
  filterIcon = true,
  setIsFilterOpen,
  isFilterOpen,
  visible = true,
}: {
  name?: string;
  type: string;
  placeholder: string;
  className?: any;
  width?: string;
  onChange?: any;
  filterIcon?: boolean;
  onBlur?: (...args: unknown[]) => unknown;
  setIsFilterOpen?: any;
  isFilterOpen?: boolean;
  visible?: boolean;
}) => {
 

  return (
    <>
      <div
        className={`searchbar flex items-center px-2 py-2 ${className}`}
      >
        <div className="flex gap-2 items-center ">
          <Icon type="search" width="13" height="15" />
          <Input
            type={`text`}
            placeholder={placeholder}
            onChange={onChange}
            variant="no-border"
            name=""
          />
        </div>
       
      </div>
      <style jsx>{`
        .searchbar {
          background-color:white;
          height: 38px;
          width: ${width};
        }
      `}</style>
    </>
  );
};
export default SearchBar;
