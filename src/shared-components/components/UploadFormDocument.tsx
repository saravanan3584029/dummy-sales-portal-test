import { useState } from "react";

function UploadDocumentForm({ handleChange, ndaFile }: any) {
  return (
    <div
      className="self-center flex flex-col gap-3 items-start"
      style={{ marginLeft: "21rem" }}
    >
      <div
        id="Label3"
        className="text-sm font-['Inter'] font-medium leading-[16px] text-[#66666e]"
      >
        NDA
      </div>
      <div className="border-solid border-[#e3e3e4] flex flex-row justify-between pl-3 w-[320px] items-center border rounded-lg">
        <div className="text-sm font-['Inter'] font-medium leading-[16px] text-[#aaaaaf]">
          Upload Attachment
        </div>
        <label
          htmlFor="fileInput"
          className="self-start flex flex-col justify-center w-20 shrink-0 h-8 items-center my-0 cursor-pointer"
        >
          <div
            className="text-center text-sm font-['Inter'] font-semibold leading-[16px] text-[#1a68f3]"
            onChange={(e) => {
              const target = e.target as HTMLInputElement;
              handleChange({
                target: {
                  name: "ndaFile",
                  value: target.files && target.files[0],
                },
              });
            }}
          >
            Browse
          </div>
          <input
            type="file"
            id="fileInput"
            accept=".pdf, .doc, .docx" // Specify the allowed file types
            onChange={(e) => {
              const target = e.target as HTMLInputElement;
              handleChange({
                target: {
                  name: "ndaFile",
                  value: target.files && target.files[0],
                },
              });
            }}
            style={{ display: "none" }}
          />
        </label>
      </div>
      {ndaFile && (
        <div className="mt-3 text-red-500">
          <span>Selected NDA File: {ndaFile.name}</span>
        </div>
      )}
    </div>
  );
}

export default UploadDocumentForm;
