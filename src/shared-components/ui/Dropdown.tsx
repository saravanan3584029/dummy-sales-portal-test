import { ChangeEvent, useEffect, useRef, useState } from "react";
import { useOnClickOutside } from "../../feature/hooks";
import { customizeEvent, noOperation } from "../../utils";
import Icon from "@/lib/svg";
import ValidationError from "../components/ValidationError";
import SearchBar from "../components/SearchBar";

type DropdownProps = {
  name?: string;
  placeholder?: any;
  options: {
    id?: string;
    label?: string;
    value?: string;
    className?: string;
    icon?: { type: string; fill: string };
    disabled?: boolean;
    selected?: boolean;
    hidden?: boolean;
  }[];
  width?: string;
  value?: any;
  error?: string;
  touched?: boolean;
  val?: any;
  className?: string;
  height?: string;
  icon?: any;
  searchBar?: any;
  onChange?: (...args: unknown[]) => unknown;
  onBlur?: (...args: unknown[]) => unknown;
};

const Dropdown = ({
  name,
  placeholder,
  options,
  width,
  value,
  className,
  height,
  error,
  icon,
  onBlur,
  touched,
  onChange,
  searchBar,
  val,
}: DropdownProps): JSX.Element => {
  const ref: any = useRef();
  const [dropdown, setDropdown] = useState<boolean>(false);
  const [updatedOptions, setUpdatedOptions] = useState(options);

  useEffect(() => {
    if (value === "" || options) {
      setUpdatedOptions(options);
    }
  }, [options, value]);

  const showDropdown = (): void => {
    setDropdown(!dropdown);
  };

  // const onKeyDown = (event, option) => {
  //   const itemIndex = options.indexOf(option);
  //   if (event.keyCode === 13) {
  //     onClickOption(event, option);
  //   } else if (event.keyCode === 38) {
  //     if (itemIndex === 0) {
  //       setSelectedOption(options[options.length - 1]);
  //     } else {
  //       setSelectedOption(options[itemIndex - 1]);
  //     }
  //   } else if (event.keyCode === 40 || event.keyCode === 9) {
  //     if (itemIndex + 1 === options.length) {
  //       setSelectedOption(options[0]);
  //     } else {
  //       setSelectedOption(options[itemIndex + 1]);
  //     }
  //   }
  // };

  const onClickOption = (event: any, option: any): void => {
    let itemIndex: number;
    itemIndex = options.indexOf(option);
    const newSelect = options.filter((item, index) => index !== itemIndex);
    setUpdatedOptions(newSelect);
    if (name) {
      customizeEvent(event, name, option?.value, option);
    }
    onChange && onChange(event);
    setDropdown(false);
    onBlur && onBlur(event);
  };

  useOnClickOutside(ref, (e: any): void => {
    e.stopPropagation();
    if (dropdown) {
      showDropdown();
    }
  });

  const filterData = (event: ChangeEvent<HTMLInputElement>) => {
    const value =
      event.target.value ||
      event.currentTarget.getAttribute("data-value") ||
      "";
    const filterData = options.filter(
      (data:any) => data[val]?.toLowerCase()?.indexOf(value.toLowerCase()) > -1
    );
    setUpdatedOptions(filterData);
  };

  return (
    <>
      <div
        ref={ref}
        className={`rounded-md border border-dark font-medium relative ${className} width ${
          error && touched && "border border-error"
        }`}
        style={{ zIndex: dropdown ? 10 : "unset" }}
      >
        <div
          id={name}
          className={`relative flex items-center justify-between selectbox`}
          // onKeyDown={noOperation}
          onClick={(e) => {
            showDropdown();
            if (dropdown && !value) {
              onClickOption(e, value);
            }
          }}
          role="button"
          tabIndex={0}
        >
          <div className="flex justify-center items-center gap-2">
            {icon && (<Icon type={icon.type} /> || icon.type)}
            <div className={`truncate pl-4  ${value ? "" : "text-disable"}`}>
              {value && value.length !== 0 ? (
                value
              ) : typeof placeholder !== "string" ? (
                <div className="flex items-center gap-2">
                  <Icon type={placeholder.type} />
                  <span className="text-gray-400">{placeholder.name}</span>
                </div>
              ) : (
                <div className="text-gray-400">{placeholder}</div>
              )}
            </div>
          </div>
          <div className="chevron">
            {dropdown ? (
              <Icon
                type="chevron-up"
                className="fill-current mr-2"
                height="16"
                width="16"
              />
            ) : (
              <Icon
                type="chevron-down"
                height="16"
                width="16"
                className="fill-current mr-2"
              />
            )}
          </div>
        </div>

        <div
          style={{ display: dropdown ? "block" : "none" }}
          className={`items border rounded-md w-full absolute`}
        >
          {searchBar && (
            <>
              <SearchBar
                name={searchBar.name}
                type={searchBar.type}
                placeholder={searchBar.placeholder}
                className={searchBar.className}
                filterIcon={searchBar.filterIcon}
                onChange={searchBar.onChange || filterData}
                onBlur={searchBar}
                visible={searchBar.visible}
              />
              <hr />
            </>
          )}
          {updatedOptions?.length > 0 ? (
            updatedOptions?.map((option: any, index) => {
              return (
                <div
                  className="flex flex-row items-center gap-2"
                  key={index}
                  onClick={(event) => onClickOption(event, option)}
                  data-value={option[val]}
                  // onKeyDown={(event) => onKeyDown(event, option)}
                  role="button"
                  tabIndex={0}
                >
                  {option?.icon && (
                    <Icon type={option.icon.type} fill={option.icon.fill} />
                  )}
                  <div style={{ color: `${option?.className}` }}>
                    {/* {option.label ? option.label : option.value} */}
                    {option[val]}
                  </div>
                </div>
              );
            })
          ) : (
            <div className=" ml-4 text-justify">No result found</div>
          )}
        </div>
      </div>

      {touched && error && <ValidationError error={error} />}
      <style jsx>{`
      .width{
        width:${width}
      }
      .items{
       height:${height}px;
        overflow:scroll;
        background:#fff;
       
      }
      .items::-webkit-scrollbar {
        display:none;
      }
        .items div {
          padding: 2px;
          padding-left:10px;
          cursor: pointer;
         
        }
        .selectbox {
          border-radius:8px;
          height: 36px;
          padding-left:10px;
        }

        @media screen and (max-width: 768px) {
        .width{
          width:100%;
        }
     @media screen and (max-height: 700px) {
        .chevron {
          position: absolute;
          right: 14px;
        }

      `}</style>
    </>
  );
};

export default Dropdown;
