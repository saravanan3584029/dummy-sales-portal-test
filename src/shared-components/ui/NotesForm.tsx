/* eslint-disable react/no-children-prop */
import { useState } from "react";
import Button from "./Button";
import TextArea from "./Textarea";

const NotesForm = ({
  btnStyle,
  formStyle,
  noteConfig,
  btnConfig,
  submitForm,
  width,
  height,
}: {
  btnStyle: string;
  formStyle: string;
  width?: string;
  height?: string;
  noteConfig: any;
  btnConfig: any;
  submitForm: any;
}) => {
  const [comment, setComment] = useState<any>();
  const handleChange = (event: any) => {
    const { name, value } = event.target;
    setComment({ [name]: value });
  };
  const handleSubmit = async () => {
    await submitForm(comment);
    setComment("");
  };

  return (
    <div className={`${formStyle}`}>
      <div>
        <TextArea
          key={noteConfig.id}
          placeholder={noteConfig.placeholder}
          onChange={handleChange}
          disablePaste={noteConfig.disablePaste}
          name={noteConfig.name}
          rows={noteConfig.rows}
          width={width}
          height={height}
          value={comment?.comment || comment?.reply}
          classname="h-20"
        />
      </div>

      <div className={`${btnStyle}`}>
        {btnConfig?.map((btn: any, index: number) => (
          <Button
            type={btn.type}
            size={btn.size}
            onClick={handleSubmit}
            variant={btn.variant}
            className={btn.className}
            precedingText={btn.precedingText}
            loading={btn.loading}
            disabled={btn.disabled}
            key={index}
          >
            {btn.children}
          </Button>
        ))}
      </div>
    </div>
  );
};
export default NotesForm;
