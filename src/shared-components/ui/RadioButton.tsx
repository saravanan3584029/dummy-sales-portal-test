import { COLOR } from "@/constants/theme";

type RadioButtonProps = {
  id: string | undefined;
  name: string | undefined;
  label: string | undefined;
  value?: string;
  className?: string;
  checked?: boolean;
  selectedChecked?: boolean;
  onChange?: (...args: unknown[]) => unknown;
};

const RadioButton = ({
  id,
  name,
  value,
  label,
  className,
  selectedChecked,
  checked = false,
  onChange,
  ...props
}: RadioButtonProps): JSX.Element => {
  return (
    <div className="input-radio">
      <input
        type="radio"
        id={id}
        name={name}
        checked={selectedChecked}
        value={value}
        onChange={onChange}
        {...props}
        className={`radio-oval ${className}`}
      />
      <span className="value">{label}</span>
    </div>
  );
};

export default RadioButton;
