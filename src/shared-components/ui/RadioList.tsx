import ValidationError from "../components/ValidationError";
import RadioButton from "./RadioButton";

interface RadioListProps {
  options: {
    name?: string | undefined;
    id?: string | undefined;
    label?: string | undefined;
    value?: string;
    defaultChecked?: boolean;
  }[];
  name?: string;
  error?: string;
  checked?: any;
  onChange?: (...args: unknown[]) => unknown;
}

const RadioList = ({
  options,
  name,
  onChange,
  checked,
  error,
  ...props
}: RadioListProps): JSX.Element => {
  return (
    <>
      <div className="radio-container">
        {options.map((radio, index) => (
          <div key={index}>
            <RadioButton
              id={radio.id}
              name={name}
              label={radio.label}
              value={radio.value}
              selectedChecked={checked === radio.value ? true : false}
              onChange={onChange}
              checked={radio.defaultChecked}
              {...props}
            />
          </div>
        ))}
      </div>
      {error && <ValidationError error={error} />}
      <style jsx>
        {`
          .radio-container {
            display: flex;
          }
        `}
      </style>
    </>
  );
};

export default RadioList;
