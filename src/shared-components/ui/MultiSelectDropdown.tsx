/* eslint-disable react/no-children-prop */
import React, { useState, ChangeEvent, useRef, useEffect } from "react";
import SearchBar from "../components/SearchBar";
import Checkbox from "./Checkbox";
import Image from "next/image";
import { useOnClickOutside } from "@/feature/hooks";
import Button from "./Button";
import Icon from "@/lib/svg";
import { COLOR } from "@/constants/theme";
import ValidationError from "../components/ValidationError";

type DropdownProps = {
  name?: string;
  placeholder?: any;
  options: {
    id?: string;
    label?: string;
    value?: string;
    className?: string;
    icon?: { type: string; fill: string };
    disabled?: boolean;
    selected?: boolean;
    hidden?: boolean;
  }[];
  width?: string;
  value?: any;
  error?: string;
  touched?: boolean;
  className?: string;
  height?: string;
  icon?: any;
  searchBar?: any;
  val?: any;
  inputStyle?: boolean;
  onChange?: (...args: unknown[]) => unknown;
  onBlur?: (...args: unknown[]) => unknown;
};

const DropdownComponent = ({
  name,
  placeholder,
  options,
  width,
  value,
  className,
  height,
  error,
  icon,
  onBlur,
  touched,
  onChange,
  searchBar,
  val,
  inputStyle,
}: DropdownProps): JSX.Element => {
  const ref: any = useRef();
  const [updatedOptions, setUpdatedOptions] = useState<any>();
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const [selectedItems, setSelectedItems] = useState<any>();

  useEffect(() => {
    setSelectedItems(value || []);
    setUpdatedOptions(options);
  }, [options, value]);

  const handleCheckboxChange = (item: any) => {
    let newCheckedItems;
    if (icon && icon.type) {
      const isChecked = selectedItems.some(
        (entry: any) => entry.techStack === item[val]
      );

      newCheckedItems = isChecked
        ? selectedItems.filter(
            (checkedItem: any) => checkedItem.techStack !== item[val]
          )
        : [...selectedItems, { techStack: item[val], isPrimary: false }];
    } else {
      const isChecked = selectedItems?.some(
        (entry: any) => entry === item[val]
      );
      newCheckedItems = isChecked
        ? selectedItems.filter((checkedItem: any) => checkedItem !== item[val])
        : [...selectedItems, item[val]];
      onChange && onChange(newCheckedItems);
      onBlur && onBlur(newCheckedItems);
    }
    setSelectedItems(newCheckedItems);
  };

  const handleSearchChange = (e: ChangeEvent<HTMLInputElement>) => {
    const filterData: any = options.filter(
      (data: any) =>
        data.value?.toLowerCase()?.indexOf(e.target.value.toLowerCase()) > -1
    );
    setUpdatedOptions(filterData);
  };

  const clickApplyHandle = () => {
    setIsDropdownOpen(!isDropdownOpen);
    onChange && onChange(selectedItems);
    onBlur && onBlur(selectedItems);
  };

  useOnClickOutside(ref, (e: any): void => {
    e.stopPropagation();
    if (isDropdownOpen) {
      setIsDropdownOpen(!isDropdownOpen);
    }
  });

  const handleStarClick = (data: string) => {
    const index = selectedItems.findIndex(
      (entry: any) => entry.techStack === data
    );

    if (index !== -1) {
      const updatedItems = [...selectedItems];
      updatedItems[index] = {
        ...updatedItems[index],
        isPrimary: !updatedItems[index].isPrimary,
      };
      setSelectedItems(updatedItems);
      onChange && onChange(updatedItems);
      onBlur && onBlur(updatedItems);
    }
  };

  const handleRemoveClick = (data: string) => {
    const updatedItems =
      icon && icon.type
        ? selectedItems.filter((entry: any) => entry.techStack !== data)
        : selectedItems.filter((entry: any) => entry !== data);
    onChange && onChange(updatedItems);
    setSelectedItems(updatedItems);
    onBlur && onBlur(updatedItems);
  };

  return (
    <div ref={ref}>
      <div>
        {!inputStyle ? (
          <div
            className={`flex flex-row font-medium flex-wrap ${
              !isDropdownOpen ? "inline-block" : "hidden"
            }`}
          >
            {selectedItems?.map((data: any, index: any) => {
              return (
                <div
                  key={index}
                  className="flex justify-evenly gap-3 items-center outline_border"
                >
                  {icon && icon?.type ? (
                    <Icon
                      type={icon.type}
                      width={icon.width}
                      height={icon.height}
                      fill={data.isPrimary ? COLOR.YELLOW : ""}
                      onClick={() => handleStarClick(data.techStack)}
                    />
                  ) : (
                    <Image
                      src="/images/profile.png"
                      width={20}
                      height={20}
                      alt="Picture of the author"
                    />
                  )}
                  {icon && icon?.type ? data.techStack : data}
                  <Icon
                    type="close"
                    width="18"
                    height="18"
                    onClick={() =>
                      handleRemoveClick(
                        icon && icon.type ? data.techStack : data
                      )
                    }
                  />
                </div>
              );
            })}
            <div
              className=" flex items-center gap-1 rounded outline_border w-18"
              onClick={() => setIsDropdownOpen(!isDropdownOpen)}
            >
              <Icon type="plus" />
              <span className="pr-2"> Add</span>
            </div>
          </div>
        ) : (
          <div
            id={name}
            className={`flex items-center justify-between border rounded-lg h-9`}
            onClick={() =>
              selectedItems.length === 0 && setIsDropdownOpen(!isDropdownOpen)
            }
            role="button"
            tabIndex={0}
          >
            <div className="ml-2" onClick={() => setIsDropdownOpen(false)}>
              {selectedItems?.length > 0 ? (
                <div className="flex justify-evenly gap-3 ">
                  {selectedItems.slice(0, 2)?.map((data: any, index: any) => {
                    return (
                      <div
                        key={index}
                        className="flex justify-evenly gap-1 items-center border rounded-md bg-gray-100 px-2"
                      >
                        {data}
                        <Icon
                          type="close"
                          width="18"
                          height="18"
                          onClick={() => handleRemoveClick(data)}
                        />
                      </div>
                    );
                  })}
                  {selectedItems.length > 2 && (
                    <div className="border rounded-lg bg-gray-100 px-1">
                      <span>+ {selectedItems.length - 2}</span>
                    </div>
                  )}
                </div>
              ) : (
                <p className="text-gray-400">{placeholder}</p>
              )}
            </div>
            <div
              className="chevron"
              onClick={() =>
                selectedItems.length > 0 && setIsDropdownOpen(!isDropdownOpen)
              }
            >
              {isDropdownOpen ? (
                <Icon
                  type="chevron-up"
                  className="fill-current mr-2"
                  height="16"
                  width="16"
                />
              ) : (
                <Icon
                  type="chevron-down"
                  height="16"
                  width="16"
                  className="fill-current mr-2"
                />
              )}
            </div>
          </div>
        )}
      </div>

      {/* dropdown container */}

      <div
        style={{
          display: isDropdownOpen ? (!inputStyle ? "block" : "") : "none",
        }}
        className={`container rounded mt-3 py-2 ${width}`}
      >
        {searchBar && (
          <>
            <SearchBar
              type={"text"}
              placeholder={"search"}
              name="search"
              width={searchBar.width}
              onChange={handleSearchChange}
            />
            <hr />
          </>
        )}

        {/* show inside the container for selected data  */}
        {icon &&
          icon.image &&
          selectedItems?.map((data: any, index: any) => {
            return (
              <div
                key={index}
                className="w-40 flex gap-3 justify-center items-center outline_border"
              >
                <Image
                  src="/images/profile.png"
                  width={20}
                  height={20}
                  alt="Picture of the author"
                />
                {data}
                <Icon
                  type="close"
                  width="18"
                  height="18"
                  onClick={() =>
                    handleRemoveClick(icon && icon.type ? data.techStack : data)
                  }
                />
              </div>
            );
          })}

        {/* options shows in the container */}

        {updatedOptions?.length > 0 ? (
          <div>
            {updatedOptions.map((item: any, index: number) => {
              return (
                <div key={index}>
                  <div>
                    <Checkbox
                      name="check"
                      checked={
                        icon && icon.type
                          ? selectedItems?.some(
                              (entry: { techStack: String }) =>
                                entry.techStack === item[val]
                            )
                          : selectedItems?.some(
                              (entry: any) => entry === item[val]
                            )
                      }
                      label={item[val]}
                      visible={true}
                      onChange={() => handleCheckboxChange(item)}
                    />
                  </div>
                </div>
              );
            })}
            {icon && icon.type && (
              <div className="flex flex-row items-center gap-3">
                <Button
                  children="Cancel"
                  type="button"
                  size="small"
                  variant="secondary"
                  onClick={() => setSelectedItems([])}
                />
                <Button
                  children="Apply Changes"
                  type="button"
                  size="small"
                  variant="primary"
                  onClick={clickApplyHandle}
                />
              </div>
            )}
          </div>
        ) : (
          "No result found"
        )}
      </div>

      {touched && error && <ValidationError error={error} />}
      <style>{`
        .container {
         width:${width}px;
          border-radius:10px;
          fontSize:12px;
          margin:5px;
          padding:5px;
          box-shadow: 0px 0px 12px 0px rgba(0, 0, 0, 0.06);
          height:${height}px;
          overflow:scroll;
          background:#fff;
  
        }
       .container::-webkit-scrollbar {
         display:none;
       }

      .outline_border{
        border-radius:20px;
        background-color:${COLOR.GRAY_1};
        fontSize:12px;
        margin:5px;
        padding:5px;
      }
      `}</style>
    </div>
  );
};

export default DropdownComponent;
