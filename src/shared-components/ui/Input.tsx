import Icon from "@/lib/svg";
import { useState } from "react";
import ValidationError from "@/shared-components/components/ValidationError";

type InputProps = {
  name: string;
  type?: "text" | "number" | "password" | "email" | "date";
  value?: string;
  variant?: "border" | "no-border";
  label?: string;
  autoSuggest?: boolean;
  suggestionList?: JSX.Element;
  width?: string;
  error?: string;
  touched?: boolean;
  placeholder?: string;
  pattern?: RegExp;
  maskingPattern?: any;
  forwardedRef?: any;
  autoComplete?: string;
  className?: string;
  inputStyle?: string;
  disabled?: boolean;
  maxLength?: number;
  disablePaste?: boolean;
  visible?: boolean;
  icon?: {
    type: string;
    fill?: string;
    width?: string;
    height?: string;
    label?: string;
  };
  onBlur?: (...args: unknown[]) => unknown;
  onChange?: (...args: unknown[]) => unknown;
  onKeyDown?: (...args: unknown[]) => unknown;
};

const Input = ({
  name,
  variant = "border",
  label,
  type = "text",
  value,
  autoSuggest,
  suggestionList,
  width = "100%",
  inputStyle,
  error,
  placeholder,
  forwardedRef,
  touched = false,
  className,
  maxLength,
  pattern,
  maskingPattern,
  icon,
  onChange,
  onKeyDown,
  onBlur,
  autoComplete = "off",
  visible = true,
  disablePaste,
  disabled = false,
  ...props
}: InputProps): JSX.Element => {
  const [keycode, setKeycode] = useState();
  const handleKeyPress = (event: any) => {
    const startPos = event.target.selectionStart;
    setKeycode(event.which);
    if (event.which === 32 && startPos === 0) {
      event.preventDefault();
      return;
    }

    if (type === "number") {
      if (["e", "E", "+", "-"].includes(event.key)) {
        event.preventDefault();
        return;
      }
    }
    if (typeof onKeyDown === "function") onKeyDown(event);
  };

  return (
    <>
      <div className={`${className} ${!visible && " hidden"} relative`}>
        <div className={`${label && "flex"}`}>
          {label && <label className={`mr-2`}>{label}</label>}
          <div className={`${icon ? "icon-input " : ""}`}>
            <input
              className={`${
                variant === "border"
                  ? "border border-dark input"
                  : "border-0 w-full outline-none"
              } ${disabled && !(error && touched) && "class-disable"} ${
                error && touched && "border border-error"
              } ${inputStyle}`}
              id={name}
              name={name}
              type={type}
              placeholder={placeholder}
              disabled={disabled}
              onChange={onChange}
              onKeyDown={handleKeyPress}
              ref={forwardedRef}
              onPaste={
                disablePaste
                  ? (e) => e.preventDefault()
                  : () => {
                      /** do nothing * */
                    }
              }
              autoComplete={autoComplete}
              onBlur={onBlur}
              value={value}
              {...props}
              maxLength={maxLength}
            />
            <span>
              {icon &&
                (icon.type ? (
                  <Icon
                    type={icon.type}
                    width={icon.width}
                    height={icon.height}
                    fill={icon.fill}
                  />
                ) : (
                  <span className="text-zinc-400">{icon?.label}</span>
                ))}
            </span>
          </div>
          {autoSuggest && suggestionList}
        </div>
        <div className="absolute top-8">
          {" "}
          {touched && error && <ValidationError error={error} />}
        </div>
      </div>

      <style jsx>{`
        .fully-rounded {
          border-radius: 60px !important;
        }
        .input {
          border-radius: 8px;
          height: 36px;
          padding-left: 1.5rem;
          width: ${width};
        }

        .icon-input {
          position: relative;
        }

        .icon-input span {
          position: absolute;
          left: 4px;
          top: 50%;
          transform: translateY(-50%);
        }

        .icon-input input {
          padding-left: 40px;
        }
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
          -webkit-appearance: none;
          margin: 0;
        }

        /* Firefox */
        input[type="number"] {
          -moz-appearance: textfield;
        }

        @media screen and (max-width: 768px) {
          .input {
            width: 100%;
          }
        }
      `}</style>
    </>
  );
};

export default Input;
