"use client";

import { ButtonProps } from "@/config/types";
import { BUTTON, COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import React from "react";

const Button = ({
  type = "button",
  disabled = false,
  size = "small",
  variant = "tertiary",
  className,
  onClick,
  children,
  precedingText,
  icon,
  loading,
  visible = true,
}: ButtonProps): JSX.Element => {
  return (
    <div className={`${precedingText && "mt-6 flex"} ${!visible && "hidden"}`}>
      {precedingText && <p className="text-sm mr-1">{precedingText}</p>}
      <button
        type={type}
        className={`${
          variant !== "link" && size
        } ${variant} btn-style flex justify-center items-center border-box ${className} ${
          loading && "pointer-events-none"
        } `}
        onClick={onClick}
        disabled={disabled}
      >
        {icon && <Icon type={icon} className="mr-1" />}
        {children}
        {loading && (
          <Icon type="loader" height="20" width="20" className="ml-2" />
        )}
      </button>

      <style jsx>
        {`
          .btn-style {
            font-size: 16px;
            font-style: normal;
            font-weight: 600;
            line-height: 20px;
          }
          .fully-rounded{
            border-radius:60px !important;
          }
          .logout-btn {
            font-size: 16px;
          }
          .primary {
                color: ${COLOR.WHITE};
                background: ${COLOR.BLUE};
                border-radius:5px;
          }
            .primary:focus{
                color: ${COLOR.WHITE},
                background: ${COLOR.GREEN},
                box-shadow: '0px 2px 8px #F2793029',
            }
            .primary:hover{
                background:${COLOR.BLUE_1} ;
                color: ${COLOR.WHITE};
            }
            .primary:disabled{
                color: ${COLOR.WHITE};
                background: '#45BD75';
            }
            .primary:pressed{
                color: ${COLOR.WHITE};
                background: '#37B469';
            }

          .secondary {
            color: black;
            background:white;
            border-radius: 5px;
          }
          .tertiary {
            color: white;
            background:#F04438;
            border-radius: 5px;
          }

          .disable {
            type: solid;
            color: #1A68F3;
            background: #3371EA1A;
            // border:1px solid #979797;
            border-radius: 5px;
          }
        

          .link {
            color: ${COLOR.GREEN};
          }

          .small {
            padding-left: 11px;
            padding-right: 11px;
            height: 32px;
          }

          .medium {
            height: 40px;
            padding-left: 30px;
            padding-right: 30px;
          }
          .large {
            padding-left: 48px;
            padding-right: 48px;
            height: 64px;
          }
          .link-btn {
            background: #ecfff4;
            border: 1px solid #45bd75;
            border-radius: 20px;
            margin-left:1rem;
          }
        `}
      </style>
    </div>
  );
};

export default Button;
