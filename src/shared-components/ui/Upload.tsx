import { useRef, useState } from "react";
import { customizeEvent } from "../../utils";
import Icon from "@/lib/svg";
import { COLOR } from "@/constants/theme";
import ValidationError from "../components/ValidationError";

type UploadProps = {
  name: string;
  type?: string;
  value?: any;
  placeholder?: string;
  acceptedFileTypes?: string;
  error?: string;
  touched?: boolean;
  pattern?: RegExp;
  maskingPattern?: any;
  className?: string;
  multiple?: boolean;
  browseClick?: boolean;
  icon?: {
    type: string;
    fill?: string;
    width?: string;
    height?: string;
    label?: string;
  };
  containerStyle?: string;
  onBlur?: (...args: unknown[]) => unknown;
  onChange?: (...args: unknown[]) => unknown;
  onKeyDown?: (...args: unknown[]) => unknown;
};

const Upload = ({
  name,
  type = "file",
  value,
  placeholder,
  acceptedFileTypes,
  error,
  touched = false,
  className,
  multiple,
  pattern,
  maskingPattern,
  icon,
  containerStyle,
  browseClick,
  onChange,
  onKeyDown,
  onBlur,
  ...props
}: UploadProps): JSX.Element => {
  const [keycode, setKeycode] = useState();
  const fileInputRef = useRef<HTMLInputElement>(null);
  const handleKeyPress = (event: any) => {
    const startPos = event.target.selectionStart;
    setKeycode(event.which);
    if (event.which === 32 && startPos === 0) {
      event.preventDefault();
      return;
    }

    if (type === "number") {
      if (["e", "E", "+", "-"].includes(event.key)) {
        event.preventDefault();
        return;
      }
    }
    if (typeof onKeyDown === "function") onKeyDown(event);
  };

  const onFileChange = (event: any) => {
    customizeEvent(event, name, event.target.files[0]);
    onChange && onChange(event);
    onBlur && onBlur(event);
  };

  const handleBrowseClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  return (
    <div className={` ${className} file-upload`}>
      {icon && (
        <div
          className="flex items-center text-blue-700 font-semibold"
          onClick={handleBrowseClick}
        >
          <Icon
            type={icon.type}
            width={icon.width}
            height={icon.height}
            fill={icon.fill}
          />
          <p className="ml-2">{placeholder}</p>
        </div>
      )}
      <input
        ref={fileInputRef}
        className={`upload-input`}
        id={name}
        name={name}
        accept={acceptedFileTypes}
        type={type}
        onChange={onFileChange}
        onKeyDown={handleKeyPress}
        onBlur={onBlur}
        value={undefined}
        {...props}
        multiple={multiple}
      />
      {!browseClick && (
        <div className="custom-upload ">
          <p
            className="mr-5 font-medium text-blue-500"
            onClick={handleBrowseClick}
          >
            Browse
          </p>
          <p className="ml-2">{placeholder}</p>
        </div>
      )}

      {/* <p className="mt-2 text-sm">{value?.name}</p> */}
      <p className="mt-2">{typeof value === "object" ? value.name : value}</p>

      {touched && error && <ValidationError error={error} />}
      <style>{`
        .file-upload {
          position:relative;
          top:0px;
          left:0px;
           width: 100%;
           height: 48px;
          
        }
        .upload-input {
          position: absolute;
          height: 36px;
          border-radius:8px;
          visibility: hidden;
        }
        .custom-upload {
          background: ${COLOR.WHITE};
          height: 36px;
          border: 1px solid gray;
          color: #626262;
          border-radius: 5px;
          display: flex;
          flex-direction: row-reverse;
          justify-content: space-between;
          align-items: center;
          z-index: 1;
          cursor: pointer;
        }
      `}</style>
    </div>
  );
};

export default Upload;
