import Input from "./Input";
import ToggleSwitch from "./ToggleSwitch";
import Button from "./Button";
import Autosuggest from "./Autosuggest";
import Link from "next/link";
import Textarea from "./Textarea";
import RadioList from "./RadioList";
import Upload from "./Upload";
import SearchBar from "../components/SearchBar";
import CheckboxList from "./CheckboxList";
import Dropdown from "./Dropdown";
import DropdownComponent from "./MultiSelectDropdown";

const Form = ({
  formData,
  handleChange,
  handleBlur,
  formValues,
  touched,
  errors,
  updatedData,
  setUpdatedData,
  fieldDomRefs,
  repeatableArr,
  touchedArr,
  formFields,
  setFormFields,
  formLabel,
  isRepeatable,
  fieldTouched,
  setFieldTouched,
  validations,
  updateErrors,
  autofillArray
}: {
  formData: any;
  handleChange?: any;
  handleBlur?: any;
  formValues?: any;
  touched?: any;
  errors?: any;
  updatedData?: any;
  setUpdatedData?: any;
  fieldDomRefs?: any;
  repeatableArr?: any;
  touchedArr?: any;
  formFields?: any;
  setFormFields?: any;
  formLabel?: any;
  isRepeatable?: any;
  fieldTouched?: any;
  setFieldTouched?: any;
  validations?: any;
  updateErrors?: any;
  autofillArray?: any;
}) => {
  const handleFormChange = (event: any, element?: any) => {
    let filterField: any = (condition: any) =>
      Object.values(updatedData).filter((el: any) => {
        return el.name === condition;
      });
    if (element?.dependencies) {
      element.dependencies.forEach((item: any) => {
        switch (item.type) {
          case "visibility":
            {
              const fieldToBeUpdated = filterField(item.field);
              if (!item?.values?.includes(event.target.value)) {
                fieldToBeUpdated[0].visible = false;
                setUpdatedData(updatedData);
              } else {
                fieldToBeUpdated[0].visible = true;
                setUpdatedData(updatedData);
              }
            }
            break;
        }
      });
    }
    handleChange(event, element);
  };

  const getInputValue = (element: any) => {
    let value;
    if (formValues && formValues[element.name] !== undefined) {
      value = formValues[element.name];
    }
    return value;
  };

  const handleFormBlur = (event: any, element?: any) => {
    let timer;
    clearTimeout(timer);
    if (element) {
      if (typeof element.callback === "function") {
        const { name, value } = event.target;
        const newEvent = {
          name: name,
          value: value ? value : event.currentTarget.getAttribute("data-value"),
        };
        const delay = element.delay || 0;
        timer = setTimeout(() => {
          element.callback(newEvent, formValues);
        }, delay);
      }
    }
    handleBlur(event, element);
  };

  const getTouchedValue = (element: any) => {
    let touchVal;
    if (touched && touched[element.name]) {
      touchVal = touched[element.name];
    }
    return touchVal;
  };

  const getErrors = (element: any) => {
    let error;
    if (errors && errors[element.name]) {
      error = errors[element.name];
    }
    return error;
  };

  const renderSwitch = (element: any) => {
    switch (element.elementType) {
      case "FormInput":
        return (
          <div key={element.id}>
            <Input
              key={element.id}
              type={element.type}
              label={element.label}
              placeholder={element.placeholder}
              onChange={(event) => handleFormChange(event, element)}
              className={element.className}
              onBlur={(event) => handleFormBlur(event)}
              disablePaste={element.disablePaste}
              value={getInputValue(element)}
              name={element.name}
              touched={getTouchedValue(element)}
              error={getErrors(element)}
              disabled={element.disabled}
              width={element.width}
              inputStyle={element.inputStyle}
              visible={element.visible}
              forwardedRef={fieldDomRefs[element.name]}
              icon={element.icon}
            />
          </div>
        );
      case "FormToggleSwitch":
        return (
          <div>
            <ToggleSwitch
              label={element.label}
              type={element.type}
              name={element.name}
              handleToggle={handleFormChange}
              error={getErrors(element)}
              className={element.className}
              value={getInputValue(element)}
              handleBlur={handleFormBlur}
              visible={element.visible}
            />
          </div>
        );
      case "FormDropdown":
        return (
          <div key={element.id}>
            <Dropdown
              name={element.name}
              options={element.options}
              height={element.height}
              onChange={(event) => handleFormChange(event, element)}
              className={element.className}
              value={getInputValue(element)}
              placeholder={element.placeholder}
              width={element.width}
              onBlur={(event) => handleFormBlur(event, element)}
              error={getErrors(element)}
              touched={getTouchedValue(element)}
              key={element.id}
              icon={element.icon}
              searchBar={element.searchBar}
              val={element.val}
            />
          </div>
        );
      case "FormAutosuggestDropdown":
        return (
          <div key={element.id}>
            <Autosuggest
              uniqueKey={element.id}
              placeholder={element.placeholder}
              onChange={(event) => handleFormChange(event, element)}
              className={element.className}
              onBlur={(event) => handleFormBlur(event, element)}
              value={getInputValue(element) || ""}
              name={element.name}
              suggestions={element.suggestionArray}
              error={getErrors(element)}
              touched={getTouchedValue(element)}
              // popupData={element.popupData}
              popupClasses={element.popupClasses}
              width={element.width}
              canAddEntry={element.canAddEntry}
              visible={element.visible}
              forwardedRef={fieldDomRefs[element.name]}
            />
          </div>
        );
      case "FormLink":
        return (
          <Link className={element.className} href={element.href}>
            {element.name}
          </Link>
        );
      case "FormCheckbox":
        return (
          <CheckboxList
            name={element.name}
            checked={getInputValue(element)}
            className={element.classname}
            options={element.options}
            onChange={(event: any) => handleFormChange(event, element)}
            onBlur={handleFormBlur}
            visible={element.visible}
          />
        );
      // case "FormCalendar":
      //   return (
      //     <div key={element.id}>
      //       <Datepicker
      //         onChange={handleFormChange}
      //         value={getInputValue(element)}
      //         name={element.name}
      //         onBlur={(event:any) => handleFormBlur(event, element)}
      //         error={getErrors(element)}
      //         touched={getTouchedValue(element)}
      //         placeholder={element.placeholder}
      //         disableNavigatorWidth={element.disableNavigatorWidth}
      //         disableSaturdaySelection={element.disableSaturdaySelection}
      //         disableSundaySelection={element.disableSundaySelection}
      //         disabledState={element.disabledTiles}
      //         dateFormat={element.dateFormat}
      //       />
      //     </div>
      //   );
      case "FormTextarea":
        return (
          <div>
            <Textarea
              key={element.id}
              placeholder={element.placeholder}
              onChange={handleFormChange}
              classname={element.class}
              onBlur={handleBlur}
              width={element.width}
              disablePaste={element.disablePaste}
              value={getInputValue(element)}
              name={element.name}
              touched={touched[element.name]}
              error={errors[element.name]}
            />
          </div>
        );
      case "FormRadio":
        return (
          <div key={element.id}>
            <RadioList
              options={element.options}
              name={element.name}
              onChange={handleFormChange}
              checked={getInputValue(element) || ""}
              error={errors[element.name]}
            />
          </div>
        );
      case "FormUploadInput":
        return (
          <div>
            <Upload
              key={element.id}
              type={element.type}
              onChange={handleFormChange}
              className={element.className}
              placeholder={element.placeholder}
              acceptedFileTypes={element.accept}
              onBlur={handleBlur}
              value={getInputValue(element)}
              name={element.name}
              multiple={element.multiple}
              touched={touched[element.name]}
              error={errors[element.name]}
              icon={element.icon}
              browseClick={element.click}
            />
          </div>
        );
      case "FormSearchBar":
        return (
          <div>
            <SearchBar
              name={element.name}
              type={element.type}
              placeholder={element.placeholder}
              className={element.className}
              filterIcon={element.filterIcon}
              onChange={element.onChange || handleFormChange}
              onBlur={handleFormBlur}
              visible={element.visible}
            />
          </div>
        );
      case "FormButton":
        return (
          <Button
            type={element.type}
            size={element.size}
            onClick={element.onClick}
            variant={element.variant}
            className={element.className}
            precedingText={element.precedingText}
            loading={element.loading}
          >
            {element.children}
          </Button>
        );
      case "FormSelectMultipleItem":
        return (
          <div key={element.id}>
            <DropdownComponent
              name={element.name}
              options={element.options}
              height={element.height}
              onChange={(event) => handleFormChange(event, element)}
              className={element.className}
              value={getInputValue(element)}
              placeholder={element.placeholder}
              width={element.width}
              onBlur={(event) => handleFormBlur(event, element)}
              error={getErrors(element)}
              touched={getTouchedValue(element)}
              key={element.id}
              icon={element.icon}
              searchBar={element.searchBar}
              val={element.val}
              inputStyle={element.inputStyle}
            />
          </div>
        );

      default:
        return null;
    }
  };

  return <div>{renderSwitch(formData)}</div>;
};

export default Form;
