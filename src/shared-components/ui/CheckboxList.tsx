import Checkbox from "./Checkbox";

interface CheckboxListProps {
  options: {
    id?: string;
    name: string;
    label?: string;
    value?: string;
    defaultChecked?: boolean;
  }[];
  className?: string;
  name: string;
  checked?: any;
  visible?: boolean;
  onChange?: (...args: unknown[]) => unknown;
  onBlur?: (...args: unknown[]) => unknown;
}

const CheckboxList = ({
  options,
  name,
  className,
  onChange,
  onBlur,
  checked,
  visible = true,
  ...props
}: CheckboxListProps): JSX.Element => {
  return (
    <>
      {options.map((checkbox) => {
        <Checkbox
          name={name}
          label={checkbox.label}
          className={className}
          onChange={onChange}
          onBlur={onBlur}
          value={checkbox.label}
          visible={visible}
          checked={checked?.includes(checkbox.label) ? true : false}
          {...props}
        />;
      })}
    </>
  );
};

export default CheckboxList;
