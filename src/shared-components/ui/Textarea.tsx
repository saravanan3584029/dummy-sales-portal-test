import ValidationError from "../components/ValidationError";

type TextAreaProps = {
  name: string;
  value?: string;
  label?: any;
  placeholder?: string;
  forwardedRef?: string;
  error?: string;
  touched?: boolean;
  classname?: string;
  width?: string;
  height?: string;
  disablePaste?: boolean;
  style?: { "textarea-width"?: string };
  onBlur?: (...args: unknown[]) => unknown;
  onChange?: (...args: unknown[]) => unknown;
  maxLength?: number;
  rows?: number;
};

const TextArea = ({
  name,
  value = "",
  label,
  placeholder = "type here",
  forwardedRef,
  error,
  touched = false,
  style = { "textarea-width": "100%" },
  onChange,
  onBlur,
  maxLength,
  classname,
  disablePaste,
  rows,
  width,
  height,
  ...props
}: TextAreaProps): JSX.Element => {
  return (
    <>
      <div>
        <textarea
          className={`${
            error && touched ? "border-red-500" : "border-gray-100"
          }  p-2 bg-transparent outline-none textarea ${classname}`}
          id={name}
          name={name}
          placeholder={placeholder}
          onChange={onChange}
          onBlur={onBlur}
          value={value}
          ref={forwardedRef}
          maxLength={maxLength}
          rows={rows}
          {...props}
        />
        <label htmlFor={name}>{label}</label>
      </div>
      <p className="text-error text-right text-sm" id="error">
        {touched && error && <ValidationError error={error} />}
      </p>
      <style>{` 
      .textarea{
        width:${width || "100%"};
        border-radius:8px;
       resize:none;
      
      }
      .textarea::-webkit-scrollbar {
        display: none;
      }
       
      `}</style>
    </>
  );
};

export default TextArea;
