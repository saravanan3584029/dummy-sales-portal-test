import React from "react";
import Icon from "@/lib/svg";

type CheckboxProps = {
  name: string;
  className?: string;
  label?: string;
  value?: string;
  forwardedRef?: any;
  onBlur?: (...args: unknown[]) => unknown;
  onChange?: (...args: unknown[]) => unknown;
  checked?: boolean;
  visible?: boolean;
  line?: boolean;
};

const Checkbox = ({
  name,
  checked,
  className,
  label,
  value,
  forwardedRef,
  onChange,
  onBlur,
  visible,
  line,
}: CheckboxProps): JSX.Element => {
  return (
    <>
      <div
        className={`py-2 checkbox-container ${className} ${
          !visible && "hidden"
        }`}
      >
        <input
          type="checkbox"
          name={name}
          checked={checked}
          value={value}
          ref={forwardedRef}
          onClick={(e) => {
            onChange && onChange(e);
          }}
          onChange={() => {}}
          onBlur={onBlur}
        />
        <span className={`px-1 ml-3 ${checked ? "text-success" : "label"}`}>
          {label}
        </span>
      </div>
      <style>{`
      input[type="checkbox"] {
        width: 15px;
        height: 15px;
        border: none;
        position: relative;
      }
      input[type="checkbox"]:checked{
        accent-color: #1A68F3;
        opacity:0.7;
        width: 15px;
        height:15px;
      }
      .label {
        color: #23232380;
      }`}</style>
    </>
  );
};

export default Checkbox;
