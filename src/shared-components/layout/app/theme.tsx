"use client";

import { ThemeConfig } from "@/config/types";

const Theme = ({ themeConfig }: { themeConfig?: ThemeConfig }): JSX.Element => {
  let { colors, fontSize, lineHeight, fontFamily } = themeConfig?.style;

  return (
    <style jsx global>
      {`
        body,
        html {
          --color-text-primary: ${colors["text-primary"]};

          --size-font-2xs: ${fontSize["2xs"]};
          --size-font-xs: ${fontSize.xs};
          --size-font-sm: ${fontSize.sm};
          --size-font-m: ${fontSize.m};
          --size-font-base: ${fontSize.base};
          --size-font-lg: ${fontSize.lg};
          --size-font-xl: ${fontSize.xl};
          --size-font-2xl: ${fontSize["2xl"]};

          --line-height-2xs: ${lineHeight["2xs"]};
          --line-height-xs: ${lineHeight.xs};
          --line-height-sm: ${lineHeight.sm};
          --line-height-m: ${lineHeight.m};
          --line-height-base: ${lineHeight.base};
          --line-height-lg: ${lineHeight.lg};
          --line-height-xl: ${lineHeight.xl};
          --line-height-2xl: ${lineHeight["2xl"]};

          --font-family-Greycliff: ${fontFamily["family-Greycliff"]};
          --font-family-Inter: ${fontFamily["family-Inter"]};
        }
      `}
    </style>
  );
};

export default Theme;
