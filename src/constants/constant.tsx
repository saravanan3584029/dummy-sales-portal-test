const STATUS = {
  AVAILABLE: "Available",
  ALIGNED: "Aligned",
  DEPLOYED: "Deployed",
  UNAVAILABLE: "Unavailable",
  TERMINATED: "Terminated",
};

const WORK_MODE = {
  ONSITE: "Onsite",
  REMOTE: "Remote",
  HYBRID: "Hybrid",
};

const PAYMENT_TERMS = {
  ADVANCE_PAYMENT: "Advance Payment",
  " 30_DAYS": "30 Days",
  "15 DAYS": "15 Days",
};

export { STATUS, WORK_MODE, PAYMENT_TERMS };
