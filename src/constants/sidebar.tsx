// Define a custom hook to generate the Sidebar with dynamic routes
function useSidebar(id: any) {
  const Sidebar = [
    {
      label: "Dashboard",
      icon: "dashboard",
      width: "16",
      height: "16",
      url: "/dashboard",
    },
    {
      label: "Vendor",
      icon: "vendors",
      url: "/vendors",
      submenu: [
        { label: "All Vendors", url: "/vendors" },
        { label: "My Vendors", url: `/vendors/${id}` }, // Use dynamic route
        { label: "Create New", url: "/vendors/create" },
      ],
    },
    {
      label: "Client",
      icon: "client",
      url: "/clients",
      submenu: [
        { label: "All Clients", url: "/clients" },
        { label: "My Clients", url: `/clients/${id}` }, // Use dynamic route
        { label: "Create New", url: "/clients/create" },
      ],
    },
    {
      label: "Resource",
      icon: "resource",
      width: "16",
      height: "16",
      fill: "white",
      url: "/resources",
      submenu: [
        { label: "All Resources", url: "/resources" },
        { label: "My Resource", url: `/resources/${id}` },
        { label: "Add Resource", url: "/resources/add" },
      ],
    },
    {
      label: "Requirements",
      icon: "requirements",
      width: "16",
      height: "16",
      url: "/requirements",
      submenu: [
        { label: "All Requirements", url: "/requirements" },
        { label: "My Requirements", url: `/requirements/${id}` },
        { label: "Create Requirements", url: "/requirements/create" },
      ],
    },
  ];

  return Sidebar;
}

export default useSidebar;
