export const COLOR = {
  RED: "#B4332A",
  LIGHT_GRAYISH_BLUE: "#EFEFF1",
  WHITE: "#FFFFFF",
  YELLOW: "#FDB022",
  BLUE: "#1A68F3",
  BLUE_1: " #3B82F6",
  GREEN: "#0E9255",
  GRAY: "#AAAAAF",
  GRAY_1: "#E3E3E4",
  GRAY_4: "#84848c",
  GRAY_7: "#39393D",
};

export const BUTTON = {
  PRIMARY: {
    default: {
      type: "solid",
      text: COLOR.WHITE,
      bgColor: COLOR.BLUE,
      borderRadius: "5px",
    },
  },
  SECONDARY: {
    default: {
      type: "solid",
      text: "black",
      bgColor: "white",
      borderRadius: "5px",
    },
  },
};
