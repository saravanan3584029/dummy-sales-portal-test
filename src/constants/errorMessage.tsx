export const ERROR_MESSAGE = {
  TEXT_INVALID: "This field must only contain letters and spaces",
  STATUS_INVALID: "Invalid status value. Please provide a valid status.",
  WORK_MODE_INVALID: "Please specify a valid work mode",
  FILEID_REQUIRED: "File id is required to query a file",
  DATA_NOT_FOUND: "Please specify a valid input",
  MISSING_PARAMETER: "Required parameter is missing",
  MULTIPLE_QUERY_ERROR:
    "some documents specified in the query were not found in the database",
  PAYMENT_TERMS_INVALID:
    "Invalid payment terms. Please provide a valid payment terms.",
  VENDOR_CANNOT_BE_UPDATE: "Vendor name cannot be updated,",
};
