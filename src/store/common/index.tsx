import { createSlice } from "@reduxjs/toolkit";

const commonSlice = createSlice({
  name: "common",
  initialState: {
    loader: false,
    toaster: {
      visible: false,
      messageData: {
        description: "",
        variant: "",
        timer: 0,
      },
    },
    popup: {
      visible: false,
      messageData: {
        title: "",
        head: {},
        description: "",
        btnArray: [],
      },
      classAdditions: {},
    },
  },
  reducers: {
    showLoader: (state) => {
      state.loader = true;
    },
    hideLoader: (state) => {
      state.loader = false;
    },
    showToaster: (state, action) => {
      state.toaster.visible = true;
      state.toaster.messageData.description = action.payload.description;
      state.toaster.messageData.variant = action.payload.variant;
      state.toaster.messageData.timer = action.payload.timer;
    },
    hideToaster: (state) => {
      state.toaster.visible = false;
    },
    showPopup: (state, action) => {
      state.popup.visible = true;
      state.popup.messageData.title = action.payload.title;
      state.popup.messageData.head = action.payload.head;
      state.popup.messageData.description = action.payload.description;
      state.popup.messageData.btnArray = action.payload.btnArray;
      state.popup.classAdditions = action.payload.className;
    },
    hidePopup: (state) => {
      state.popup.visible = false;
    },
  },
});
export const {
  showLoader,
  hideLoader,
  showToaster,
  hideToaster,
  showPopup,
  hidePopup,
} = commonSlice.actions;
export default commonSlice.reducer;
