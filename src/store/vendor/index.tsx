import { createSlice } from "@reduxjs/toolkit";
const VendorSlice = createSlice({
  name: "vendor",
  initialState: {
    vendor: [],
    resource:[]
  },
  reducers: {
    setVendorData: (state, action) => {
      state.vendor = action.payload;
    },
    setResourceTabData: (state, action) => {
      state.resource = action.payload;
    },
  },
});
export const { setVendorData,setResourceTabData } = VendorSlice.actions;
export default VendorSlice.reducer;
