import { createSlice } from "@reduxjs/toolkit";
const UserSlice = createSlice({
  name: "user",
  initialState: {
    Auth: false,
    userName: "",
    userId: "",
    user: "",
    client: "",
    contactPerson: "",
  },
  reducers: {
    SetUserName: (state, action) => {
      state.userName = action.payload;
    },

    SetAuth: (state, action) => {
      state.Auth = action.payload;
    },
    SetClearAuth: (state, action) => {
      state.Auth = false;
      state.userName = "";
    },
    SetUserId: (state, action) => {
      state.userId = action.payload;
    },
    setUserData: (state, action) => {
      state.user = action.payload;
    },
    setClient: (state, action) => {
      state.client = action.payload;
    },
    setContactPerson: (state, action) => {
      state.contactPerson = action.payload;
    },
  },
});
export const {
  SetUserName,
  SetAuth,
  SetClearAuth,
  SetUserId,
  setUserData,
  setClient,
  setContactPerson,
} = UserSlice.actions;
export default UserSlice.reducer;
