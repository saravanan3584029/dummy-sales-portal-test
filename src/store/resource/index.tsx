import { createSlice } from "@reduxjs/toolkit";
import { object } from "joi";
const resourceSlice = createSlice({
  name: "resource",
  initialState: {
    resource: [],
  },
  reducers: {
    setResourceData: (state, action) => {
      state.resource = action.payload.response;
    },
  },
});
export const { setResourceData } = resourceSlice.actions;
export default resourceSlice.reducer;
