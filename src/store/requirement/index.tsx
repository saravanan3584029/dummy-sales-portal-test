import { createSlice } from "@reduxjs/toolkit";
const requirementSlice = createSlice({
  name: "resource",
  initialState: {
    requirement: [],
  },
  reducers: {
    getRequirementData: (state, action) => {
      state.requirement = action.payload.response;
    },
  },
});
export const { getRequirementData } = requirementSlice.actions;
export default requirementSlice.reducer;
