import { createSlice } from "@reduxjs/toolkit";
const ResultSlice = createSlice({
    name: "result",
    initialState: {
        DomainSearch: [],
        EmailSearch: [],
        EmailVerify: [],
    },
    reducers: {
        SetDomainSearch: (state, action) => {
            state.DomainSearch = action.payload;
        },
        SetEmailSearch: (state, action) => {
            state.EmailSearch = action.payload;
        },
        SetEmailVerify: (state, action) => {
            state.EmailVerify = action.payload;
        },
        SetClear: (state) => {
            state.DomainSearch = [];
            state.EmailSearch = [];
            state.EmailVerify = [];
        }
    }
});
export const { SetDomainSearch,SetEmailSearch,SetEmailVerify,SetClear } = ResultSlice.actions;
export default ResultSlice.reducer;