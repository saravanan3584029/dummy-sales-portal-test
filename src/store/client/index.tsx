import { createSlice } from "@reduxjs/toolkit";
const ClientSlice = createSlice({
  name: "client",
  initialState: {
    client: [],
  },
  reducers: {
    getClientsData: (state, action) => {
      state.client = action.payload;
    },
  },
});
export const { getClientsData } = ClientSlice.actions;
export default ClientSlice.reducer;
