import { createSlice } from "@reduxjs/toolkit";
const SideBar = createSlice({
    name: "sidebar",
    initialState: {
      selectedMenuItem: "",
    selectedSubmenuItem:""
        
    },
    reducers: {
        SetSelectedMenuItem: (state, action) => {
            state.selectedMenuItem = action.payload;
        },
        SetSelectedSubmenuItem: (state, action) => {
            state.selectedSubmenuItem = action.payload;
        },
        
    }
});
export const { SetSelectedSubmenuItem,SetSelectedMenuItem } = SideBar.actions;
export default SideBar.reducer;