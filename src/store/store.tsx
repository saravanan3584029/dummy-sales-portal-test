import { configureStore } from "@reduxjs/toolkit";

import ResultSlice from "./resultslice";
import UserSlice from "./user";
import SideBar from "./sidebarslice";
import resourceSlice from "./resource/index";
import commonSlice from "./common/index";
import requirementSlice from "./requirement/index";
import ClientSlice from "./client/index";
import VendorSlice from "./vendor/index";

const store = configureStore({
  reducer: {
    result: ResultSlice,
    user: UserSlice,
    sidebar: SideBar,
    resource: resourceSlice,
    common: commonSlice,
    requirement: requirementSlice,
    vendor: VendorSlice,
    client: ClientSlice,
  },
});
export default store;
