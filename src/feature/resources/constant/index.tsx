import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import { states } from "@/utils/locationUtils";

export const resourceStatusArray = [
  {
    id: 1,
    label: "Available",
    value: "Available",
    className: `${COLOR.GREEN}`,
    icon: {
      type: "selected-dot",
      fill: "#39393D",
    },
  },
  {
    id: 2,
    label: "Aligned",
    value: "Aligned",
    className: `${COLOR.GRAY}`,
    icon: {
      type: "selected-dot",
      fill: "#AAAAAF",
    },
  },
  {
    id: 3,
    label: "Deployed",
    value: "Deployed",
    className: `${COLOR.GRAY}`,
    icon: {
      type: "selected-dot",
      fill: "#AAAAAF",
    },
  },
  {
    id: 4,
    label: "Unavailable",
    value: "Unavailable",
    className: `${COLOR.RED}`,
    icon: {
      type: "selected-dot",
      fill: "#39393DF",
    },
  },
  {
    id: 5,
    label: "Terminated",
    value: "Terminated",
    className: `${COLOR.GRAY_7}`,
    icon: {
      type: "selected-dot",
      fill: "#39393DF",
    },
  },
];

export const workMode = [
  {
    id: 1,
    label: "Onsite",
    value: "Onsite",
  },
  {
    id: 1,
    label: "Remote",
    value: "Remote",
  },
  {
    id: 1,
    label: "Hybrid",
    value: "Hybrid",
  },
];

// Function to filter unique states and districts
const filterUniqueStatesAndDistricts = (inputStates: any) => {
  const uniqueStates = [
    ...new Set(inputStates.map((entry: any) => entry.State)),
  ];
  const uniqueDistricts = [
    ...new Set(inputStates.map((entry: any) => entry.District)),
  ];
  const uniqueCitys = [...new Set(inputStates.map((entry: any) => entry.City))];

  const forStateData = uniqueStates.map((state, index) => ({
    id: index + 1,
    label: state,
    value: state,
  }));
  const forDistrictData = uniqueDistricts.map((district, index) => ({
    id: forStateData.length + index,
    label: district,
    value: district,
  }));
  const forCityData = uniqueCitys.map((city, index) => ({
    id: forDistrictData.length + index,
    label: city,
    value: city,
  }));

  const resultArray = [
    ...forStateData,
    ...forDistrictData,
    ...forCityData,
    // { type: "State", values: uniqueStates },
    // { type: "District", values: uniqueDistricts },
  ];
  return resultArray;
};

// Usage
export const uniqueStatesAndDistricts = filterUniqueStatesAndDistricts(states);

const technologiesArray = [
  "Java",
  "Python",
  "JavaScript",
  "C#",
  "Ruby",
  "Go",
  "Swift",
  "Kotlin",
  "TypeScript",
  "HTML, CSS, JavaScript",
  "React.js",
  "Angular",
  "Vue.js",
  "Node.js",
  "Express.js",
  "Django",
  "Flask",
  "Android (Java, Kotlin)",
  "iOS (Swift, Objective-C)",
  "React Native",
  "Flutter",
  "Xamarin",
  "MySQL",
  "PostgreSQL",
  "MongoDB",
  "SQLite",
  "Oracle Database",
  "Microsoft SQL Server",
  "Amazon Web Services (AWS)",
  "Microsoft Azure",
  "Google Cloud Platform (GCP)",
  "IBM Cloud",
  "Docker",
  "Kubernetes",
  "Jenkins",
  "Ansible",
  "Terraform",
  "Django (Python)",
  "Flask (Python)",
  "Ruby on Rails (Ruby)",
  "Express.js (JavaScript/Node.js)",
  "Spring Boot (Java)",
  "React.js",
  "Angular",
  "Vue.js",
  "Svelte",
  "Bootstrap",
  "TensorFlow",
  "PyTorch",
  "scikit-learn",
  "OpenCV",
  "Natural Language Processing (NLP)",
  "Encryption Technologies",
  "Firewalls",
  "Intrusion Detection Systems (IDS)",
  "Virtual Private Networks (VPNs)",
  "Raspberry Pi",
  "Arduino",
  "MQTT (Message Queuing Telemetry Transport)",
  "IoT Platforms",
  "Ethereum",
  "Hyperledger Fabric",
  "Solidity (programming language for smart contracts)",
  "Hadoop",
  "Spark",
  "Apache Kafka",
  "Apache Flink",
  "VMware",
  "VirtualBox",
  "KVM (Kernel-based Virtual Machine)",
  "Git",
  "GitHub",
  "GitLab",
  "Bitbucket",
  "Slack",
  "Microsoft Teams",
  "Zoom",
  "Google Meet",
  "Apache",
  "Nginx",
  "Microsoft IIS",
  "Docker",
  "Podman",
  "Containerd",
];

export const technologies = technologiesArray.map((technology, index) => ({
  id: index + 1,
  label: technology,
  value: technology,
}));

