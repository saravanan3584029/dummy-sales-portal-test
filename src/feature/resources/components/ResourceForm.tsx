/* eslint-disable react/no-children-prop */

"use client";
import React, { Fragment, useCallback, useEffect, useState } from "react";
import FormRender from "@/utils/form/formRender";
import {
  resourceBtnConfig,
  resourceFormConfig,
  formLinkConfig,
} from "../config/FormConfig";
import Icon from "@/lib/svg";
import { resourceValidations } from "../config/validationConfig";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/navigation";
import Notes from "@/shared-components/components/Note";
import { hideLoader, showLoader, showToaster } from "@/store/common";
import { ResourceData } from "../config/types";
import { setUserData } from "@/store/user";
import { setVendorData } from "@/store/vendor";
import { getItemFromLocalStorage } from "@/utils";

const ResourceForm = ({
  className,
  columnNum = 3,
  id,
  creatingNewEntry = false,
  filterDepartment = false,
}: {
  className?: string;
  columnNum?: number;
  id?: string;
  creatingNewEntry?: boolean;
  filterDepartment?: boolean;
}) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [updateData, setUpdateData] = useState<any>();
  const resourceStore = useSelector((store: any) => store.resource.resource);
  const vendorStore = useSelector((store: any) => store.vendor.vendor);
  const accountManager = useSelector((store: any) => store.user.user);

  const [currentUserId, setCurrentUserId] = useState<any>();
  const [currentUserName, setCurrentUserName] = useState<any>();
  
  useEffect(() => {
    setCurrentUserId(getItemFromLocalStorage("user_id"));
    setCurrentUserName(getItemFromLocalStorage("user_name"));
    const formDropDownData = async () => {
      const response = await fetch(`/api/user/`);
      if (response.ok) {
        const data = await response.json();
        dispatch<any>(setUserData(data.response));
      }
      const vendorResponse = await fetch(`/api/vendors`);
      if (response.ok) {
        const responseData = await vendorResponse.json();
        dispatch<any>(setVendorData(responseData.response));
      }
    };
    formDropDownData();
  }, []);

  const formData = resourceFormConfig(vendorStore, accountManager);

  const fetchSingleUser = useCallback(async () => {
    try {
      const response = await fetch(`/api/resource/` + id);
      if (response.ok) {
        const data = await response.json();
        setUpdateData(data.response);
      }
    } catch (error) {
      console.error("Fetch Error:", error);
    }
  }, [id]);

  useEffect(() => {
    const fetchData = async () => {
      if (id && resourceStore.length === 0) {
        await fetchSingleUser();
      } else {
        const data = resourceStore.find((item: any) => item._id === id);
        setUpdateData(data);
      }
    };
    fetchData();
  }, [id, resourceStore, fetchSingleUser]);

  const addResourceHandler = async (data: any) => {
    dispatch<any>(showLoader());
    const formData = new FormData();
    let { status, resume, ...remining } = data;
    const statusValue = status && status.props.children[1];
    const resourceData = {
      ...remining,
      status: statusValue,
    };
    formData.append("file", resume);
    formData.append("jsonData", JSON.stringify(resourceData));
    try {
      const response = await fetch("/api/resource", {
        method: "POST",
        body: formData,
      });
      dispatch<any>(hideLoader());
      if (response.ok) {
        const data = await response.json();
        if (data.success) {
          dispatch<any>(
            showToaster({
              description: data.message,
              timer: 1000,
            })
          );
          router.push("/resources");
        } else {
          dispatch<any>(
            showToaster({
              description:
                data.message ||
                (Object.values(data.error.errors) as Error[])[0]?.message,
              timer: 3000,
              variant: "red",
            })
          );
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const updateResourceHandler = async (data: any) => {
    const { status, ...remaining } = data;
    const val = typeof status === "object" ? status.props.children[1] : status;
    const request = { ...remaining, status: val };
    let updateObject: any = {};
    Object.keys(request).map((ele) => {
      if (ele === "resume") {
        if (request[ele] !== updateData[ele]?.filename) {
          updateObject = { ...updateObject, [ele]: request[ele] };
        }
      }else if(Array.isArray(request[ele])){
        if (request[ele].length === updateData[ele].length) {
          const hasDifference =request[ele].some(
            (item: any, index: number) => {
              if (typeof item === "string") {
                return item !== updateData[ele][index];
              } else {
                return (
                  item.techStack !== updateData[ele][index].techStack ||
                  item.isPrimary !== updateData[ele][index].isPrimary
                );
              }
            }
          );

          if (hasDifference) {
            updateObject = { ...updateObject, [ele]: request[ele] };
          }
        } else {
          updateObject = { ...updateObject, [ele]: request[ele] };
        }
      }else if(typeof updateData[ele]==='object'){
        if(request[ele]!==updateData[ele][ele])
        updateObject = { ...updateObject, [ele]: request[ele] };

      } else if (request[ele] !== updateData[ele]) {
        updateObject = { ...updateObject, [ele]: request[ele] };
      }
    });
    
    try {
      if (Object.keys(updateObject).length > 0) {
        dispatch(showLoader());
        const form = new FormData();
        const { resume, ...entry } = updateObject;
        const dataToUpdate = { ...entry, _id: id };
        resume && form.append("file", resume);
        form.append("jsonData", JSON.stringify(dataToUpdate));
        const response = await fetch("/api/resource", {
          method: "PUT",
          body: form,
        });
        dispatch(hideLoader());
        if (response.ok) {
          const data = await response.json();
          if (data.success) {
            dispatch<any>(
              showToaster({
                description: data.message,
                timer: 1000,
              })
            );
            router.push("/resources");
          } else {
            dispatch<any>(
              showToaster({
                description:
                  data.message ||
                  (Object.values(data.error.errors) as Error[])[0]?.message,
                timer: 3000,
                variant: "red",
              })
            );
          }
        }
      } else {
        dispatch<any>(
          showToaster({
            description: "No entry has been updated in the form.",
            timer: 2000,
            variant: "green",
          })
        );
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleNotesComment = async (data: any, replyId: any) => {
    if (id) {
      if (data) {
        const postComment = { ...data, postedBy: currentUserName };
        try {
          dispatch(showLoader());
          const response = await fetch("/api/resource/note", {
            method: "POST",
            body: JSON.stringify({
              postComment,
              _id: replyId ? replyId.id : id,
            }),
          });
          if (response.ok) {
            const data = await response.json();
            await fetchSingleUser();
            dispatch(hideLoader());
            if (data.success) {
              dispatch(
                showToaster({
                  timer: 2000,
                  description: data.message,
                })
              );
            }
          }
          dispatch(hideLoader());
        } catch (error) {
          console.log(error);
        }
      } else {
        dispatch(
          showToaster({
            timer: 2000,
            description: "Comment cannot be empty add atleast any comment",
          })
        );
      }
    } else {
      dispatch(
        showToaster({
          timer: 2000,
          description:
            "Prior to vendor creation, commenting functionality shall be restricted.",
        })
      );
    }
  };

  return (
    <Fragment>
      <div className="flex flex-row mr-12 gap-2 items-center mt-4 text-gray-500">
        <Icon type="back" />
        <div className="flex flex-row justify-between items-center text-m">
          <span>Dashboard</span>
          <Icon type="chev_right" />
          <span>Resource</span>
          <Icon type="chev_right" />
          <span>All Resource</span>
        </div>
      </div>

      <div className="mt-3">
        <FormRender
          formHeading={id ? "Update Resource" : "Add Resource"}
          formColumn={columnNum}
          formLink={formLinkConfig(currentUserId)}
          formLinkStyle={`text-blue-500 font-bold`}
          formLabelStyle="text-sm text-disable font-bold mb-2 "
          formLine={true}
          formData={formData}
          initialStaticData={
            id
              ? {
                  name: updateData?.name,
                  vendorName: updateData?.vendorName?.vendorName,
                  status: updateData?.status,
                  phone: updateData?.phone,
                  email: updateData?.email,
                  costing: updateData?.costing,
                  experience: updateData?.experience,
                  location: updateData?.location,
                  workMode: updateData?.workMode,
                  resume: updateData?.resume?.filename,
                  technology: updateData?.technology || [],
                  accountManager: updateData?.accountManager || [],
                }
              : {
                  name: "",
                  vendorName: "",
                  status: "",
                  phone: "",
                  email: "",
                  costing: "",
                  experience: "",
                  location: "",
                  workMode: "",
                  resume: "",
                  technology: [],
                  accountManager: [],
                }
          }
          validations={resourceValidations}
          submitForm={id ? updateResourceHandler : addResourceHandler}
          btnConfig={resourceBtnConfig(id ? "Update" : "Create")}
          btnStyle={false}
        />
      </div>

      <Notes handleComment={handleNotesComment} notesData={updateData} />
    </Fragment>
  );
};

export default ResourceForm;
