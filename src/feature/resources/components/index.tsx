"use client";
import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import Card from "@/shared-components/components/Card";
import { setResourceData } from "@/store/resource";
import Link from "next/link";
import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { resourceCardConfig } from "../config/cardConfig";
import SearchBar from "@/shared-components/components/SearchBar";
import DetailView from "../../../shared-components/components/DetailView";
import Filter from "@/shared-components/components/Filter";
import filterConfig, { filterBtnConfig } from "../config/filterConfig";
import { FilterTypes, ResourceData } from "../config/types";
import { hideLoader, showLoader, showToaster } from "@/store/common";
import {
  applyFiltersToData,
  base64ToBlob,
  downloadFiles,
  getItemFromLocalStorage,
} from "@/utils";
import {
  detailConfig,
  detailHeaderConfig,
  tabsConfig,
} from "../config/detailViewConfig";
import { log } from "console";
import { setUserData } from "@/store/user";
import { setVendorData } from "@/store/vendor";

export default function ResourceDashboard({ id }: { id?: string }) {
  const dispatch = useDispatch();
  const [popup, setPopup] = useState<any>({});
  // select button
  const [showCheckBox, setShowCheckBox] = useState<boolean>(false);
  const [checkedData, setCheckedData] = useState<ResourceData[]>([]);
  // deatail view
  const [showDetailView, setShowDetailView] = useState<any>();
  const [selectedItem, setSelectedItem] = useState<any>();
  //filter
  const [isFilterOpen, setIsFilterOpen] = useState<Boolean>(false);
  const [filters, setFilters] = useState<any>([]);

  const [currentUserId, setCurrentUserId] = useState<any>();
  const [currentUserName, setCurrentUserName] = useState<any>();
  const [currentUserRole, setCurrentUserRole] = useState<any>();

  const vendorStore = useSelector((store: any) => store.vendor.vendor);
  const accountManager = useSelector((store: any) => store.user.user);

  useEffect(() => {
    setCurrentUserId(getItemFromLocalStorage("user_id"));
    setCurrentUserName(getItemFromLocalStorage("user_name"));
    setCurrentUserRole(getItemFromLocalStorage("user_role"));
    const formDropDownData = async () => {
      if (vendorStore.length === 0) {
        const response = await fetch(`/api/user/`);
        if (response.ok) {
          const data = await response.json();
          dispatch<any>(setUserData(data.response));
        }
      }
      if (accountManager.length === 0) {
        const response = await fetch(`/api/vendors`);
        if (response.ok) {
          const responseData = await response.json();
          dispatch<any>(setVendorData(responseData.response));
        }
      }
    };
    formDropDownData();
  }, []);

  const filterConfigData = filterConfig(vendorStore, accountManager);

  const func = async (id?: string) => {
    dispatch<any>(showLoader());
    const response = await fetch(
      `/api/resource${id ? `/myresource/${id}` : ""}`,
      {
        method: "GET",
      }
    );
    if (response.ok) {
      const responseData = await response.json();
      dispatch(setResourceData(responseData));
      dispatch<any>(hideLoader());
    }
  };
  useEffect(() => {
    func(id);
  }, []);

  const resourceStore = useSelector((store: any) => store.resource.resource);

  const [filteredData, setFilteredData] = useState<any>([]);

  const showPopup = (item: any) => {
    setPopup(item);
  };

  // delete a resource from the database
  const handleDeleteResource = async (data: any) => {
    let id;
    if (typeof data === "string") {
      id = data;
    } else {
      id = data.map((obj: { _id: string }) => obj._id);
    }

    dispatch<any>(showLoader());
    const response = await fetch("/api/resource", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id: id }),
    });
    await func();
    dispatch<any>(hideLoader());
  };

  // download a resume from database
  const handleDownloadResume = async (id: any) => {
    try {
      dispatch<any>(showLoader());
      const response = await fetch("/api/file", {
        method: "POST",
        body: JSON.stringify({ id: id }),
      });

      if (response.ok) {
        const fileName = response.headers.get("Content-Disposition");
        const blob = await response.blob();
        downloadFiles(blob, fileName);
      }
      dispatch<any>(hideLoader());
    } catch (error) {
      console.log("Error downloading file:", error);
    }
  };

  const handleMultipleDownloadResume = async () => {
    try {
      const resumeId = checkedData.map((obj: any) => obj.resume._id);

      dispatch<any>(showLoader());
      const response = await fetch("/api/file", {
        method: "POST",
        body: JSON.stringify({ id: resumeId }),
      });

      if (response.ok) {
        const { files } = await response.json();
        files.forEach((fileData: any) => {
          const { content, contentType, filename } = fileData;
          const blob = base64ToBlob(content, contentType);
          downloadFiles(blob, filename);
        });
      }
      dispatch<any>(hideLoader());
    } catch (error) {
      console.log("Error downloading file:", error);
    }
  };

  //card button popup
  const showPopupDetails = (item: any) => {
    return Object.keys(popup).length && popup?._id === item?._id ? (
      <>
        <div
          role="presentation"
          className={`popup-wrapper justify-center items-center flex outline-none focus:outline-none`}
          onClick={() => setPopup({})}
        />
        <ul className="popup bg-white p-2">
          <li
            className="details-item"
            onClick={() => {
              setShowDetailView(true);
              setSelectedItem(item);
            }}
          >
            {" "}
            View More
          </li>
          {/* <li className="details-item">Open Resource</li> */}
          <li
            className="details-item"
            onClick={() => handleDownloadResume(item.resume._id)}
          >
            Download Resume
          </li>
          {/* <li className="details-item">Align to Requirement</li> */}

          {(currentUserRole === "admin" ||
            item.accountManager.includes(currentUserName)) && (
            <>
              <li className="details-item">
                <Link href={`/resources/update/${item._id}`}>Edit Deatils</Link>
              </li>
              <li
                className="details-item text-red-600"
                onClick={() => handleDeleteResource(item._id)}
              >
                Delete Resource
              </li>
            </>
          )}
        </ul>
      </>
    ) : null;
  };

  // search filter handler
  const handleSearchFilter = (event: any) => {
    const value = event.target.value;
    const filterData = resourceStore?.filter(
      (user: any) => user.name?.toLowerCase()?.indexOf(value.toLowerCase()) > -1
    );
    setFilteredData(filterData);
  };

  //handle a selected filter
  const handleSelectedFilter = (key: string, value: any) => {
    if (typeof value === "object") {
      setFilters([...filters, { key: key, value: { ...value } }]);
    } else {
      const existingFilterIndex = filters.findIndex(
        (filter: { value: string }) => filter.value === value
      );
      setFilteredData([]);
      setFilters((prevFilters: any) => {
        if (existingFilterIndex !== -1) {
          const updatedFilters = [...prevFilters];
          updatedFilters.splice(existingFilterIndex, 1);
          return updatedFilters;
        } else {
          return [...prevFilters, { key: key, value: value }];
        }
      });
    }
  };

  // handle a apply filter button
  const handleApplyFilter = () => {
    const filterdData = applyFiltersToData(resourceStore, filters);
    const uniqueFilteredData = filterdData?.filter((data: any) => {
      return !filteredData?.some((prevData: any) => areEqual(data, prevData));
    });
    setFilteredData([...(filteredData || []), ...(uniqueFilteredData || [])]);
    setIsFilterOpen(false);
  };

  const areEqual = (data1: any, data2: any) => {
    return JSON.stringify(data1) === JSON.stringify(data2);
  };

  // handle a remove filter cross icon
  const handleRemoveFilterIcon = async (item: FilterTypes) => {
    const removedItem = filters.filter(
      (entry: any) => entry.key !== item.key || entry.value !== item.value
    );
    setFilters(removedItem);
    const filterdData = applyFiltersToData(resourceStore, removedItem);
    setFilteredData(filterdData);
  };

  const showDeatailView = (item: any) => {
    setSelectedItem(item);
    setShowDetailView(true);
  };

  const handleFilterReset = () => {
    setFilters([]);
    setFilteredData([]);
  };

  //handle comment buttons

  const handleNotesComment = async (data: any, id: any) => {
    if (data) {
      const postComment = { ...data, postedBy: currentUserName };

      dispatch(showLoader());
      try {
        const response = await fetch("/api/resource/note", {
          method: "POST",
          body: JSON.stringify({
            postComment,
            _id: id ? id.id : selectedItem._id,
          }),
        });
        if (response.ok) {
          const data = await response.json();
          await func();
          setShowDetailView(false);
          dispatch(hideLoader());
          if (data.success) {
            dispatch(
              showToaster({
                timer: 2000,
                description: data.message,
              })
            );
          }
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      dispatch(
        showToaster({
          timer: 2000,
          description: "Comment cannot be empty add atleast any comment",
        })
      );
    }
  };

  const handleSelectButton = () => {
    setShowCheckBox(!showCheckBox);
    setCheckedData([]);
  };

  const handleCheckboxData = (data: ResourceData) => {
    const { _id } = data;
    const isExist = checkedData.some((item: ResourceData) => item._id === _id);

    if (isExist) {
      const updatedData = checkedData.filter(
        (item: ResourceData) => item._id !== _id
      );
      setCheckedData(updatedData);
    } else {
      setCheckedData((prevData: ResourceData[]) => [...prevData, data]);
    }
  };

  const handleExpand = async (id: string) => {
    try {
      dispatch(showLoader());
      const response = await fetch("/api/file", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      });
      if (response.ok) {
        const blob = await response.blob();
        const blobUrl = URL.createObjectURL(blob);
        const link: any = document.createElement("a");
        link.href = blobUrl;
        link.target = "_blank";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
      dispatch(hideLoader());
    } catch (error) {
      dispatch(hideLoader());
      console.log("Error downloading file:", error);
    }
  };

  const handleDownload = async (id: string) => {
    try {
      dispatch<any>(showLoader());
      const response = await fetch("/api/file", {
        method: "POST",
        body: JSON.stringify({ id: id }),
      });

      if (response.ok) {
        const fileName = response.headers.get("Content-Disposition");
        const blob = await response.blob();
        downloadFiles(blob, fileName);
      }
      dispatch<any>(hideLoader());
    } catch (error) {
      console.log("Error downloading file:", error);
    }
  };
  return (
    <Fragment>
      <div className="flex flex-row mr-12 gap-2 items-center mt-4 text-gray-500">
        <Icon type="back" />
        <div className="flex flex-row justify-between items-center text-sm">
          <span>Dashboard</span>
          <Icon type="chev_right" />
          <span>Resource</span>
          <Icon type="chev_right" />
          <span>All Resource</span>
        </div>
      </div>

      <div className="flex justify-between items-center mt-2 font-bold">
        <div className="flex gap-12 items-center">
          <div className={`font-bold text-xl`}>
            {id ? "My Resource" : "Resource"}
          </div>
          <div
            style={{ color: `${COLOR.BLUE}` }}
            className={`flex justify-evenly items-center gap-4 `}
          >
            <Link href={"/resources/add"}>
              <div className="flex items-center gap-2">
                <Icon type="plus" fill={COLOR.BLUE} />
                Add Resource
              </div>
            </Link>
            {!id && (
              <Link href={`/resources/${currentUserId}`}>
                <div className="flex items-center gap-2">My Resource</div>
              </Link>
            )}

            <Link href={"/vendors"}>
              {" "}
              <div className="flex items-center gap-2">
                <Icon type="vendor" fill={COLOR.BLUE} />
                All vendors
              </div>
            </Link>
          </div>
        </div>
      </div>
      <div className="flex justify-between items-center mt-4">
        <SearchBar
          type="text"
          placeholder="Search vendors"
          onChange={handleSearchFilter}
          width="300px"
          className={"border-2 rounded-lg"}
        />
        <div className="flex justify-between items-center gap-4">
          {" "}
          <div
            className={`flex items-center gap-2 border rounded-md p-2 font-medium text-center ${
              showCheckBox && "text-blue-600 border-blue-600 bg-blue-200"
            }`}
            onClick={handleSelectButton}
          >
            <Icon
              type="Select"
              width="20"
              height="20"
              fill={showCheckBox ? "blue" : ""}
            />
            Select
          </div>
          <div className="border rounded-md p-2">
            <Icon
              type="filter"
              width="20"
              height="20"
              onClick={() => setIsFilterOpen(true)}
            />
          </div>
        </div>
      </div>

      <div
        className={`${
          (showCheckBox || (!isFilterOpen && filters.length > 0)) &&
          "bg-gray-100 rounded-lg"
        } mt-2 pb-2 mx-2 min-h-[80%]`}
      >
        <div className="mx-2 pt-2">
          {isFilterOpen && (
            <Filter
              config={filterConfigData}
              setPopup={() => setIsFilterOpen(!isFilterOpen)}
              resetFilterHandler={handleFilterReset}
              values={filters}
              filterHandler={handleApplyFilter}
              selectedFilter={handleSelectedFilter}
              btnConfig={filterBtnConfig()}
            />
          )}

          {filters.length > 0 && (
            <div className="flex flex-row gap-2 ">
              {filters?.map((item: any, index: number) => {
                return (
                  <div
                    key={index}
                    className="apply-filter flex items-center text-center gap-2 border rounded-lg px-2 py-1 "
                  >
                    <div className="text-white font-medium">
                      {typeof item.value === "object" ? (
                        <div>{`${item.value.minimum} - ${item.value.maximum} `}</div>
                      ) : (
                        item.value
                      )}
                    </div>
                    <Icon
                      type="close"
                      width="18"
                      height="18"
                      fill="white"
                      onClick={() => handleRemoveFilterIcon(item)}
                    />
                  </div>
                );
              })}
              {!isFilterOpen && (
                <div
                  className="add-filter flex items-center text-center gap-2 border rounded-lg px-2 py-1 font-medium"
                  onClick={() => setIsFilterOpen(true)}
                >
                  <Icon type="plus" fill="blue" />
                  Add Filter
                </div>
              )}
            </div>
          )}
        </div>

        <div className="mx-2 pt-2">
          {filteredData?.length > 0
            ? ` ${filteredData.length} Results`
            : !isFilterOpen && filters?.length > 0
            ? ""
            : ` ${resourceStore?.length} Results`}
        </div>

        <div className="mx-3">
          <Card
            data={
              filteredData?.length > 0
                ? filteredData
                : !isFilterOpen && filters.length > 0
                ? []
                : resourceStore || []
            }
            config={resourceCardConfig}
            columns={5}
            gridCols={3}
            showDetailsIcon
            showCheckBox={showCheckBox}
            checkedData={checkedData}
            selectData={handleCheckboxData}
            showPopup={showPopup}
            showDetailView={showDeatailView}
            detailsPopup={(item: any) => {
              return showPopupDetails(item);
            }}
          />
        </div>
      </div>

      {showCheckBox && (
        <div className="w-1/2 fixed bottom-20 left-1/2 z-10 transform -translate-x-1/2 ml-20 bg-black p-2.5 border rounded-2xl text-white">
          <div className="flex justify-between items-center gap-2">
            <div className="flex items-center gap-2">
              <Icon
                type="close"
                width="18"
                height="18"
                fill="white"
                onClick={handleSelectButton}
              />
              {checkedData.length} Selected
            </div>
            <div className="flex items-center gap-2">
              {/* <span className="flex items-center gap-2 border rounded-lg bg-gray-800 p-2">
                <Icon type="excel" width="20" height="20" fill="green" />{" "}
                Expoert To CSV
              </span> */}
              <span
                className="flex items-center gap-2 border rounded-lg bg-gray-800 p-2"
                onClick={handleMultipleDownloadResume}
              >
                <Icon type="download" width="15" height="15" fill="white" />
                Resume
              </span>
              {/* <span className="border rounded-lg bg-blue-700 p-2">
                Align Profile
              </span> */}
              {id && (
                <span
                  className="border rounded-lg bg-red-500 p-3"
                  onClick={() => handleDeleteResource(checkedData)}
                >
                  <Icon type="trash" fill="white" />
                </span>
              )}
            </div>
          </div>
        </div>
      )}

      <DetailView
        detailView={showDetailView}
        heading="Resource Detail"
        setShowDetail={setShowDetailView}
        selectedItem={selectedItem}
        handleComment={handleNotesComment}
        headerConfig={detailHeaderConfig()}
        tabsConfig={tabsConfig}
        detailConfig={detailConfig()}
        handleDownload={handleDownload}
        handleExpand={handleExpand}
      />
      <style>
        {`
        .apply-filter{
          background-color:blue;

        }
        .add-filter{
          color:blue;
          background-color:#3371EA1A;
        }
         .popup-wrapper {
          position:fixed;
          background-color:  rgba(0, 0, 0, 0.5);
          inset: 0px;
          z-index:10;
         }
         .popup{
          width:170px;
          position:absolute;
          top:0;
          right:0;
          z-index:20;
          border: 1px solid #C6C6CA;
          border-radius: 8px;

         }
         .details-item{
          font-weight:500;
          padding:3px 3px;
          cursor:pointer;
        }
         .details-item:last-child{
          border:0;
        }
         `}
      </style>
    </Fragment>
  );
}
