import Icon from "@/lib/svg";
import { resourceStatusArray, technologies, uniqueStatesAndDistricts } from "../constant";

const filterConfig = (vendorArray: any, accountManager: any) => {
  return {
    Status: {
      val: "status",
      options: resourceStatusArray,
      function: (
        data: any,
        value: string,
        handleClick: any,
        selectedItem: number
      ) => {
        const result = data.map((entry: any, index: number) => {
          return (
            <div
              key={index}
              className={`flex justify-start items-center px-2 gap-2 ${
                selectedItem === index ? "selectedItem" : "not-selected"
              }`}
            >
              {
                <Icon
                  type={entry.icon.type}
                  width={entry.icon.width}
                  height={entry.icon.height}
                  fill={entry.icon.fill}
                />
              }
              <div onClick={() => handleClick(value, entry.value, index)}>
                {entry.label}
              </div>
            </div>
          );
        });
        return (
          <div className="flex flex-col justify-center items-left p-2">
            {result}
          </div>
        );
      },
    },
    Budget: {
      val: "costing",
      form: {
        "": {
          config: {
            " ": {
              type: "number",
              elementType: "FormInput",
              disablePaste: false,
              placeholder: "Min",
              name: "minimum",
              icon: {
                type: "rupee",
                width: "20",
                height: "18",
              },
            },
            "": {
              type: "number",
              elementType: "FormInput",
              disablePaste: false,
              placeholder: "Max",
              name: "maximum",
              icon: {
                type: "rupee",
                width: "20",
                height: "18",
              },
            },
          },
        },
      },
    },
    Experience: {
      id: 3,
      val: "experience",
      form: {
        Experience: {
          config: {
            "": {
              type: "number",
              elementType: "FormInput",
              placeholder: "Year Min",
              disablePaste: false,
              name: "minimum",
            },
            " ": {
              type: "number",
              elementType: "FormInput",
              placeholder: "Year Max",
              disablePaste: false,
              name: "maximum",
            },
          },
        },
      },
    },
    Technology: {
      id: 4,
      val: "technology",
      options: technologies,
      optionValue: "value",
      searchBar: {
        placeHolder: "Search technology",
      },
    },
    Vendor: {
      id: 5,
      val: "vendorName",
      options: vendorArray,
      optionValue: "vendorName",
      searchBar: {
        placeHolder: "Search Vendor",
      },
    },
    Location: {
      id: 6,
      val: "location",
      options: uniqueStatesAndDistricts,
      optionValue: "value",
      searchBar: {
        placeHolder: "Search Location",
      },
    },
    Manager: {
      val: "accountManager",
      id: 7,
      options: accountManager,
      optionValue: "name",
    },
  };
};

export default filterConfig;

export const filterBtnConfig = () => [
  {
    id: 1,
    type: "button",
    size: "small",
    children: "ApplyFilter",
    variant: "primary",
    className: "ml-3 my-3",
  },
  {
    id: 2,
    type: "reset",
    size: "small",
    variant: "secondary",
    children: "Reset",
    className: "border rounded-lg mr-3 my-3",
  },
];

export const filterFormBtnConfig = () => [
  {
    id: 1,
    type: "button",
    size: "small",
    children: "Apply",
    variant: "primary",
    className: "ml-3 my-3",
  },
  {
    id: 2,
    type: "reset",
    size: "small",
    variant: "secondary",
    children: "Reset",
    className: "border rounded-lg mr-3 my-3",
  },
];
