import Icon from "@/lib/svg";
import { formatNumberToCurrency } from "@/utils";

export const detailHeaderConfig = () => {
  return {
    Name: {
      val: "name",
      class: " col-span-3 text-lg font-bold leading-9",
    },
    Status: {
      val: "status",
      class:
        "border rounded-lg bg-green-100 text-center mx-3 my-3 text-green-900 font-medium",
    },
    "Vendor Name": {
      val: "vendorName",
      class: "col-span-2 my-3",
      function: (data:{vendorName: string}) => {
        return (
          <div className="flex items-center gap-2">
            <Icon type="vendor" />
            {data.vendorName}
          </div>
        );
      },
    },
  };
};

export const detailConfig = () => {
  return {
    Vendor: {
      val: "vendorName",
      entry:"vendorName",
      key:"text-m",
      class: "my-2",
      value: "col-span-2",
    },
    Location: {
      val: "location",
      key:"text-m",
      class: "my-2",
      value: "col-span-2",
    },
    Experience: {
      val: "experience",
      class: "my-2",
      key:"text-m",
      value: "col-span-2",
      function: (data: number) => {
        return `${data} Years`;
      },
    },
    Costing: {
      val: "costing",
      key:"text-m",
      class: "my-2",
      value: "col-span-2",
      function: (data: number) => {
        const result = formatNumberToCurrency(data);
        return (
          <div className="flex items-center">
            <Icon type="rupee" height="15" />
            {result}
          </div>
        );
      },
    },
    Manager: {
      class: "my-2",
      key:"text-m",
      val: "accountManager",
      line: true,
      value: "col-span-2",
    },
    Expertise: {
      val: "technology",
      key: "col-span-3 text-m",
      value: "col-span-3",
      function: (data: any) => {
        const result = data?.map(
          (item: { techStack: string }, index: number) => {
            return (
              <div
                key={index}
                className="rounded border-b-4 outline_bold text-center p-1"
              >
                {item.techStack}
              </div>
            );
          }
        );
        return <div className="flex flex-wrap gap-2 py-2">{result}</div>;
      },
    },
    Resume: {
      val: "resume",
      key: "col-span-3 mt-2 text-m",
      value: "col-span-3",
      function: (data: any, handleExpand: any, download: any) => {
        return (
          <>
            {data && (
              <div className="flex justify-between items-center border-2 rounded-lg px-2 py-1">
                <div className="flex items-center gap-2">
                <span className="bg-blue-100 rounded-xl p-2">
                  <Icon
                    type="attachment"
                    width="20"
                    height="20"
                    fill="#1A68F3"
                  />
                </span> 
                {data.filename}
              </div>
                <div className="flex items-center gap-5">
                  <Icon
                    type="expand"
                    width="18"
                    height="15"
                    onClick={() => handleExpand(data._id)}
                  />
                  <Icon
                    type="download"
                    width="18"
                    height="15"
                    onClick={() => download(data._id)}
                  />
                </div>
              </div>
            )}
          </>
        );
      },
    },
  };
};

export const tabsConfig = [
  {
    name: "Details",
  },
  {
    name: "Alignments",
  },
  {
    name: "Notes",
  },
];

export const notesPublishconfiguration = {
  id: 1,
  type: "text",
  placeholder: "Add a comment",
  disablePaste: false,
  rows: 3,
  name: "comment",
  height: "90",
};

export const notesReplyConfiguration = {
  id: 2,
  type: "text",
  placeholder: "Add a reply",
  disablePaste: false,
  name: "reply",
  rows: 1,
  width: "0%",
  height: "20px",
};

export const notesBtnconfig = (btnName: string, size?: string) => [
  {
    id: 1,
    type: "button",
    size: size || "medium",
    children: btnName,
    // className: "w-36",
    variant: "primary",
  },
];
