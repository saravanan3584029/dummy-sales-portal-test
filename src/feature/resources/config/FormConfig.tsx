import {
  resourceStatusArray,
  uniqueStatesAndDistricts,
  technologies,
  workMode,
} from "../constant";

export const formLinkConfig = (id: any) => {
  return {
    "My Resources": {
      id: 1,
      path: `/resources/${id}`,
      labelName: "My Resources",
    },
    "All Resources": {
      id: 2,
      path: `/resources`,
      labelName: "All Resources",
      icon: {
        type: "resource",
        width: "16",
        height: "16",
        fill: "#1A68F3",
      },
    },
    "All Vendors": {
      id: 3,
      path: "/vendors",
      labelName: "All Vendors",
      icon: {
        type: "vendors",
        width: "16",
        height: "18",
        fill: "#1A68F3",
      },
    },
  };
};

export const resourceFormConfig = (vendorArray: any, accountManager: any) => {
  return {
    section1: {
      class: "mt-5",
      config: {
        Name: {
          elementType: "FormInput",
          placeholder: "Enter Resource Name",
          disablePaste: false,
          name: "name",
        },
        Vendor: {
          elementType: "FormDropdown",
          placeholder: "Select",
          options: vendorArray,
          name: "vendorName",
          val: "vendorName",
          disablePaste: true,
          height: "200",
          icon: {
            type: "vendors",
          },
          searchBar: {
            placeholder: "Search here",
          },
        },
        Status: {
          elementType: "FormDropdown",
          disablePaste: true,
          options: resourceStatusArray,
          val: "value",
          name: "status",
          placeholder: {
            type: "selected-dot",
            name: "Available",
          },
        },
      },
    },

    section2: {
      id: 2,
      class: "mt-5",
      config: {
        Details: {
          className: "row-span-4",
        },
        Phone: {
          elementType: "FormInput",
          disablePaste: false,
          name: "phone",
          type: "number",
          icon: {
            label: "+91",
          },
        },

        "Email Address": {
          elementType: "FormInput",
          placeholder: "Email",
          disablePaste: false,
          variant: "border",
          name: "email",
          type: "email",
        },
        Costing: {
          type: "number",
          elementType: "FormInput",
          disablePaste: false,
          name: "costing",
          icon: {
            type: "rupee",
            width: "20",
            height: "18",
          },
        },
        Experience: {
          type: "number",
          elementType: "FormInput",
          placeholder: "Years",
          disablePaste: false,
          name: "experience",
          width: "50",
        },
        Location: {
          elementType: "FormDropdown",
          disablePaste: true,
          placeholder: "Select",
          options: uniqueStatesAndDistricts,
          height: "200",
          val: "value",
          name: "location",
          icon: {
            type: "location",
          },
          searchBar: {
            placeholder: "Search here",
          },
        },
        "Work mode": {
          elementType: "FormDropdown",
          disablePaste: true,
          placeholder: "Select",
          val: "value",
          options: workMode,
          name: "workMode",
        },

        Resume: {
          elementType: "FormUploadInput",
          placeholder: "Upload Attachment",
          name: "resume",
          accept: ".pdf,.doc,.docx,.png,.jpg,.jpeg",
        },
      },
    },
    section3: {
      id: 3,
      class: "mt-5",
      config: {
        "Technology ": {
          Feild: {
            startText: "Click on the star ",
            icon: {
              type: "star",
            },
            endText: "icon to mark a skill as primary.",
          },
        },
        Technology: {
          elementType: "FormSelectMultipleItem",
          disablePaste: false,
          options: technologies,
          val: "value",
          name: "technology",
          width: "300",
          className: "col-span-2",
          height: "200",
          icon: {
            type: "star-outline",
            width: "15",
            height: "18",
          },
          searchBar: {
            placeholder: "Search here",
          },
        },
      },
    },

    section4: {
      id: 4,
      class: "mt-5",
      config: {
        "Account Manager ": {},
        "Account Manager": {
          elementType: "FormSelectMultipleItem",
          options: accountManager,
          disablePaste: false,
          name: "accountManager",
          className: "col-span-2",
          val:"name",
          width: "300",
          icon: {
            image: "/images/profile.png",
          },
        },
      },
    },
  };
};

export const resourceBtnConfig = (btnName: string, loading?: boolean) => [
  {
    id: 1,
    type: "button",
    size: "medium",
    children: btnName,
    variant: "primary",
    disabled: loading,
    loading: loading,
  },
  {
    id: 2,
    type: "reset",
    size: "medium",
    variant: "secondary",
    children: "Cancel",
    className: "border rounded-lg",
  },
];

export const resourceDetailConfig = [
  {
    name: "Details",
  },
  {
    name: "Alignments",
  },
  {
    name: "Notes",
  },
];

export const notesPublishconfiguration = {
  id: 1,
  type: "text",
  placeholder: "Add a comment",
  disablePaste: false,
  rows: 3,
  name: "comment",
};

export const notesReplyConfiguration = {
  id: 2,
  type: "text",
  placeholder: "Add a reply",
  disablePaste: false,
  name: "reply",
  rows: 1,
  width: "0%",
  height: "20px",
};

export const notesBtnconfig = (btnName: string, size?: string) => [
  {
    id: 1,
    type: "button",
    size: size || "medium",
    children: btnName,
    // className: "w-36",
    variant: "primary",
  },
];
