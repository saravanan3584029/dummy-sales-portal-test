import validationMessages from "@/utils/validation/validationMessages";

export const resourceValidations = {
  name: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "name",
      message: validationMessages.isText,
    },
  },
  vendorName: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  status: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  phone: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "mobile",
      message: validationMessages.isMobile,
    },
  },

  email: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "email",
      message: validationMessages.isEmail,
    },
  },

  costing: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "positiveNonzeroFloat",
      message: validationMessages.isNonzeroNumber,
    },
  },

  experience: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "positiveNonzeroFloat",
      message: validationMessages.isNonzeroNumber,
    },
  },

  location: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  workMode: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  resume: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  technology: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "isPrimary",
      message: validationMessages.isNotPrimary,
    },
  },

  accountManager: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
};
