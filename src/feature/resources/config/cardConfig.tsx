import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import { formatNumberToString, getTimeDifference } from "@/utils";

export const resourceCardConfig: any = {
  Name: {
    val: "name",
    class: "col-span-5 font-semibold text-base capitalize leading-5",
  },
  vendorName: {
    icon: { type: "vendor", width: "16", height: "16", fill: COLOR.GRAY_4 },
    val: "vendorName",
    entry:"vendorName",
    class: `col-span-5 my-2 text-m text-gray-400`,
  },
  status: {
    val: "status",
    class:
      "font-medium rounded-xl bg-green-100 mt-5 px-2 py-1 text-center text-green-800 text-m",
  },
  experience: {
    icon: { type: "work-duration", fill: "#AAAAAF" },
    val: "experience",
    class: "col-span-2 mt-6 ml-3 text-gray-500 text-m",
    function: (data: number) => {
      return `${data} yrs`;
    },
  },
  costing: {
    icon: { type: "dot", fill: "#AAAAAF" },
    val: "costing",
    class: "col-span-2 mt-6 text-gray-500 text-m",
    function: (data: number) => {
      const number = formatNumberToString(data);
      return <div className="flex items-center gap-1"><Icon type="rupee" width="16" height="16" fill= "#66666E"/>{number}</div>;
    },
  },
  technology: {
    val: "technology",
    class: "col-span-5 mt-3 text-m",
    function: (data: any) => {
      const primary = data.filter((item: any) => item.isPrimary === true);
      const secondary = data.filter((item: any) => item.isPrimary === false);
      const originalArray = [...primary, ...secondary];
      const renderData = originalArray.slice(0, 3);
      const remainingData = originalArray.length - 3;
      const result = renderData.map((entry: any, index: number) => {
        return (
          <div
            key={index}
            className={`flex items-center gap-2 text-center border-2 rounded-lg p-1 outline_bold`}
          >
            {entry.techStack}
            {entry.isPrimary && (
              <Icon type="star" width="10" height="10" fill={COLOR.YELLOW} />
            )}
          </div>
        );
      });
      remainingData > 0 &&
        result.push(
          <div className="border-b-4 outline_bold text-center p-1 m-1">
            + {remainingData}
          </div>
        );
      return (
        <div className="flex items-center justify-start gap-x-2 font-medium">{result}</div>
      );
    },
  },
  location: {
    icon: { type: "location" },
    val: "location",
    class: "col-span-3 text-lg mt-3 text-gray-500 text-m",
  },
  workMode: {
    icon: { type: "dot", fill: "#AAAAAF" },
    val: "workMode",
    class: "col-span-2 mt-3 text-gray-500 text-m",
  },
  accountManager: {
    val: "accountManager",
    class: " mt-6 col-span-3 text-gray-500 text-m",
    icon: {
      type: "user-circle",
      fill: "#AAAAAF",
    },
  },
  createdAt: {
    val: "createdAt",
    class: "col-span-2 mt-6 text-sm text-gray-400",
    icon: { type: "clock", width: "15", fill: "#AAAAAF" },
    function: (data: any) => {
      const formattedDate = getTimeDifference(data);
      return `${formattedDate} ago`;
    },
  },
};
