export type multiselect = {
  name: string;
  isPrimary: boolean;
};
type Tech = {
  item: string;
  isPrimary: boolean;
};

export type ResourceData = {
  _id?: string;
  name: string;
  vendorName: string;
  status: string;
  phone: number;
  email: string;
  costing: number;
  experience: number;
  location: string;
  workMode: string;
  resume: {_id:string}|string;
  technology: Tech[];
  accountManager: string[];
};
export type FilterTypes = {
  key?: string;
  value?: string;
};
