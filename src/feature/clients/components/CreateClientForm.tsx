"use client";
import Icon from "@/lib/svg";
import { getItemFromLocalStorage, letterFormater } from "@/utils";
import FormRender from "@/utils/form/formRender";
import { usePathname, useRouter } from "next/navigation";
import { clientFormFormConfig } from "../config/clientFormConfig";
import Notes from "@/shared-components/components/Note";
import { hideLoader, showLoader, showToaster } from "@/store/common";
import { useDispatch, useSelector } from "react-redux";
import { useCallback, useEffect, useState } from "react";
import { clientValidations } from "../config/validationConfig";
import { formLinkConfig } from "../config";
import { resourceBtnConfig } from "@/feature/resources/config/FormConfig";
import { setClient, setUserData } from "@/store/user";
import ndaFile from "@/server/models/ndaFile";

export default function CreateClientForm({
  columnNum = 3,
  id,
}: {
  columnNum?: number;
  id?: string | string[];
}) {
  const param = usePathname();
  const dispatch = useDispatch();
  const router = useRouter();
  const [updateData, setUpdateData] = useState<any>();
  const userStore = useSelector((store: any) => store.user);
  const { user } = userStore;
  const [currentUserId, setCurrentUserId] = useState<string>();
  const [currentUserName, setCurrentUserName] = useState<string>();

  const formData = clientFormFormConfig(user);
  const storeData = useSelector((store: any) => store?.client);

  const fetchSingleUser = useCallback(async () => {
    try {
      const response = await fetch(`/api/clients/` + id);
      if (response.ok) {
        const data = await response.json();
        setUpdateData(data.response);
      }
    } catch (error) {
      console.error("Fetch Error:", error);
    }
  }, [id]);

  useEffect(() => {
    setCurrentUserId(getItemFromLocalStorage("user_id"));
    setCurrentUserName(getItemFromLocalStorage("user_name"));
  }, [currentUserName]);

  useEffect(() => {
    const fetchData = async () => {
      if (id && storeData?.client.length === 0) {
        await fetchSingleUser();
      } else {
        const data = storeData?.client?.find((item: any) => item._id === id);
        setUpdateData(data);
      }
    };
    fetchData();
  }, [id, storeData, fetchSingleUser]);

  const createClientHandler = async (data: any) => {
    const { ndaFile, ...re } = data;
    dispatch<any>(showLoader());
    const formData = new FormData();
    if (ndaFile) {
      formData.append("file", ndaFile);
    }
    formData.append("jsonData", JSON.stringify(re));
    try {
      const response = await fetch("/api/clients", {
        method: "POST",
        body: formData,
      });
      dispatch<any>(hideLoader());
      if (response.ok) {
        const data = await response.json();
        if (data.success) {
          dispatch<any>(
            showToaster({
              description: data.message,
              timer: 1000,
            })
          );
          router.push("/clients");
        } else {
          dispatch<any>(
            showToaster({
              description:
                data.message ||
                (Object.values(data.error.errors) as Error[])[0]?.message,
              timer: 3000,
              variant: "red",
            })
          );
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const updateClientHandler = async (data: any) => {
    let updateObject: any = {};
    Object.keys(data).map((ele) => {
      if (ele === "ndaFile") {
        if (data[ele] !== updateData[ele]?.filename) {
          updateObject = { ...updateObject, [ele]: data[ele] };
        }
      } else if (Array.isArray(data[ele])) {
        const hasDifference = data[ele].some((item: any, index: number) => {
          if (typeof item === "string") {
            return item !== updateData[ele][index];
          } else {
            return (
              JSON.stringify(item) !== JSON.stringify(updateData[ele][index])
            );
          }
        });
        if (hasDifference) {
          updateObject = { ...updateObject, [ele]: data[ele] };
        }
      } else if (data[ele] !== updateData[ele]) {
        updateObject = { ...updateObject, [ele]: data[ele] };
      }
    });
    try {
      if (Object.keys(updateObject).length > 0) {
        dispatch(showLoader());
        const form = new FormData();
        const { ndaFile, ...entry } = updateObject;
        const dataToUpdate = { ...entry, _id: id };
        ndaFile && form.append("file", ndaFile);
        form.append("jsonData", JSON.stringify(dataToUpdate));
        const response = await fetch(`/api/clients`, {
          method: "PUT",
          body: form,
        });
        dispatch(hideLoader());
        if (response.ok) {
          const data = await response.json();
          if (data.success) {
            dispatch<any>(
              showToaster({
                description: data.message,
                timer: 1000,
              })
            );
            router.push("/clients");
          } else {
            dispatch<any>(
              showToaster({
                description:
                  data.message ||
                  (Object.values(data.error.errors) as Error[])[0]?.message,
                timer: 3000,
                variant: "red",
              })
            );
          }
        }
      } else {
        dispatch<any>(
          showToaster({
            description: "No entry has been updated in the form.",
            timer: 2000,
            variant: "green",
          })
        );
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    const accountManager = async () => {
      const response = await fetch(`/api/user/`);
      if (response.ok) {
        const data = await response.json();
        dispatch<any>(setUserData(data.response));
      }
    };
    accountManager();
  }, []);

  const handleNotesComment = async (data: any, replyId: any) => {
    if (id) {
      if (data) {
        const postComment = { ...data, postedBy: currentUserName };
        try {
          dispatch(showLoader());
          const response = await fetch("/api/clients/note", {
            method: "POST",
            body: JSON.stringify({
              postComment,
              _id: replyId ? replyId.id : id,
            }),
          });
          if (response.ok) {
            const data = await response.json();
            await fetchSingleUser();
            dispatch(hideLoader());
            if (data.success) {
              dispatch(
                showToaster({
                  timer: 2000,
                  description: data.message,
                })
              );
            }
          }
          dispatch(hideLoader());
        } catch (error) {
          console.log(error);
        }
      } else {
        dispatch(
          showToaster({
            timer: 2000,
            description: "Comment cannot be empty add atleast any comment",
          })
        );
      }
    } else {
      dispatch(
        showToaster({
          timer: 2000,
          description:
            "Prior to vendor creation, commenting functionality shall be restricted.",
        })
      );
    }
  };

  return (
    <div>
      <div className="flex flex-row mr-12 gap-2 items-center mt-4 text-gray-500 cursor-pointer mb-6">
        <Icon type="back" />
        <div className="flex flex-row justify-between items-center text-m">
          <span>Dashboard</span>
          <Icon type="chev_right" />
          <span>Clients</span>
          <Icon type="chev_right" />
          <span>All Clients</span>
        </div>
      </div>

      <FormRender
        formHeading={`${id ? "Update Client" : "Create Client"}`}
        formColumn={columnNum}
        formLink={formLinkConfig("")}
        formLinkStyle={`text-blue-500 font-bold`}
        formLabelStyle="mb-2 "
        formLine={true}
        formData={formData}
        initialStaticData={
          id
            ? {
                clientName: updateData?.clientName,
                location: updateData?.location,
                paymentTerms: updateData?.paymentTerms,
                ndaFile: updateData?.ndaFile.filename,
                accountManager: updateData?.accountManager,
              }
            : {
                clientName: "",
                location: "",
                paymentTerms: "",
                ndaFile: "",
                accountManager: [],
              }
        }
        initialNestedData={{
          contactPersons: id ? updateData?.contactPersons : [],
        }}
        validations={clientValidations}
        submitForm={id ? updateClientHandler : createClientHandler}
        btnConfig={resourceBtnConfig(id ? "Update" : "Create")}
        btnStyle={false}
      />

      <Notes handleComment={handleNotesComment} notesData={updateData} />
    </div>
  );
}
