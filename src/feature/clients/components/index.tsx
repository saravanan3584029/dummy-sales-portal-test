"use client";

import {
  hideLoader,
  hidePopup,
  showLoader,
  showPopup,
  showToaster,
} from "@/store/common";
import { useDispatch, useSelector } from "react-redux";
import { ChangeEvent, useEffect, useState } from "react";
import { getClientsData } from "@/store/client";
import Icon from "@/lib/svg";
import { COLOR } from "@/constants/theme";
import Link from "next/link";
import {
  applyFiltersToData,
  areEqual,
  base64ToBlob,
  downloadFiles,
  getItemFromLocalStorage,
} from "@/utils";
import SearchBar from "@/shared-components/components/SearchBar";
import Card from "@/shared-components/components/Card";
import Filter from "@/shared-components/components/Filter";
import filterConfiguration from "@/feature/requirements/config/FilterConfig";
import { filterBtnConfig } from "@/feature/resources/config/filterConfig";
import { FilterTypes } from "@/feature/resources/config/types";
import { ClientCardConfig } from "@/feature/clients/config/clientCardConfig";
import { updateConfig } from "@/feature/requirements/config";
import {
  RequirementStatus,
  requirementPriority,
} from "@/feature/requirements/constant";
import filterConfig from "../config/clientFilterConfig";
import DetailView from "@/shared-components/components/DetailView";
import {
  HeaderConfig,
  detailConfig,
  tabsConfig,
} from "../config/clientDetailsConfig";

export default function Clients({ id }: { id?: string }) {
  const dispatch = useDispatch();

  const [filteredData, setFilteredData] = useState<any>([]);
  const [popup, setPopup] = useState<any>({});
  // select button
  const [showCheckBox, setShowCheckBox] = useState<boolean>(false);
  const [checkedData, setCheckedData] = useState<any>([]);
  //filter
  const [isFilterOpen, setIsFilterOpen] = useState<Boolean>(false);
  const [filters, setFilters] = useState<any>([]);

  // deatail view
  const [showDetailView, setShowDetailView] = useState<any>();
  const [selectedItem, setSelectedItem] = useState<any>();

  const [updateStatusData, setUpdateStatusData] = useState<boolean>(false);
  const [updatePriorityData, setUpdatePriorityData] = useState<boolean>(false);

  const userStore = useSelector((store: any) => store.user);
  const { user, requirement } = userStore;

  const storeData = useSelector((store: any) => store.client);

  const filterConfiData = filterConfig(user, requirement);

  const [currentUserId, setCurrentUserId] = useState<string>();
  const [currentUserName, setCurrentUserName] = useState<string>();
  const [currentUserRole, setCurrentUserRole] = useState<string>();

  useEffect(() => {
    setCurrentUserId(getItemFromLocalStorage("user_id"));
    setCurrentUserName(getItemFromLocalStorage("user_name"));
    setCurrentUserRole(getItemFromLocalStorage("user_role"));
  }, []);

  const func = async () => {
    dispatch<any>(showLoader());
    const response = await fetch(
      `/api/clients${id ? `/myClients/${id}` : ""}`,
      {
        method: "GET",
      }
    );

    if (response.ok) {
      const responseData = await response.json();
      dispatch(getClientsData(responseData.response));
    }
    dispatch<any>(hideLoader());
  };

  useEffect(() => {
    func();
  }, []);

  // search filter handler
  const handleSearchFilter = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const filterData = storeData?.client?.filter(
      (user: any) =>
        user.clientName?.toLowerCase()?.indexOf(value.toLowerCase()) > -1
    );
    setFilteredData(filterData);
  };

  const handleSelectButton = () => {
    setShowCheckBox(!showCheckBox);
    setCheckedData([]);
  };

  const handleFilterReset = () => {
    setFilters([]);
    setFilteredData([]);
  };

  // handle a apply filter button
  const handleApplyFilter = () => {
    const filterdData = applyFiltersToData(storeData?.client, filters);
    console.log(filteredData);
    const uniqueFilteredData = filterdData?.filter((data: any) => {
      return !filteredData?.some((prevData: any) => areEqual(data, prevData));
    });
    setFilteredData([...(filteredData || []), ...(uniqueFilteredData || [])]);
    setIsFilterOpen(false);
  };

  //handle a selected filter
  const handleSelectedFilter = (key: string, value: any) => {
    if (typeof value === "object") {
      setFilters([...filters, { key: key, value: { ...value } }]);
    } else {
      const existingFilterIndex = filters.findIndex(
        (filter: { value: string }) => filter.value === value
      );
      setFilteredData([]);
      setFilters((prevFilters: any) => {
        if (existingFilterIndex !== -1) {
          const updatedFilters = [...prevFilters];
          updatedFilters.splice(existingFilterIndex, 1);
          return updatedFilters;
        } else {
          return [...prevFilters, { key: key, value: value }];
        }
      });
    }
  };

  const handleRemoveFilterIcon = async (item: FilterTypes) => {
    const removedItem = filters.filter(
      (entry: any) => entry.key !== item.key || entry.value !== item.value
    );
    setFilters(removedItem);
    const filterdData = applyFiltersToData(storeData?.client, removedItem);
    setFilteredData(filterdData);
  };

  const handleCheckboxData = (data: any) => {
    const { _id } = data;
    const isExist = checkedData.some((item: any) => item._id === _id);
    if (isExist) {
      const updatedData = checkedData.filter((item: any) => item._id !== _id);
      setCheckedData(updatedData);
    } else {
      setCheckedData((prevData: any) => [...prevData, data]);
    }
  };

  const showCardPopup = (item: any) => {
    setPopup(item);
  };

  //card button popup
  const showPopupDetails = (item: any) => {
    return Object.keys(popup).length && popup?._id === item?._id ? (
      <>
        <div
          role="presentation"
          className={`fixed inset-0 bg-black bg-opacity-30  z-10 justify-center items-center flex outline-none focus:outline-none`}
          onClick={() => setPopup({})}
        />
        <ul className="popup bg-white p-2">
          <li
            className="details-item "
            onClick={() => {
              setShowDetailView(true);
              setSelectedItem(item);
              setUpdatePriorityData(false);
              setUpdateStatusData(false);
              setPopup({});
            }}
          >
            View More
          </li>
          {/* <li className="details-item">Open Requirement</li> */}
          {(currentUserRole === "admin" ||
            item.accountManager.includes(currentUserName)) && (
            <>
              <li className="details-item">
                <Link href={`/clients/update/${item._id}`}>Edit Deatils</Link>
              </li>
            </>
          )}
          {(currentUserRole === "admin" ||
            item.accountManager.includes(currentUserName)) && (
            <>
              <li
                className="details-item text-red-500"
                onClick={() => handleDeleteClient(item._id)}
              >
                Delete Client
              </li>
            </>
          )}
        </ul>
      </>
    ) : null;
  };

  const showDeatailView = (item: any) => {
    setSelectedItem(item);
    setShowDetailView(true);
  };

  const cardConfig = ClientCardConfig();

  const handleMultipleDownloadResume = async () => {
    try {
      const fileId = checkedData.map((obj: any) => obj.ndaFile._id);

      dispatch<any>(showLoader());
      const response = await fetch("/api/getnda", {
        method: "POST",
        body: JSON.stringify({ id: fileId }),
      });

      if (response.ok) {
        const { files } = await response.json();
        files.forEach((fileData: any) => {
          const { content, contentType, filename } = fileData;
          const blob = base64ToBlob(content, contentType);
          downloadFiles(blob, filename);
        });
      }
      dispatch<any>(hideLoader());
    } catch (error) {
      console.log("Error downloading file:", error);
    }
  };

  //Delete clients
  const handleDeleteClient = async (data: any) => {
    let id;
    if (typeof data === "string") {
      id = data;
    } else {
      id = data.map((obj: { _id: string }) => obj._id);
    }

    dispatch<any>(showLoader());
    const response = await fetch("/api/clients", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id: id }),
    });
    await func();
    dispatch<any>(hideLoader());
  };

  const handleExpand = async (id: string) => {
    try {
      dispatch(showLoader());
      const response = await fetch("/api/getnda", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      });
      if (response.ok) {
        const blob = await response.blob();
        const blobUrl = URL.createObjectURL(blob);
        const link: any = document.createElement("a");
        link.href = blobUrl;
        link.target = "_blank";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
      dispatch(hideLoader());
    } catch (error) {
      dispatch(hideLoader());
      console.log("Error downloading file:", error);
    }
  };

  const handleDownload = async (id: any) => {
    try {
      dispatch<any>(showLoader());
      const response = await fetch("/api/getnda", {
        method: "POST",
        body: JSON.stringify({ id: id }),
      });

      if (response.ok) {
        const fileName = response.headers.get("Content-Disposition");
        const blob = await response.blob();
        downloadFiles(blob, fileName);
      }
      dispatch<any>(hideLoader());
    } catch (error) {
      console.log("Error downloading file:", error);
    }
  };

  const handleNotesComment = async (data: any, id: any) => {
    if (data) {
      const postComment = { ...data, postedBy: currentUserName };
      try {
        dispatch(showLoader());
        const response = await fetch("/api/clients/note", {
          method: "POST",
          body: JSON.stringify({
            postComment,
            _id: id ? id.id : selectedItem._id,
          }),
        });
        if (response.ok) {
          const data = await response.json();
          await func();
          setShowDetailView(false);
          dispatch(hideLoader());
          if (data.success) {
            dispatch(
              showToaster({
                timer: 2000,
                description: data.message,
              })
            );
          }
        }
        dispatch(hideLoader());
      } catch (error) {
        console.log(error);
      }
    } else {
      dispatch(
        showToaster({
          timer: 2000,
          description: "Comment cannot be empty add atleast any comment",
        })
      );
    }
  };

  return (
    <>
      <div className="flex flex-row mr-12 gap-2 items-center my-4  mb-8  text-gray-500">
        <Icon type="back" />
        <div className="flex flex-row justify-between items-center text-sm">
          <span>Dashboard</span>
          <Icon type="chev_right" />
          <span>Clients</span>
          <Icon type="chev_right" />
          <span>All Clients</span>
        </div>
      </div>

      <div className="flex justify-between items-center my-4 font-bold">
        <div className="flex gap-12 items-center">
          <div className={`font-bold text-xl`}>
            {id ? "My Clients" : "Clients"}
          </div>
          <div
            style={{ color: `${COLOR.BLUE}` }}
            className={`flex  justify-evenly items-center gap-4 text-s `}
          >
            <Link href={"/clients/create"}>
              <div className="flex items-center gap-2">
                <Icon type="plus" fill={COLOR.BLUE} />
                Add Clients
              </div>
            </Link>

            {id ? (
              <div>
                <Link href={`/clients`}>All Clients</Link>
              </div>
            ) : (
              <div>
                <Link href={`/clients/${currentUserId}`}>My Clients</Link>
              </div>
            )}
          </div>
        </div>
      </div>

      <div className="flex justify-between items-center mt-4">
        <SearchBar
          type="text"
          placeholder="Search client here"
          onChange={handleSearchFilter}
          width="300px"
          className={"border-2 rounded-lg"}
        />
        <div className="flex justify-between items-center gap-4">
          {" "}
          <div
            className={`flex items-center gap-2 border rounded-md p-2 font-medium text-center 
            ${showCheckBox && "text-blue-600 border-blue-600 bg-blue-200"}
            `}
            onClick={handleSelectButton}
          >
            <Icon
              type="Select"
              width="20"
              height="20"
              fill={showCheckBox ? "blue" : ""}
            />
            Select
          </div>
          <div className="border rounded-md p-2">
            <Icon
              type="filter"
              width="20"
              height="20"
              onClick={() => setIsFilterOpen(true)}
            />
          </div>
        </div>
      </div>

      <div
        className={`${
          (showCheckBox || (!isFilterOpen && filters.length > 0)) &&
          "bg-gray-100 rounded-lg"
        } mt-2 pb-2 mx-2 min-h-[80%]`}
      >
        <div className="mx-2 pt-2">
          {isFilterOpen && (
            <Filter
              config={filterConfiData}
              setPopup={() => setIsFilterOpen(!isFilterOpen)}
              resetFilterHandler={handleFilterReset}
              values={filters}
              filterHandler={handleApplyFilter}
              selectedFilter={handleSelectedFilter}
              btnConfig={filterBtnConfig()}
            />
          )}

          {filters.length > 0 && (
            <div className="flex flex-row gap-2 ">
              {filters?.map((item: any, index: number) => {
                return (
                  <div
                    key={index}
                    className="apply-filter flex items-center text-center gap-2 border rounded-lg px-2 py-1 "
                  >
                    <div className="text-white font-medium">
                      {typeof item.value === "object" ? (
                        <div>{`${item.value.minimum} - ${item.value.maximum} `}</div>
                      ) : (
                        item.value
                      )}
                    </div>
                    <Icon
                      type="close"
                      width="18"
                      height="18"
                      fill="white"
                      onClick={() => handleRemoveFilterIcon(item)}
                    />
                  </div>
                );
              })}
              {!isFilterOpen && (
                <div
                  className="add-filter flex items-center text-center gap-2 border rounded-lg px-2 py-1 font-medium"
                  onClick={() => setIsFilterOpen(true)}
                >
                  <Icon type="plus" fill="blue" />
                  Add Filter
                </div>
              )}
            </div>
          )}
        </div>

        <div className="mx-2 pt-2">
          {filteredData?.length > 0
            ? filteredData.length
            : !isFilterOpen && filters.length > 0
            ? []
            : storeData?.client?.length}{" "}
          Result
        </div>

        <div className="mx-3">
          <Card
            data={
              filteredData?.length > 0
                ? filteredData
                : !isFilterOpen && filters.length > 0
                ? []
                : storeData.client
            }
            config={cardConfig}
            columns={5}
            showDetailsIcon
            showCheckBox={showCheckBox}
            checkedData={checkedData}
            selectData={handleCheckboxData}
            showPopup={showCardPopup}
            showDetailView={showDeatailView}
            detailsPopup={(item: any) => {
              return showPopupDetails(item);
            }}
          />
        </div>
      </div>

      {showCheckBox && (
        <div className="w-1/2 fixed bottom-20 left-1/2 z-10 transform -translate-x-1/2 ml-20 bg-black p-2.5 border rounded-2xl text-white">
          <div className="flex justify-between items-center gap-2">
            <div className="flex items-center gap-2">
              <Icon
                type="close"
                width="18"
                height="18"
                fill="white"
                onClick={handleSelectButton}
              />
              {checkedData.length} Selected
            </div>
            <div className="flex items-center gap-2">
              {/* <span className="flex items-center gap-2 border rounded-lg bg-gray-800 p-2">
                <Icon type="excel" width="20" height="20" fill="green" />{" "}
                Expoert To CSV
              </span> */}
              <span
                className="flex items-center gap-2 border rounded-lg bg-gray-800 p-2"
                onClick={handleMultipleDownloadResume}
              >
                <Icon type="download" width="15" height="15" fill="white" />
                NDA
              </span>

              {currentUserRole === "admin" && (
                <span
                  className="border rounded-lg bg-red-500 p-3"
                  onClick={() => handleDeleteClient(checkedData)}
                >
                  <Icon type="trash" fill="white" />
                </span>
              )}
            </div>
          </div>
        </div>
      )}

      <DetailView
        detailView={showDetailView}
        heading="Client Detail"
        setShowDetail={setShowDetailView}
        selectedItem={selectedItem}
        handleComment={handleNotesComment}
        headerConfig={HeaderConfig()}
        tabsConfig={tabsConfig()}
        detailConfig={detailConfig()}
        handleDownload={handleDownload}
        handleExpand={handleExpand}
      />
      <style>
        {`
        .apply-filter{
          background-color:blue;

        }
        .add-filter{
          color:blue;
          background-color:#3371EA1A;
        }
         .popup-wrapper {
          position:fixed;
          background-color:rgba(0, 0, 0, 0.5);
          inset: 0px;
          z-index:10;
         }
         .popup{
          width:170px;
          position:absolute;
          top:0;
          right:0;
          z-index:20;
          border: 1px solid #C6C6CA;
          border-radius: 8px;

         }

         .status-Popup{
          width:170px;
          position:absolute;
          top:0;
          right:180px;
          z-index:10;
          border: 1px solid #C6C6CA;
          border-radius: 8px;
          background-color: white;
         }
         .details-item{
          font-weight:500;
          padding:3px 3px;
          cursor:pointer;
        }
      
        .selectedFeild{
          border-radius: 5px;
          background-color:${COLOR.GREEN};
        }
         .details-item:last-child{
          border:0;
        }
         `}
      </style>
    </>
  );
}
