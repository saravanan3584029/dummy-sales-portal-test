export const paymentArray = [
  {
    id: 1,
    label: "Advance Payment",
    value: "Advance Payment",
  },
  {
    id: 2,
    label: "30 Days",
    value: "30 Days",
  },
  {
    id: 3,
    label: "15 Days",
    value: "15 Days",
  },
];


export const keyToRemove = "contactPersons";