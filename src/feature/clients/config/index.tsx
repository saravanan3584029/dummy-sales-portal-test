export const formLinkConfig = (id: any) => {
  return {
    "Add Client": {
      id: 1,
      path: `/clients/create`,
      labelName: "Add Client",
      icon: {
        type: "plus",
        width: "16",
        height: "18",
        fill: "#1e40af",
      },
    },
    "My Clients": {
      id: 2,
      path: `/clients/${id}`,
      labelName: "My Client",
    },
    "All Clients": {
      id: 3,
      path: "/clients",
      labelName: "All Clients",
      icon: {
        type: "client",
        width: "16",
        height: "18",
        fill: "#1A68F3",
      },
    },
  };
};