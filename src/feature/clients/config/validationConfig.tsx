import validationMessages from "@/utils/validation/validationMessages";

export const clientValidations = {
  clientName: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "name",
      message: validationMessages.isText,
    },
  },
  name: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "name",
      message: validationMessages.isText,
    },
  },

  phone: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "mobile",
      message: validationMessages.isMobile,
    },
  },

  email: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "email",
      message: validationMessages.isEmail,
    },
  },
  location: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  paymentTerms: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
  ndaFile: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  accountManager: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
  designation: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
};
