import { COLOR } from "@/constants/theme";
import { budget, experience } from "@/feature/requirements/config/types";
import Icon from "@/lib/svg";
import { formatNumberToString, getTimeDifference } from "@/utils";

export const ClientCardConfig = () => {
  return {
    clientName: {
      val: "clientName",
      class: "w-11/12 text-xm font-semibold leading-5 col-span-5 mb-2",
      function: (data: any) => {
        const letter = data?.slice(0, 1)?.toUpperCase();
        return (
          <div className="flex items-center">
            <div
              className="w-10 h-10 rounded-full flex justify-center items-center text-white flex-shrink-0 text-md"
              style={{ backgroundColor: "#61B9A5" }}
            >
              {letter}
            </div>
            <div className="ml-4">{data}</div>
          </div>
        );
      },
    },
    location: {
      val: "location",
      class: "col-span-3 text-gray-500 mt-4 text-m",
      icon: {
        type: "location",
        fill: "#66666E",
      },
    },
    payment: {
      icon: { type: "dot", fill: "#AAAAAF" },
      val: "paymentTerms",
      class: "text-gray-500 mt-4 text-sm col-span-2",
      function: (data: any) => {
        return (
          <>
            <Icon type="payment" fill="gray" />
            {data}
          </>
        );
      },
    },
    contactPersons: {
      val: "contactPersons",
      class: "col-span-5 text-gray-500 mt-2 text-m",
      icon: {
        type: "phone",
        fill: "#66666E",
      },
      function: (array: any) => {
        const renderData = array.slice(0, 2);
        const result = renderData.map((entry: any, index: number) => {
          return `${entry.name}${renderData.length === index + 1 ? " " : ","} `;
        });
        array.length > 2 &&
          result.push(<div className="">+ {array.length - 2}</div>);
        return (
          <div className="flex items-center justify-start gap-x-1">
            {result}
          </div>
        );
      },
    },
    requirements: {
      val: "requirements",
      entry: "technology",
      class: "col-span-3 text-gray-500 mt-5",
      function: (data: any) => {
        const renderData = data.slice(0, 3);
        const remainingData = data.length - 3;
        const result = renderData.map((entry: any, index: number) => {
          return (
            <div
              key={index}
              className={`text-black flex items-center gap-2 text-center rounded-lg py-1 px-2`}
              style={{ backgroundColor: "#F4F4F4" }}
            >
              {entry["techStack"]}
              {/* {entry["isPrimary"] && (
                <Icon type="star" width="10" height="10" fill={COLOR.YELLOW} />
              )} */}
            </div>
          );
        });
        remainingData > 0 &&
          result.push(
            <div
              className="text-black flex rounded-lg p-1"
              style={{ backgroundColor: "#F4F4F4" }}
            >
              + {remainingData}
            </div>
          );
        return (
          <>
            <p className="mb-2">Requirements</p>
            <div className="flex items-center justify-start gap-x-1">
              {result.length > 0 ? result : "-"}
            </div>
          </>
        );
      },
    },
    accountManager: {
      val: "accountManager",
      class: "col-span-3 text-gray-500 mt-4",
      icon: {
        type: "user-circle",
        fill: "#AAAAAF",
      },
    },
    createdAt: {
      val: "createdAt",
      class: "text-sm text-gray-400 mt-4 col-span-2",
      icon: { type: "clock", width: "15", fill: "#AAAAAF" },
      function: (data: any) => {
        const formattedDate = getTimeDifference(data);
        return `${formattedDate} ago`;
      },
    },
  };
};

export const clientResourceConfig = () => {
  return {
    technology: {
      val: "technology",
      class: "col-span-5 mt-3",
      function: (array: any) => {
        const primary = array.filter((item: any) => item.isPrimary === true);
        const secondary = array.filter((item: any) => item.isPrimary === false);
        const originalArray = [...primary, ...secondary];
        const renderData = originalArray.slice(0, 3);
        const remainingData = originalArray.length - 3;
        const result = renderData.map((entry: any, index: number) => {
          return (
            <div key={index} className={"text-gray-900"}>
              {entry.techStack}
              {renderData.length === index + 1 ? " " : ","}
            </div>
          );
        });
        remainingData > 0 &&
          result.push(
            <div key="remaining" className="text-gray-400 ml-1">
              +{remainingData}
            </div>
          );

        return (
          <div className="flex gap-0.5 flex-wrap font-bold text-base leading-5">
            {result}
          </div>
        );
      },
    },
    status: {
      val: "status",
      class: " text-center mt-4 text-white text-sm col-span-1",
      function: (data: string) => {
        if (data === "Open") {
          return <div className="bg-green-600 rounded-xl">{data}</div>;
        } else if (data === "Completed") {
          return <div className="bg-blue-600 rounded-xl">{data}</div>;
        } else if (data === "On Hold") {
          return <div className="bg-yellow-600 rounded-xl">{data}</div>;
        } else {
          return <div className="bg-gray-600 rounded-xl">{data}</div>;
        }
      },
    },
    reqId: {
      val: "reqId",
      class: " mt-4 col-span-1 ",
      function: (id: String) => {
        return (
          <div className="text-blue-700 font-semibold bg-blue-50 rounded-lg text-sm text-center mr-3">
            #{id}
          </div>
        );
      },
    },
    location: {
      val: "location",
      class: "col-span-1 text-gray-500 mt-4 text-sm",
      icon: {
        type: "location",
        fill: "#66666E",
      },
    },
    workMode: {
      icon: { type: "dot", fill: "#AAAAAF" },
      val: "workMode",
      class: "text-gray-500 mt-4 text-sm col-span-1",
    },

    experience: {
      val: "experience",
      class: "text-gray-500 mt-4 text-sm col-span-1",
      icon: { type: "dot", fill: "#AAAAAF" },
      function: (experience: experience) => {
        return (
          <div className="flex">
            <Icon type="work-duration" fill={"#AAAAAF"} />
            {`${experience.minimum}-${experience.maximum} yrs`}
          </div>
        );
      },
    },
  };
};
