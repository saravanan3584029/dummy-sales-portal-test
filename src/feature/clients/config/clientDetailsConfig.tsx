import Icon from "@/lib/svg";

export const HeaderConfig = () => {
  return {
    clientName: {
      val: "clientName",
      class: " col-span-3 text-sm leading-9",
      function: (data: string) => {
        const firstLetter = data.slice(0, 1)?.toUpperCase();
        return (
          <div className="flex items-center gap-2">
            <span
              className="w-10 h-10 rounded-full flex justify-center items-center text-white text-lg"
              style={{ backgroundColor: "#CC7C6C" }}
            >
              {firstLetter}
            </span>
            <span className="w-11/12 text-black-800 text-lg font-semibold">
              {data}
            </span>
          </div>
        );
      },
    },
  };
};

export const tabsConfig = () => [
  {
    name: "Details",
  },
  {
    name: "Requirements",
  },
  {
    name: "Notes",
  },
];

export const detailConfig = () => {
  return {
    Location: {
      class: "my-2",
      val: "location",
      value: "col-span-2 ml-4",
    },
    "Payment Terms": {
      class: "my-2",
      val: "paymentTerms",
      value: "col-span-2 ml-4",
    },

    "Contact Person": {
      val: "contactPersons",
      value: "col-span-2",
      function: (array: any) => {
        const result = array.map((item: any, index: number) => {
          return (
            <div key={item._id} className="mb-4 ml-4">
              <div className="flex items-center gap-x-3">
                <div className="capitalize">{item.name}</div>{" "}
                <div className="text-gray-400">{item.designation}</div>
              </div>
              <div className="flex items-center gap-2">
                <Icon type="phone" fill="gray" />
                {item.phoneNumber}
              </div>
              <div className="flex items-center gap-2">
                {" "}
                <span className="text-gray-400">@</span> {item.email}
              </div>
            </div>
          );
        });
        return result;
      },
      line: true,
    },
    NDA: {
      val: "ndaFile",
      key: "col-span-3 mt-2",
      value: "col-span-3",
      function: (data: any, handleExpand: any, download: any) => {
        return (
          <div className="flex justify-between items-center border border-gray-300 px-2 py-2 rounded-md mt-3">
            <div className="flex items-center gap-2">
              <div
                className="flex items-center gap-2 p-2 rounded-md"
                style={{ backgroundColor: "#E8F0FE" }}
              >
                <Icon type="attachment" width="16" height="16" fill="#1A68F3" />
              </div>
              <div>{data.filename}</div>
            </div>
            <div className="flex items-center gap-5">
              <Icon
                type="expand"
                width="18"
                height="15"
                onClick={() => handleExpand(data._id)}
              />
              <Icon
                type="download"
                width="18"
                height="15"
                onClick={() => download(data._id)}
              />
            </div>
          </div>
        );
      },
    },
  };
};
