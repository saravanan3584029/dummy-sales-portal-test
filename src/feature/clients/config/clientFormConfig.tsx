// import { accountManager, states } from "@/feature/resources/constant";
import FormRender from "@/utils/form/formRender";
import { BUTTON } from "@/constants/theme";
import { uniqueStatesAndDistricts } from "@/feature/resources/constant";
import { paymentArray } from "../constant";

export const clientFormFormConfig = ( accountManager?: any) => {
 
  return {
    section1: {
      class: "mt-5",
      config: {
        Name: {
          elementType: "FormInput",
          placeholder: "Enter Client Name",
          disablePaste: false,
          name: "clientName",
        },
      },
    },

    section2: {
      id: 2,
      class: "mt-5",
      config: {
        Details: {
          className: "row-span-2",
        },
        Location: {
          elementType: "FormDropdown",
          disablePaste: true,
          placeholder: "Select",
          options: uniqueStatesAndDistricts,
          val:"label",
          height: "200",
          name: "location",
          icon: {
            type: "location",
          },
          searchBar: {
            placeholder: "Search here",
          },
        },
        "Payment Terms": {
          elementType: "FormDropdown",
          disablePaste: true,
          placeholder: "Select",
          options: paymentArray,
          val:"label",
          name: "paymentTerms",
        },

        NDA: {
          elementType: "FormUploadInput",
          placeholder: "Upload Attachment",
          name: "ndaFile",
          accept: "application/pdf,.doc,.docx",
        },
      },
    },
    section3: {
      id: 3,
      class: "mt-5",
      config: {
        "Contact person": {
          className: "row-span-2",
        },
        contact: {
          elementType: "NestedForm",
          name: "contactPersons",
          className: "col-span-2 mr-28",
          content: [
            {
              elementType: "FormInput",
              placeholder: "Name",
              key: "Name",
              disablePaste: false,
              name: "name",
              disabled: false,
              className: "col-span-2",
              wrapper:"nestedForm"
            },
            {
              elementType: "FormInput",
              key: "Phone Number",
              disablePaste: false,
              name: "phoneNumber",
              disabled: false,
              className: "col-span-2",
              wrapper:"nestedForm",
              icon: {
                label: "+91",
              },
            },
            {
              elementType: "FormInput",
              placeholder: "Email",
              key: "Email Address",
              disablePaste: false,
              name: "email",
              disabled: false,
              className: "col-span-2",
              wrapper:"nestedForm"
            },
            {
              elementType: "FormInput",
              placeholder: "Designation",
              disablePaste: false,
              key: "Designation",
              name: "designation",
              disabled: false,
              className: "col-span-2",
              wrapper:"nestedForm"
            },
            {
              elementType: "FormToggleSwitch",
              name: "isPrimary",
              label: "Primary contact",
              type: "checkbox",
              className: "col-span-4",
              wrapper:"nestedForm"
            },
          ],
          btnConfig: [
            {
              elementType: "FormButton",
              type: "button",
              goal: "ADD",
              variant: "primary",
              className: "",
              children: "Add",
            },
            {
              elementType: "FormButton",
              type: "button",
              goal: "REMOVE",
              variant: "disable",
              className: "",
              children: "cancel",
            },
          ],
        },
      },
    },

    section4: {
      id: 4,
      class: "mt-5",
      config: {
        "Account Manager ": {},
        "Account Manager": {
          elementType: "FormSelectMultipleItem",
          options: accountManager,
          disablePaste: false,
          name: "accountManager",
          val:"name",
          className: "col-span-2",
          width: "300",
          icon: {
            image: "/images/profile.png",
          },
        },
      },
    },
  };
};

export const nestedFormBtnConfig = (btnName: string) => [
  {
    id: 1,
    type: "button",
    size: "medium",
    children: btnName,
    variant: "primary",
    classname: "",
  },
  {
    id: 2,
    type: "reset",
    size: "medium",
    varient: "secondary",
    children: "Cancel",
    className: "border rounded-lg",
  },
];
