import { uniqueStatesAndDistricts } from "@/feature/resources/constant";
import { paymentArray } from "../constant";

const filterConfig = (accountManager:any,clientSuggestionArray:any) => {
    return {
      "Payment Terms": {
        val: "paymentTerms",
        options: paymentArray,
        optionValue: "value",
      },
      Location: {
        id: 6,
        val: "location",
        options: uniqueStatesAndDistricts,
        optionValue: "value",
        searchBar: {
          placeHolder: "Search Location",
        },
      },
      Requirements: {
        id: 5,
        val: "requirements",
        options: clientSuggestionArray,
        optionValue: "requirements",
        searchBar: {
          placeHolder: "Search Requirements",
        },
      },
      Manager: {
        val: "accountManager",
        id: 7,
        options: accountManager,
        optionValue: "name",
        searchBar: {
          placeHolder: "Search Manager",
        },
      },
    };
  };
  
  export default filterConfig;
  