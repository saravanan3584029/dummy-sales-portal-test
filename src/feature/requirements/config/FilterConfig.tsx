import {
  uniqueStatesAndDistricts,
  technologies,
  workMode,
} from "@/feature/resources/constant/index";
import { RequirementStatus, requirementPriority } from "../constant";

const filterConfiguration = (accountManager:any,clientSuggestionArray:any) => {
  return {
    Status: {
      val: "status",
      options: RequirementStatus,
      optionValue: "value",
    },
    Budget: {
      val: "budget",
      form: {
        "": {
          config: {
            " ": {
              type: "number",
              elementType: "FormInput",
              disablePaste: false,
              placeholder: "Min",
              name: "minimum",
              icon: {
                type: "rupee",
                width: "20",
                height: "18",
              },
            },
            "": {
              type: "number",
              elementType: "FormInput",
              disablePaste: false,
              placeholder: "Max",
              name: "maximum",
              icon: {
                type: "rupee",
                width: "20",
                height: "18",
              },
            },
          },
        },
      },
    },
    Experience: {
      id: 3,
      val: "experience",
      form: {
        Experience: {
          config: {
            "": {
              type: "number",
              elementType: "FormInput",
              placeholder: "Year Min",
              disablePaste: false,
              name: "minimum",
            },
            " ": {
              type: "number",
              elementType: "FormInput",
              placeholder: "Year Max",
              disablePaste: false,
              name: "maximum",
            },
          },
        },
      },
    },
    Technology: {
      id: 4,
      val: "technology",
      options: technologies,
      optionValue: "value",
      searchBar: {
        placeHolder: "Search technology",
      },
    },
    Client: {
      id: 5,
      val: "clientName",
      options: clientSuggestionArray,
      optionValue: "clientName",
      searchBar: {
        placeHolder: "Search Client",
      },
    },
    Location: {
      id: 6,
      val: "location",
      options: uniqueStatesAndDistricts,
      optionValue: "value",
      searchBar: {
        placeHolder: "Search Location",
      },
    },
    Manager: {
      val: "accountManager",
      id: 7,
      options: accountManager,
      optionValue: "name",
      searchBar: {
        placeHolder: "Search Manager",
      },
    },
    " Work Type": {
      val: "workMode",
      id: 8,
      options: workMode,
      optionValue: "value",
    },
    Priority: {
      val: "priority",
      id: 9,
      options: requirementPriority,
      optionValue: "value",
    },
  };
};

export default filterConfiguration;

export const filterBtnConfig = () => [
  {
    id: 1,
    type: "button",
    size: "small",
    children: "ApplyFilter",
    variant: "primary",
    className: "ml-3 my-3",
  },
  {
    id: 2,
    type: "reset",
    size: "small",
    varient: "secondary",
    children: "Reset",
    className: "border rounded-lg mr-3 my-3",
  },
];

export const filterFormBtnConfig = () => [
  {
    id: 1,
    type: "button",
    size: "small",
    children: "Apply",
    variant: "primary",
    className: "ml-3 my-3",
  },
  {
    id: 2,
    type: "reset",
    size: "small",
    varient: "secondary",
    children: "Reset",
    className: "border rounded-lg mr-3 my-3",
  },
];
