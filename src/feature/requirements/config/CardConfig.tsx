import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import { formatNumberToString, getTimeDifference } from "@/utils";
import { budget, experience } from "./types";

export const RequirementConfiguration: any = {
  clientName: {
    val: "clientName",
    class: "w-11/12 text-gray-500 col-span-5 text-sm leading-4",
    entry:"clientName",
    icon: {
      type: "client",
    },
  },
  technology: {
    val: "technology",
    class: "col-span-5 mt-3",
    function: (array: any) => {
      const primary = array.filter((item: any) => item.isPrimary === true);
      const secondary = array.filter((item: any) => item.isPrimary === false);
      const originalArray = [...primary, ...secondary];
      const renderData = originalArray.slice(0, 3);
      const remainingData = originalArray.length - 3;
      const result = renderData.map((entry: any, index: number) => {
        return (
          <div key={index} className={"text-gray-900"}>
            {entry.techStack}
            {renderData.length === index + 1 ? " " : ","}
          </div>
        );
      });
      remainingData > 0 &&
        result.push(
          <div key="remaining" className="text-gray-400 ml-1">
            +{remainingData}
          </div>
        );

      return (
        <div className="flex gap-0.5 flex-wrap font-bold text-base leading-5">
          {result}
        </div>
      );
    },
  },
  reqId: {
    val: "reqId",
    class: " mt-4 col-span-2 ",
    function: (id: String) => {
      return (
        <div className="text-blue-700 font-semibold bg-blue-50 rounded-lg text-m text-center mr-3">
          #{id}
        </div>
      );
    },
  },

  priority: {
    val: "priority",
    class: "col-span-3 mt-3 text-m font-medium",
    function: (data: string) => {
      if (data === "High") {
        return (
          <div className="flex items-center gap-2 text-red-600 rounded-xl px-1 py-1 mr-8">
            <Icon type="High Priority" width="16" height="16" />
            {data}
          </div>
        );
      } else if (data === "Medium") {
        return (
          <div className="flex items-center gap-2 text-yellow-600 rounded-xl px-1 py-1 mr-8">
            <Icon type="Med Priority" width="16" height="16" />
            {data}
          </div>
        );
      } else if (data === "Low") {
        return (
          <div className="flex items-center gap-2 text-gray-600 rounded-xl px-1 py-1 mr-8">
            <Icon type="Low Priority" width="16" height="16" />
            {data}
          </div>
        );
      } else {
        return (
          <div className="flex items-center gap-2 text-gray-600 rounded-xl px-1 py-1 mr-5">
            <Icon type="dot large" width="16" height="16" /> {data}
          </div>
        );
      }
    },
  },
  status: {
    val: "status",
    class: " text-center mt-6 text-white text-base leading-4 col-span-2",
    function: (data: string) => {
      if (data === "Open") {
        return (
          <div className="bg-green-600 rounded-xl px-1 py-1 mr-8">{data}</div>
        );
      } else if (data === "Completed") {
        return (
          <div className="bg-blue-600 rounded-xl px-1 py-1 mr-5">{data}</div>
        );
      } else if (data === "On Hold") {
        return (
          <div className="bg-yellow-600 rounded-xl px-1 py-1 mr-5">{data}</div>
        );
      } else {
        return (
          <div className="bg-gray-600 rounded-xl px-1 py-1 mr-5">{data}</div>
        );
      }
    },
  },

  openPositions: {
    val: "openPositions",
    class: "col-span-3 mt-7 text-m",
    icon: {
      type: "dot",
      fill: "#AAAAAF",
    },
    function: (number: number) => {
      return (
        <div className="text-gray-500 font-normal">{number} positions</div>
      );
    },
  },

  location: {
    val: "location",
    class: "col-span-3 text-gray-500 mt-4 text-m",
    icon: {
      type: "location",
      fill: "#66666E",
    },
  },
  workMode: {
    icon: { type: "dot", fill: "#AAAAAF" },
    val: "workMode",
    class: "text-gray-500 mt-4 text-m col-span-2",
  },

  experience: {
    val: "experience",
    class: "text-gray-500 mt-3 text-m col-span-2",
    icon: {
      type: "work-duration",
      fill: "#AAAAAF",
    },
    function: (experience: experience) => {
      return `${experience.minimum}-${experience.maximum} yrs`;
    },
  },
  contractDuration: {
    val: "contractDuration",
    class: "text-gray-500 mt-3 text-m ml-2",
    icon: {
      type: "dot",
      fill: "#AAAAAF",
    },
  },

  budget: {
    val: "budget",
    class: "text-gray-500 mt-3 ml-2 text-m ",
    icon: {
      type: "dot",
      fill: "#AAAAAF",
    },
    function: (budget: budget) => {
      const { maximum, minimum } = budget;
      const min = formatNumberToString(minimum);
      const max = formatNumberToString(maximum);
      return (
        <div className="flex items-center">
          <Icon type="rupee" fill="#66666E" width="15" height="15" />
          {min} - {max}
        </div>
      );
    },
  },

  accountManager: {
    val: "accountManager",
    class: "col-span-3 text-gray-500 mt-7",
    icon: {
      type: "user-circle",
      fill: "#AAAAAF",
    },
  },
  createdAt: {
    val: "createdAt",
    class: "text-sm text-gray-400 mt-7 col-span-2",
    icon: { type: "clock", width: "15", fill: "#AAAAAF" },
    function: (data: any) => {
      const formattedDate = getTimeDifference(data);
      return `${formattedDate} ago`;
    },
  },
};
