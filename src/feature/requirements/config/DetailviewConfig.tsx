import Icon from "@/lib/svg";
import { formatNumberToCurrency, formatNumberToString } from "@/utils";
import { budget, experience } from "./types";

export const HeaderConfig = () => {
  return {
    technology: {
      val: "technology",
      class: " col-span-3",
      function: (array: any) => {
        const primary = array.filter((item: any) => item.isPrimary === true);
        const secondary = array.filter((item: any) => item.isPrimary === false);
        const originalArray = [...primary, ...secondary];
        const result = originalArray.map((item: any, index: number) => {
          return (
            <div key={index} className="capitalize font-semibold text-lg">
              {item.techStack} {originalArray.length === index + 1 ? " " : ","}
            </div>
          );
        });
        return (
          <div className="flex items-center underline  flex-wrap ">
            {result}
          </div>
        );
      },
    },
    clientName: {
      val: "clientName",
      class: " col-span-3 text-sm leading-9",
      function: (data: string) => {
        const firstLetter = data.slice(0, 1);
        return (
          <div className="flex items-center gap-2 mt-3">
            <span className="px-3  border bg-gray-600 text-white">
              {firstLetter}
            </span>
            <span className="underline">{data}</span>
          </div>
        );
      },
    },
  };
};

export const tabsConfig = () => [
  {
    name: "Details",
  },
  {
    name: "Resources Aligned",
  },
  {
    name: "Notes",
  },
];

export const detailConfig = () => {
  return {
    "Job Description": {
      class: "my-2",
      val: "description",
      key: "col-span-3",
      value: "col-span-3 ml-2",
      function: (content: string, {}, {}, setView: any, view: boolean) => {
        const truncatedContent = content.split("\n").slice(0, 2).join("\n");
        if (content.length > truncatedContent.length) {
          return !view ? (
            <div>
              {truncatedContent}
              <br />
              <span
                className="underline font-medium"
                onClick={() => setView(!view)}
              >
                View More
              </span>
            </div>
          ) : (
            <div>
              {content}
              <br />
              <span
                className="underline font-medium"
                onClick={() => setView(!view)}
              >
                View Less
              </span>
            </div>
          );
        } else {
          return <div>{content}</div>;
        }
      },
    },
    "": {
      class: "my-3",
      val: "attachment",
      value: "col-span-3",
      line: true,
      function: (data: any, handleExpand: any, download: any) => {
        return (
          <>
            {data ? (
              <div className="flex justify-between items-center">
                <div className="flex items-center gap-2">
                  <Icon type="attachment" width="20" height="20" />
                  {data.filename}
                </div>
                <div className="flex items-center gap-5">
                  <Icon
                    type="expand"
                    width="18"
                    height="15"
                    onClick={() => handleExpand(data._id)}
                  />
                  <Icon
                    type="download"
                    width="18"
                    height="15"
                    onClick={() => download(data._id)}
                  />
                </div>
              </div>
            ) : null}
          </>
        );
      },
    },

    Client: {
      class: "my-2",
      val: "clientName",
      value: "col-span-2",
    },
    Location: {
      class: "my-2",
      val: "location",
      value: "col-span-2",
    },
    Experience: {
      class: "my-2",
      val: "experience",
      value: "col-span-2",
      function: (experience: experience) => {
        return `${experience.minimum} - ${experience.maximum} yrs`;
      },
    },
    Budget: {
      val: "budget",
      class: "my-2",
      value: "col-span-2",
      function: (budget: budget) => {
        const { maximum, minimum } = budget;
        const min = formatNumberToString(minimum);
        const max = formatNumberToString(maximum);
        return `${min} - ${max}`;
      },
    },

    Type: {
      val: "workMode",
      class: "my-2",
      value: "col-span-2",
      line: true,
    },

    "Technology Required": {
      val: "technology",
      key: "col-span-3",
      value: "col-span-3",
      function: (data: any) => {
        const result = data?.map(
          (item: { techStack: string }, index: number) => {
            return (
              <div
                key={index}
                className="rounded-lg  bg-gray-300 text-center p-2"
              >
                {item.techStack}
              </div>
            );
          }
        );
        return <div className="flex flex-wrap gap-2 py-2">{result}</div>;
      },
    },

    "Contact Person": {
      val: "contactPerson",
      key: "col-span-3",
      value: "col-span-2",
      line: true,
    },

    "Account Manager": {
      val: "accountManager",
      key: "col-span-3",
      value: "col-span-3",
      line: true,
      function: (data: any) => {
        const result = data.map((item: string, index: number) => {
          return (
            <div
              key={index}
              className="flex items-center gap-2 rounded-lg border text-center p-2 bg-gray-400"
            >
              <Icon type="user-circle" />
              {item}
            </div>
          );
        });
        return <div className="flex items-center gap-2">{result}</div>;
      },
    },
    "Version History": {
      line: true,
    },
  };
};
