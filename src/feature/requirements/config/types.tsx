type Tech = {
  techStack: string;
  isPrimary: boolean;
};

export type budget = {
  minimum: number;
  maximum: number;
};
export type experience = {
  minimum: number;
  maximum: number;
};

type attachment = {
  _id: string;
  filename: string;
  fileReference: string;
  createdAt: Date;
  updatedAt: Date;
};
export type RequirementData = {
  _id?: string;
  clientName: string;
  technology: Tech[];
  description: string;
  location: string;
  budget: budget;
  experience: experience;
  contractDuration: string;
  openPositions: "";
  contactPerson: "";
  accountManager: string[];
  attachment: attachment;
};
