import { COLOR } from "@/constants/theme";
import { uniqueStatesAndDistricts, technologies, workMode } from "@/feature/resources/constant";
import {
  RequirementStatus,
  contractDuration,
  requirementPriority,
} from "../constant";

export const formLinkConfig = (id: any) => {
  return {
    "Add Requirement": {
      id: 1,
      path: `/requirements/create`,
      labelName: "Add Requirement",
      icon: {
        type: "plus",
        width: "16",
        height: "18",
        fill: "#1e40af",
      },
    },
    "My Requirement": {
      id: 2,
      path: `/requirements/${id}`,
      labelName: "My Requirement",
    },
    "All Clients": {
      id: 3,
      path: "/clients",
      labelName: "All Clients",
      icon: {
        type: "client",
        width: "16",
        height: "18",
        fill: "#1A68F3",
      },
    },
  };
};

export const requirementFormConfig = (
  accountManager: any,
  client: any,
  callback: any,
  contactPerson: any,
  id?: string
) => {
  return {
    section1: {
      class: "mt-5",
      config: {
        "Select Client": {},
        Client: {
          elementType: "FormDropdown",
          val: "clientName",
          placeholder: "Select",
          options: client,
          name: "clientName",
          disablePaste: true,
          callback: callback,
          height: "200",
          icon: {
            type: "client",
            fill: "blue",
          },
          searchBar: {
            placeholder: "Search here",
          },
        },
      },
    },

    section2: {
      id: 2,
      class: "mt-5",
      config: {
        "Skills Required": {},
        Technology: {
          elementType: "FormSelectMultipleItem",
          disablePaste: false,
          options: technologies,
          name: "technology",
          width: "300",
          className: "col-span-2",
          height: "200",
          val: "label",
          icon: {
            type: "star-outline",
            width: "15",
            height: "18",
          },
          searchBar: {
            placeholder: "Search here",
          },
        },
      },
    },

    section3: {
      id: 3,
      class: "mt-5",
      config: {
        "Job Description ": {
          className: "row-span-2",
        },
        Description: {
          elementType: "FormTextarea",
          name: "description",
          className: "col-span-2",
          placeholder: "Type a job description",
          class: "border rounded-lg h-48",
          row: 8,
        },
        "": {
          elementType: "FormUploadInput",
          placeholder: "Add Attachment",
          name: "attachment",
          accept: ".pdf,.doc,.docx,.png,.jpg,.jpeg",
          click: true,
          icon: {
            type: "attachment",
            width: "15",
            height: "18",
            fill: "blue",
          },
        },
      },
    },

    section4: {
      id: 4,
      class: "mt-5",
      config: {
        "Additional Details": {
          className: "row-span-4",
        },
        Location: {
          elementType: "FormDropdown",
          disablePaste: true,
          placeholder: "Select",
          options: uniqueStatesAndDistricts,
          val: "label",
          height: "200",
          name: "location",
          icon: {
            type: "location",
          },
          searchBar: {
            placeholder: "Search here",
          },
        },
        "Work mode": {
          elementType: "FormDropdown",
          disablePaste: true,
          placeholder: "Select",
          options: workMode,
          val: "label",
          name: "workMode",
        },
        Budget: {
          Feild: {
            "": {
              type: "number",
              elementType: "FormInput",
              disablePaste: false,
              name: "budgetMin",
              icon: {
                type: "rupee",
                width: "20",
                height: "18",
                fill: "#AAAAAF",
              },
            },
            "-": {},
            " ": {
              type: "number",
              elementType: "FormInput",
              disablePaste: false,
              name: "budgetMax",
              icon: {
                type: "rupee",
                width: "20",
                height: "18",
                fill: "#AAAAAF",
              },
            },
          },
        },
        Experience: {
          Feild: {
            "": {
              type: "number",
              elementType: "FormInput",
              disablePaste: false,
              name: "experienceMin",
              icon: { label: "Min" },
            },
            "-": {},
            " ": {
              type: "number",
              elementType: "FormInput",
              disablePaste: false,
              name: "experienceMax",
              icon: { label: "Max" },
            },
          },
        },
        "Contract Duration": {
          elementType: "FormDropdown",
          disablePaste: true,
          placeholder: "Select",
          options: contractDuration,
          name: "contractDuration",
          val: "label",
        },

        "Open Positions": {
          type: "number",
          elementType: "FormInput",
          disablePaste: false,
          name: "openPositions",
          width: "50%",
        },
      },
    },
    section5: {
      id: 5,
      class: "mt-5",
      config: {
        "Contact Person": {},
        Select: {
          elementType: "FormSelectMultipleItem",
          placeholder: "Select",
          disablePaste: false,
          inputStyle: true,
          val: "name",
          options: contactPerson,
          name: "contactPerson",
        },
      },
    },
    section6: {
      id: 6,
      class: "mt-5",
      config: {
        "Account Manager ": {},
        "Account Manager": {
          elementType: "FormSelectMultipleItem",
          options: accountManager,
          disablePaste: false,
          name: "accountManager",
          className: "col-span-2",
          width: "300",
          val: "name",
          icon: {
            image: "/images/profile.png",
          },
        },
      },
    },
  };
};

export const requirementBtnConfig = (btnName: string, loading?: boolean) => [
  {
    id: 1,
    type: "button",
    size: "medium",
    children: btnName,
    variant: "primary",
    disabled: loading,
    loading: loading,
  },
  {
    id: 2,
    type: "reset",
    variant: "secondary",
    size: "medium",
    children: "Cancel",
    className: "border rounded-lg",
  },
];
