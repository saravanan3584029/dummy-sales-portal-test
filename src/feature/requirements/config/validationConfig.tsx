import validationMessages from "@/utils/validation/validationMessages";

export const requirementValidations = {
  clientName: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
  technology: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "isPrimary",
      message: validationMessages.isNotPrimary,
    },
  },
  description: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
  location: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
  experienceMin: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "positiveNonzeroFloat",
      message: validationMessages.isNonzeroNumber,
    },
  },
  experienceMax: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "positiveNonzeroFloat",
      message: validationMessages.isNonzeroNumber,
    },
  },
  budgetMin: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "positiveNonzeroFloat",
      message: validationMessages.isNonzeroNumber,
    },
  },
  budgetMax: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "positiveNonzeroFloat",
      message: validationMessages.isNonzeroNumber,
    },
  },
  openPositions: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "positiveNonzeroFloat",
      message: validationMessages.isNonzeroNumber,
    },
  },

  workMode: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
  contractDuration: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
  contactPerson: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
  accountManager: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
};
