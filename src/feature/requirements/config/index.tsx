export const updateConfig = (btn?:string,handleUpdate?: (_: string) => void) => [
  {
    id: 1,
    type: "reset",
    variant: "secondary",
    size: "medium",
    children: "Cancel",
    className: "w-32 border rounded-lg",
  },
  {
    id: 2,
    type: "button",
    size: "medium",
    children: btn,
    variant: "tertiary",
    className: "w-32",
    onClick: handleUpdate,
  },
];
