export const contractDuration = [
  {
    id: 1,
    label: "3-6 Months ",
    value: "3-6 Months",
  },
  {
    id: 2,
    label: "6 Months ",
    value: "6 Months",
  },
  {
    id: 2,
    label: "1 Year ",
    value: "1 Year",
  },
];

export const RequirementStatus = [
  {
    id: 1,
    label: "Open",
    value: "Open",
    className: `#0E9255`,
    icon: {
      type: "dot large",
      fill: "#0E9255",
    },
  },
  {
    id: 2,
    label: "On Hold",
    value: "On Hold",
    className: `#CA8D1B`,
    icon: {
      type: "dot large",
      fill: "#CA8D1B",
    },
  },
  {
    id: 3,
    label: "Completed",
    value: "Completed",
    className: `#1553C2`,
    icon: {
      type: "dot large",
      fill: "#1553C2",
    },
  },
  {
    id: 4,
    label: "Closed",
    value: "Closed",
    className: `#4F4F55`,
    icon: {
      type: "dot large",
      fill: "#4F4F55",
    },
  },
];

export const requirementPriority = [
  {
    id: 1,
    label: "High",
    value: "High",
    className: `#C0362D`,
    icon: {
      type: "High Priority",
      fill: "#C0362D",
    },
  },
  {
    id: 2,
    label: "Medium",
    value: "Medium",
    className: `#CA8D1B`,
    icon: {
      type: "Med Priority",
      fill: "#CA8D1B",
    },
  },
  {
    id: 3,
    label: "Low",
    value: "Low",
    className: `#4F4F55`,
    icon: {
      type: "Low Priority",
      fill: "#4F4F55",
    },
  },
  {
    id: 4,
    label: "No Priority",
    value: "No Priority",
    className: `#4F4F55`,
    icon: {
      type: "dot large",
      fill: "#4F4F55",
    },
  },
];
