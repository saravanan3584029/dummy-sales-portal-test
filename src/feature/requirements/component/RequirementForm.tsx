/* eslint-disable react/no-children-prop */

"use client";
import React, { Fragment, useCallback, useEffect, useState } from "react";
import FormRender from "@/utils/form/formRender";
import {
  formLinkConfig,
  requirementBtnConfig,
  requirementFormConfig,
} from "../config/FormConfig";
import Icon from "@/lib/svg";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/navigation";
import Notes from "@/shared-components/components/Note";
import { hideLoader, showLoader, showToaster } from "@/store/common";
import { requirementValidations } from "../config/validationConfig";
import { areEqual, getItemFromLocalStorage } from "@/utils";
import { setClient, setContactPerson, setUserData } from "@/store/user/index";

const RequirementForm = ({
  className,
  columnNum = 3,
  id,
  creatingNewEntry = false,
  filterDepartment = false,
}: {
  className?: string;
  columnNum?: number;
  id?: string;
  creatingNewEntry?: boolean;
  filterDepartment?: boolean;
}) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [updateData, setUpdateData] = useState<any>();

  const [currentUserId, setCurrentUserId] = useState<any>();
  const [currentUserName, setCurrentUserName] = useState<any>();

  const userStore = useSelector((store: any) => store.user);
  const { user, client, contactPerson } = userStore;

  useEffect(() => {
    setCurrentUserId(getItemFromLocalStorage("user_id"));
    setCurrentUserName(getItemFromLocalStorage("user_name"));
    
    const accountManager = async () => {
      const response = await fetch(`/api/user/`);
      if (response.ok) {
        const data = await response.json();
        dispatch<any>(setUserData(data.response));
      }
      const clientResponse = await fetch(`/api/clients/`);
      if (response.ok) {
        const responseData = await clientResponse.json();
        dispatch<any>(setClient(responseData.response));
      }
    };
    accountManager();
  }, []);

  const contactPersonData = (data: any) => {
    const person = client.find(
      (item: any) => item.clientName === data.value
    )?.contactPersons;
    dispatch(setContactPerson(person));
  };
  const formData = requirementFormConfig(
    user,
    client,
    contactPersonData,
    contactPerson,
    id
  );

  const storeData = useSelector((store: any) => store.requirement.requirement);

  const fetchSingleUser = useCallback(async () => {
    try {
      const response = await fetch(`/api/requirements/` + id);
      if (response.ok) {
        const data = await response.json();
        setUpdateData(data.response);
      }
    } catch (error) {
      console.error("Fetch Error:", error);
    }
  }, [id]);

  useEffect(() => {
    const fetchData = async () => {
      if (id && storeData.length === 0) {
        await fetchSingleUser();
      } else {
        const data = storeData?.find((item: any) => item._id === id);
        setUpdateData(data);
      }
    };
    fetchData();
  }, [id, storeData, fetchSingleUser]);

  const createRequirementHandler = async (data: any) => {
    const {
      attachment,
      budgetMin,
      budgetMax,
      experienceMin,
      experienceMax,
      ...reminingData
    } = data;
    const budget = {
      minimum: budgetMin,
      maximum: budgetMax,
    };
    const experience = {
      minimum: experienceMin,
      maximum: experienceMax,
    };
    const actualData = { ...reminingData, budget, experience };
    dispatch<any>(showLoader());
    const formData = new FormData();
    if (attachment) {
      formData.append("file", attachment);
    }
    formData.append("jsonData", JSON.stringify(actualData));
    try {
      const response = await fetch("/api/requirements", {
        method: "POST",
        body: formData,
      });
      dispatch<any>(hideLoader());
      if (response.ok) {
        const data = await response.json();
        if (data.success) {
          dispatch<any>(
            showToaster({
              description: data.message,
              timer: 1000,
            })
          );
          router.push("/requirements");
        } else {
          dispatch<any>(
            showToaster({
              description:
                data.message ||
                (Object.values(data.error.errors) as Error[])[0]?.message,
              timer: 3000,
              variant: "red",
            })
          );
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const updateRequirementHandler = async (data: any) => {
    const {
      budgetMin,
      budgetMax,
      experienceMin,
      experienceMax,
      ...reminingData
    } = data;
    const budget = {
      minimum: budgetMin,
      maximum: budgetMax,
    };
    const experience = {
      minimum: experienceMin,
      maximum: experienceMax,
    };
    const actualData = { ...reminingData, budget, experience };
    let updateObject: any = {};
    Object.keys(actualData).map((ele: any) => {
      if (
        ele === "attachment" &&
        actualData[ele] !== updateData[ele]?.filename
      ) {
        updateObject = { ...updateObject, [ele]: actualData[ele] };
      } else if (Array.isArray(actualData[ele])) {
        if (actualData[ele].length === updateData[ele].length) {
          const hasDifference = actualData[ele].some(
            (item: any, index: number) => {
              if (typeof item === "string") {
                return item !== updateData[ele][index];
              } else {
                return (
                  item.techStack !== updateData[ele][index].techStack ||
                  item.isPrimary !== updateData[ele][index].isPrimary
                );
              }
            }
          );

          if (hasDifference) {
            updateObject = { ...updateObject, [ele]: actualData[ele] };
          }
        } else {
          updateObject = { ...updateObject, [ele]: actualData[ele] };
        }
      } else if (typeof actualData[ele] === "object") {
        if (
          JSON.stringify(actualData[ele]) !== JSON.stringify(updateData[ele])
        ) {
          updateObject = { ...updateObject, [ele]: actualData[ele] };
        }
      } else if (actualData[ele] !== updateData[ele]) {
        updateObject = { ...updateObject, [ele]: actualData[ele] };
      }
    });

    try {
      if (Object.keys(updateObject).length > 0) {
        dispatch(showLoader());
        const form = new FormData();
        const { attachment, ...entry } = updateObject;
        const dataToUpdate = { ...entry, _id: id };
        attachment && form.append("file", attachment);
        form.append("jsonData", JSON.stringify(dataToUpdate));
        const response = await fetch("/api/requirements", {
          method: "PUT",
          body: form,
        });
        dispatch(hideLoader());
        if (response.ok) {
          const data = await response.json();
          if (data.success) {
            dispatch<any>(
              showToaster({
                description: data.message,
                timer: 1000,
              })
            );
            router.push("/requirements");
          } else {
            dispatch<any>(
              showToaster({
                description:
                  data.message ||
                  (Object.values(data.error.errors) as Error[])[0]?.message,
                timer: 3000,
                variant: "red",
              })
            );
          }
        }
      } else {
        dispatch<any>(
          showToaster({
            description: "No entry has been updated in the form.",
            timer: 2000,
            variant: "green",
          })
        );
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleNotesComment = async (data: any, replyId: any) => {
    if (id) {
      if (data) {
        const postComment = { ...data, postedBy: currentUserName };
        try {
          dispatch(showLoader());
          const response = await fetch("/api/requirements/note", {
            method: "POST",
            body: JSON.stringify({
              postComment,
              _id: replyId ? replyId.id : id,
            }),
          });
          if (response.ok) {
            const data = await response.json();
            await fetchSingleUser();
            dispatch(hideLoader());
            if (data.success) {
              dispatch(
                showToaster({
                  timer: 2000,
                  description: data.message,
                })
              );
            }
          }
          dispatch(hideLoader());
        } catch (error) {
          console.log(error);
        }
      } else {
        dispatch(
          showToaster({
            timer: 2000,
            description: "Comment cannot be empty add atleast any comment",
          })
        );
      }
    } else {
      dispatch(
        showToaster({
          timer: 2000,
          description:
            "Prior to vendor creation, commenting functionality shall be restricted.",
        })
      );
    }
  };

  return (
    <Fragment>
      <div className="flex flex-row mr-12 gap-2 items-center mt-4 text-gray-500">
        <Icon type="back" />
        <div className="flex flex-row justify-between items-center text-m ">
          <span>Dashboard</span>
          <Icon type="chev_right" />
          <span>Requirement</span>
          <Icon type="chev_right" />
          <span>All Clients</span>
        </div>
      </div>

      <div className="mt-3">
        <FormRender
          formHeading={id ? "Update Requirement" : "Create Requirement"}
          formColumn={columnNum}
          formLink={formLinkConfig(currentUserId)}
          formLinkStyle={`text-blue-500 font-bold`}
          formLabelStyle=" mb-2 "
          formStyle="mx-5"
          formLine={true}
          formData={formData}
          initialStaticData={
            id
              ? {
                  clientName: updateData?.clientName,
                  technology: updateData?.technology || [],
                  description: updateData?.description,
                  attachment: updateData?.attachment?.filename,
                  location: updateData?.location,
                  workMode: updateData?.workMode,
                  budgetMin: updateData?.budget?.minimum,
                  budgetMax: updateData?.budget?.maximum,
                  experienceMin: updateData?.experience?.minimum,
                  experienceMax: updateData?.experience?.maximum,
                  contractDuration: updateData?.contractDuration,
                  openPositions: updateData?.openPositions,
                  contactPerson: updateData?.contactPerson,
                  accountManager: updateData?.accountManager || [],
                }
              : {
                  clientName: "",
                  technology: [],
                  description: "",
                  attachment: "",
                  location: "",
                  workMode: "",
                  budgetMin: "",
                  budgetMax: "",
                  experienceMin: "",
                  experienceMax: "",
                  contractDuration: "",
                  openPositions: "",
                  contactPerson: "",
                  accountManager: [],
                }
          }
          validations={requirementValidations}
          submitForm={id ? updateRequirementHandler : createRequirementHandler}
          btnConfig={requirementBtnConfig(id ? "Update" : "Create")}
        />
        <Notes handleComment={handleNotesComment} notesData={updateData} />
      </div>
    </Fragment>
  );
};

export default RequirementForm;
