"use client";
import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import Card from "@/shared-components/components/Card";
import Link from "next/link";
import React, { ChangeEvent, Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import SearchBar from "@/shared-components/components/SearchBar";
import DetailView from "../../../shared-components/components/DetailView";
import Filter from "@/shared-components/components/Filter";
import {
  hideLoader,
  hidePopup,
  showLoader,
  showPopup,
  showToaster,
} from "@/store/common";
import { getRequirementData } from "@/store/requirement";
import { RequirementConfiguration } from "../config/CardConfig";
import {
  HeaderConfig,
  tabsConfig,
  detailConfig,
} from "../config/DetailviewConfig";
import { updateConfig } from "../config";
import { RequirementData } from "../config/types";
import {
  applyFiltersToData,
  areEqual,
  base64ToBlob,
  downloadFiles,
  getItemFromLocalStorage,
} from "@/utils";
import { FilterTypes } from "@/feature/resources/config/types";
import { filterBtnConfig } from "@/feature/resources/config/filterConfig";
import filterConfiguration from "../config/FilterConfig";
import { RequirementStatus, requirementPriority } from "../constant";
import { setClient, setUserData } from "@/store/user";

export default function RequirementDashboard({ id }: { id?: string }) {
  const dispatch = useDispatch();
  const userStore = useSelector((store: any) => store.user);
  const { user, client } = userStore;

  const filterConfiData = filterConfiguration(user, client);
  const [filteredData, setFilteredData] = useState<RequirementData[]>([]);
  const [popup, setPopup] = useState<any>({});
  // select button
  const [showCheckBox, setShowCheckBox] = useState<boolean>(false);
  const [checkedData, setCheckedData] = useState<RequirementData[]>([]);
  // deatail view
  const [showDetailView, setShowDetailView] = useState<any>();
  const [selectedItem, setSelectedItem] = useState<RequirementData>();
  //filter
  const [isFilterOpen, setIsFilterOpen] = useState<Boolean>(false);
  const [filters, setFilters] = useState<any>([]);

  const [updateStatusData, setUpdateStatusData] = useState<boolean>(false);
  const [updatePriorityData, setUpdatePriorityData] = useState<boolean>(false);

  const [currentUserId, setCurrentUserId] = useState<any>();
  const [currentUserName, setCurrentUserName] = useState<any>();
  const [currentUserRole, setCurrentUserRole] = useState<any>();

  useEffect(() => {
    setCurrentUserId(getItemFromLocalStorage("user_id"));
    setCurrentUserName(getItemFromLocalStorage("user_name"));
    setCurrentUserRole(getItemFromLocalStorage("user_role"));
    const fetchData = async () => {
      if (user.length === 0) {
        const response = await fetch(`/api/user/`);
        if (response.ok) {
          const data = await response.json();
          dispatch<any>(setUserData(data.response));
        }
      }
      if (client.length === 0) {
        const response = await fetch(`/api/clients/`);
        if (response.ok) {
          const responseData = await response.json();
          dispatch<any>(setClient(responseData.response));
        }
      }
    };
    fetchData();
  }, []);

  const func = async () => {
    dispatch<any>(showLoader());
    const response = await fetch(
      `/api/requirements${id ? `/myrequirement/${id}` : ""}`,
      {
        method: "GET",
      }
    );

    if (response.ok) {
      const responseData = await response.json();
      dispatch(getRequirementData(responseData));
    }
    dispatch<any>(hideLoader());
  };
  useEffect(() => {
    func();
  }, []);

  const storeData = useSelector((store: any) => store.requirement.requirement);

  // search filter handler
  const handleSearchFilter = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const filterData = storeData?.filter(
      (user: any) =>
        user.clientName?.toLowerCase()?.indexOf(value.toLowerCase()) > -1
    );
    setFilteredData(filterData);
  };

  const showCardPopup = (item: RequirementData) => {
    setPopup(item);
  };
  const handleSelectButton = () => {
    setShowCheckBox(!showCheckBox);
    setCheckedData([]);
  };

  const showDeatailView = (item: RequirementData) => {
    setSelectedItem(item);
    setShowDetailView(true);
  };

  const handleFilterReset = () => {
    setFilters([]);
    setFilteredData([]);
  };

  // handle a apply filter button
  const handleApplyFilter = () => {
    const filterdData = applyFiltersToData(storeData, filters);
    const uniqueFilteredData = filterdData?.filter((data: any) => {
      return !filteredData?.some((prevData: any) => areEqual(data, prevData));
    });
    setFilteredData([...(filteredData || []), ...(uniqueFilteredData || [])]);
    setIsFilterOpen(false);
  };

  //handle a selected filter
  const handleSelectedFilter = (key: string, value: any) => {
    if (typeof value === "object") {
      setFilters([...filters, { key: key, value: { ...value } }]);
    } else {
      const existingFilterIndex = filters.findIndex(
        (filter: { value: string }) => filter.value === value
      );
      setFilteredData([]);
      setFilters((prevFilters: any) => {
        if (existingFilterIndex !== -1) {
          const updatedFilters = [...prevFilters];
          updatedFilters.splice(existingFilterIndex, 1);
          return updatedFilters;
        } else {
          return [...prevFilters, { key: key, value: value }];
        }
      });
    }
  };

  const handleStatus = async (key: string, value: string, id: string) => {
    const form = new FormData();
    const dataToUpdate = { [key]: value, _id: id };
    form.append("jsonData", JSON.stringify(dataToUpdate));
    const response = await fetch("/api/requirements", {
      method: "PUT",
      body: form,
    });
    dispatch(hideLoader());
    if (response.ok) {
      dispatch<any>(hidePopup());
      setPopup({});
      setUpdateStatusData(false);
      setUpdatePriorityData(false);
      const data = await response.json();
      await func();
      if (data.success) {
        dispatch<any>(
          showToaster({
            description: data.message,
            timer: 1000,
          })
        );
      } else {
        dispatch<any>(
          showToaster({
            description:
              data.message ||
              (Object.values(data.error.errors) as Error[])[0]?.message,
            timer: 3000,
            variant: "red",
          })
        );
      }
    }
  };

  const handleConfirm = (key: string, value: string, id: string) => {
    dispatch<any>(
      showPopup({
        title: "Update Requirement",
        description: "Are you would like to update these requirements.",
        btnArray: updateConfig("Update", () => handleStatus(key, value, id)),
        className: { popupContainer: "w-1/4", popup: "pt-3 rounded-lg" },
      })
    );
  };

  const updateCardStatus = (Array: any, id: any, name: string) => {
    return (
      <div className="status-Popup px-3 py-2">
        <div className="flex flex-col ">
          {Array.map((item: any) => {
            return (
              <div
                key={item.id}
                className="flex items-center gap-2"
                style={{ color: `${item.className}` }}
                onClick={() => handleConfirm(name, item.value, id)}
              >
                <Icon type={item.icon.type} fill={item.icon.fill} />
                {item.value}
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  //card button popup
  const showPopupDetails = (item: RequirementData) => {
    return Object.keys(popup).length && popup?._id === item?._id ? (
      <>
        <div
          role="presentation"
          className={`fixed inset-0 bg-black bg-opacity-30  z-10 justify-center items-center flex outline-none focus:outline-none`}
          onClick={() => setPopup({})}
        />
        <ul className="popup bg-white p-2">
          <li
            className="details-item "
            onClick={() => {
              setShowDetailView(true);
              setSelectedItem(item);
              setUpdatePriorityData(false);
              setUpdateStatusData(false);
              setPopup({});
            }}
          >
            View More
          </li>
          {/* <li className="details-item">Open Requirement</li> */}
          {(currentUserRole === "admin" ||
            item.accountManager.includes(currentUserName)) && (
            <>
              <li className="details-item">
                <Link href={`/requirements/update/${item._id}`}>
                  Edit Deatils
                </Link>
              </li>
              <li
                className={`details-item ${
                  updateStatusData ? "selectedFeild" : ""
                }`}
                onClick={() => {
                  setUpdateStatusData(!updateStatusData);
                  setUpdatePriorityData(false);
                }}
              >
                change Status
              </li>
              <li
                className={`details-item hover:bg-blue ${
                  updatePriorityData ? "selectedFeild" : ""
                }`}
                onClick={() => {
                  setUpdatePriorityData(!updatePriorityData);
                  setUpdateStatusData(false);
                }}
              >
                change Priority
              </li>
            </>
          )}
        </ul>

        {updateStatusData &&
          updateCardStatus(RequirementStatus, item._id, "status")}
        {updatePriorityData &&
          updateCardStatus(requirementPriority, item._id, "priority")}
      </>
    ) : null;
  };

  const handleDeleteRequirement = async () => {
    const id = checkedData.map((obj: RequirementData) => obj._id);
    dispatch<any>(showLoader());
    const response = await fetch("/api/requirements", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id: id }),
    });
    if (response.ok) {
      await func();
      setCheckedData([]);
      dispatch<any>(hidePopup());
      dispatch<any>(hideLoader());
    }
  };

  const handleDeleteIcon = async () => {
    dispatch<any>(
      showPopup({
        head: {
          icon: {
            type: "trash",
            fill: "#F04438",
            className: "mx-2 my-3 border rounded-lg bg-red-100 p-2",
          },
        },
        title: "Delete Requirement",
        description:
          "Are you would like to delete these requirements? This action can not be undone.",
        btnArray: updateConfig("delete", handleDeleteRequirement),
        className: { popupContainer: "w-1/4" },
      })
    );
  };

  const handleMultipleDownloadResume = async () => {
    try {
      const attachmentId = checkedData.flatMap((obj: RequirementData) =>
        obj?.attachment?._id !== undefined ? [obj.attachment._id] : []
      );
      dispatch<any>(showLoader());
      if (attachmentId.length > 0) {
        const response = await fetch("/api/file", {
          method: "POST",
          body: JSON.stringify({ id: attachmentId }),
        });

        if (response.ok) {
          const { files } = await response.json();
          files.forEach((fileData: any) => {
            const { content, contentType, filename } = fileData;
            const blob = base64ToBlob(content, contentType);
            downloadFiles(blob, filename);
          });
        }
        dispatch<any>(hideLoader());
      } else {
        dispatch<any>(hideLoader());
        dispatch<any>(
          showToaster({
            description:
              "Select the file, attachment to the requirement for download the job description.",
            timer: 2000,
            variant: "green",
          })
        );
      }
    } catch (error) {
      console.log("Error downloading file:", error);
    }
  };
  const handleCheckboxData = (data: any) => {
    const { _id } = data;
    const isExist = checkedData.some(
      (item: RequirementData) => item._id === _id
    );
    if (isExist) {
      const updatedData = checkedData.filter((item: any) => item._id !== _id);
      setCheckedData(updatedData);
    } else {
      setCheckedData((prevData: any) => [...prevData, data]);
    }
  };

  const handleRemoveFilterIcon = async (item: FilterTypes) => {
    const removedItem = filters.filter(
      (entry: any) => entry.key !== item.key || entry.value !== item.value
    );
    setFilters(removedItem);
    const filterdData = applyFiltersToData(storeData, removedItem);
    setFilteredData(filterdData);
  };

  const handleExpand = async (id: string) => {
    try {
      dispatch(showLoader());
      const response = await fetch("/api/file", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      });
      if (response.ok) {
        const blob = await response.blob();
        const blobUrl = URL.createObjectURL(blob);
        const link: any = document.createElement("a");
        link.href = blobUrl;
        link.target = "_blank";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
      dispatch(hideLoader());
    } catch (error) {
      dispatch(hideLoader());
      console.log("Error downloading file:", error);
    }
  };

  const handleDownload = async (id: string) => {
    try {
      dispatch<any>(showLoader());
      const response = await fetch("/api/file", {
        method: "POST",
        body: JSON.stringify({ id: id }),
      });

      if (response.ok) {
        const fileName = response.headers.get("Content-Disposition");
        const blob = await response.blob();
        downloadFiles(blob, fileName);
      }
      dispatch<any>(hideLoader());
    } catch (error) {
      console.log("Error downloading file:", error);
    }
  };

  const handleNotesComment = async (data: any, id: any) => {
    if (data) {
      const postComment = { ...data, postedBy: currentUserName };
      try {
        dispatch(showLoader());
        const response = await fetch("/api/requirements/note", {
          method: "POST",
          body: JSON.stringify({
            postComment,
            _id: id ? id.id : selectedItem?._id,
          }),
        });
        if (response.ok) {
          const data = await response.json();
          await func();
          setShowDetailView(false);
          dispatch(hideLoader());
          if (data.success) {
            dispatch(
              showToaster({
                timer: 2000,
                description: data.message,
              })
            );
          }
        }
        dispatch(hideLoader());
      } catch (error) {
        console.log(error);
      }
    } else {
      dispatch(
        showToaster({
          timer: 2000,
          description: "Comment cannot be empty add atleast any comment",
        })
      );
    }
  };
  return (
    <Fragment>
      <div className="flex flex-row mr-12 gap-2 items-center my-4  mb-8  text-gray-500">
        <Icon type="back" />
        <div className="flex flex-row justify-between items-center text-sm">
          <span>Dashboard</span>
          <Icon type="chev_right" />
          <span>Requirement</span>
          <Icon type="chev_right" />
          <span>All Requirement</span>
        </div>
      </div>

      <div className="flex justify-between items-center my-4 font-bold">
        <div className="flex gap-12 items-center">
          <div className={`font-bold text-xl`}>
            {id ? "My Requirements" : "Requirements"}
          </div>
          <div
            style={{ color: `${COLOR.BLUE}` }}
            className={`flex  justify-evenly items-center gap-4 text-s `}
          >
            <Link href={"/requirements/create"}>
              <div className="flex items-center gap-2">
                <Icon type="plus" fill={COLOR.BLUE} />
                Add Requirement
              </div>
            </Link>

            {id ? (
              <div>
                <Link href={`/requirements`}>All Requirement</Link>
              </div>
            ) : (
              <div>
                <Link href={`/requirements/${currentUserId}`}>
                  My Requirement
                </Link>
              </div>
            )}
            {!id && (
              <Link href={`/clients`}>
                {" "}
                <div className="flex items-center gap-2">
                  <Icon type="client" fill={COLOR.BLUE} />
                  All Clients
                </div>
              </Link>
            )}
          </div>
        </div>
      </div>
      <div className="flex justify-between items-center mt-4">
        <SearchBar
          type="text"
          placeholder="Search client here"
          onChange={handleSearchFilter}
          width="300px"
          className={"border-2 rounded-lg"}
        />
        <div className="flex justify-between items-center gap-4">
          {" "}
          <div
            className={`flex items-center gap-2 border rounded-md p-2 font-medium text-center 
            ${showCheckBox && "text-blue-600 border-blue-600 bg-blue-200"}
            `}
            onClick={handleSelectButton}
          >
            <Icon
              type="Select"
              width="20"
              height="20"
              fill={showCheckBox ? "blue" : ""}
            />
            Select
          </div>
          <div className="border rounded-md p-2">
            <Icon
              type="filter"
              width="20"
              height="20"
              onClick={() => setIsFilterOpen(true)}
            />
          </div>
        </div>
      </div>

      <div
        className={`${
          (showCheckBox || (!isFilterOpen && filters.length > 0)) &&
          "bg-gray-100 rounded-lg"
        } mt-2 pb-2 mx-2 min-h-[80%]`}
      >
        <div className="mx-2 pt-2">
          {isFilterOpen && (
            <Filter
              config={filterConfiData}
              setPopup={() => setIsFilterOpen(!isFilterOpen)}
              resetFilterHandler={handleFilterReset}
              values={filters}
              filterHandler={handleApplyFilter}
              selectedFilter={handleSelectedFilter}
              btnConfig={filterBtnConfig()}
            />
          )}

          {filters.length > 0 && (
            <div className="flex flex-row gap-2 ">
              {filters?.map((item: any, index: number) => {
                return (
                  <div
                    key={index}
                    className="apply-filter flex items-center text-center gap-2 border rounded-lg px-2 py-1 "
                  >
                    <div className="text-white font-medium">
                      {typeof item.value === "object" ? (
                        <div>{`${item.value.minimum} - ${item.value.maximum} `}</div>
                      ) : (
                        item.value
                      )}
                    </div>
                    <Icon
                      type="close"
                      width="18"
                      height="18"
                      fill="white"
                      onClick={() => handleRemoveFilterIcon(item)}
                    />
                  </div>
                );
              })}
              {!isFilterOpen && (
                <div
                  className="add-filter flex items-center text-center gap-2 border rounded-lg px-2 py-1 font-medium"
                  onClick={() => setIsFilterOpen(true)}
                >
                  <Icon type="plus" fill="blue" />
                  Add Filter
                </div>
              )}
            </div>
          )}
        </div>

        <div className="mx-2 pt-2">
          {filteredData?.length > 0
            ? filteredData.length
            : !isFilterOpen && filters.length > 0
            ? []
            : storeData?.length}{" "}
          Result
        </div>

        <div className="mx-3">
          <Card
            data={
              filteredData?.length > 0
                ? filteredData
                : !isFilterOpen && filters.length > 0
                ? []
                : storeData
            }
            config={RequirementConfiguration}
            gridCols={3}
            columns={5}
            showDetailsIcon
            showCheckBox={showCheckBox}
            checkedData={checkedData}
            selectData={handleCheckboxData}
            showPopup={showCardPopup}
            showDetailView={showDeatailView}
            detailsPopup={(item: any) => {
              return showPopupDetails(item);
            }}
          />
        </div>
      </div>

      {showCheckBox && (
        <div className="w-1/2 fixed bottom-20 left-1/2 z-10 transform -translate-x-1/2 ml-20 bg-black p-2.5 border rounded-2xl text-white">
          <div className="flex justify-between items-center gap-2">
            <div className="flex items-center gap-2">
              <Icon
                type="close"
                width="18"
                height="18"
                fill="white"
                onClick={handleSelectButton}
              />
              {checkedData.length} Selected
            </div>
            <div className="flex items-center gap-2">
              {/* <span className="flex items-center gap-2 border rounded-lg bg-gray-800 p-2">
                <Icon type="excel" width="20" height="20" fill="green" />{" "}
                Expoert To CSV
              </span> */}
              <span
                className="flex items-center gap-2 border rounded-lg bg-gray-800 p-2"
                onClick={handleMultipleDownloadResume}
              >
                <Icon type="download" width="15" height="15" fill="white" />
                Job Description
              </span>
              {/* <span
                className="border rounded-lg bg-red-500 p-2.5"
                onClick={handleDeleteIcon}
              >
                <Icon type="trash" fill="white" />
              </span>{" "} */}
            </div>
          </div>
        </div>
      )}

      <DetailView
        detailView={showDetailView}
        heading="Requirement Detail"
        setShowDetail={setShowDetailView}
        selectedItem={selectedItem}
        headerConfig={HeaderConfig()}
        tabsConfig={tabsConfig()}
        detailConfig={detailConfig()}
        handleDownload={handleDownload}
        handleExpand={handleExpand}
        handleComment={handleNotesComment}
      />
      <style>
        {`
        .apply-filter{
          background-color:blue;

        }
        .add-filter{
          color:blue;
          background-color:#3371EA1A;
        }
         .popup-wrapper {
          position:fixed;
          background-color:rgba(0, 0, 0, 0.5);
          inset: 0px;
          z-index:10;
         }
         .popup{
          width:170px;
          position:absolute;
          top:0;
          right:0;
          z-index:20;
          border: 1px solid #C6C6CA;
          border-radius: 8px;

         }

         .status-Popup{
          width:170px;
          position:absolute;
          top:0;
          right:180px;
          z-index:10;
          border: 1px solid #C6C6CA;
          border-radius: 8px;
          background-color: white;
         }
         .details-item{
          font-weight:500;
          padding:3px 3px;
          cursor:pointer;
        }
      
        .selectedFeild{
          border-radius: 5px;
          background-color:${COLOR.GREEN};
        }
         .details-item:last-child{
          border:0;
        }
         `}
      </style>
    </Fragment>
  );
}
