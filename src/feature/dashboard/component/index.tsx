"use client";
import { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import { useDispatch } from "react-redux";
import { hideLoader, showLoader } from "@/store/common";
import Link from "next/link";
import Icon from "@/lib/svg";
import { COLOR } from "@/constants/theme";
const Dashboard = () => {
  const dispatch = useDispatch();
  const [summaryData, setSummaryData] = useState({});
  const [count, setCount] = useState<any>();
  const [TodayCount, setTodayCount] = useState<any>();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const route = useRouter();
  useEffect(() => {
    if (localStorage.getItem("user_role") !== "admin") {
      route.push("/vendors");
    }
  }, []);

  const fetchData = async () => {
    try {
      dispatch(showLoader());
      const response = await fetch("/api/dashboard");
      if (response.ok) {
        const data = await response.json();
        const { count, summary } = data;
        setCount(count);
        setSummaryData(summary);
        calculateTotals(summary);
      }
      dispatch(hideLoader());
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchSelectedData = async () => {
    try {
      dispatch(showLoader());
      const response = await fetch("/api/dashboard", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          startDate: startDate.toISOString(),
          endDate: endDate.toISOString(),
        }),
      });
      if (response.ok) {
        const data = await response.json();
        setSummaryData(data.response);
        calculateTotals(data.response);
      }
      dispatch(hideLoader());
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const calculateTotals = (data: any) => {
    let newVendorsCount = 0;
    let newClientsCount = 0;
    let newResourceCount = 0;
    let newRequirementCount = 0;
    for (const userId in data) {
      if (data[userId].vendorsCount > 0) {
        newVendorsCount++;
      }
      if (data[userId].clientsCount > 0) {
        newClientsCount++;
      }
      if (data[userId].resourceCount > 0) {
        newResourceCount++;
      }
      if (data[userId].requirementCount > 0) {
        newRequirementCount++;
      }
    }
    const todayData = {
      vendor: newVendorsCount,
      client: newClientsCount,
      resource: newResourceCount,
      requirement: newRequirementCount,
    };
    setTodayCount(todayData);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
        <div className="">
          <div className="flex justify-between items-center my-4 font-bold">
            <div className="flex gap-12 items-center">
              <div className={`font-bold text-xl`}>Dashboard</div>
              <div
                style={{ color: `${COLOR.BLUE}` }}
                className={`flex justify-evenly items-center gap-4 text-s `}
              >
                <Link href={"/vendors"}>
                  <div className="flex items-center gap-2">
                    <Icon type="vendor" fill={COLOR.BLUE} />
                    All Vendor
                  </div>
                </Link>
                <Link href={`/clients`}>
                  <div className="flex items-center gap-2">
                    <Icon type="client" fill={COLOR.BLUE} />
                    All Clients
                  </div>
                </Link>
                <Link href={`/resources`}>
                  <div className="flex items-center gap-2">
                    <Icon
                      type="resource"
                      fill={COLOR.BLUE}
                      width="15"
                      height="15"
                    />
                    <span>All resource</span>
                  </div>
                </Link>

                <Link href={`/requirements`}>
                  <div className="flex items-center gap-2">
                    <Icon
                      type="requirements"
                      fill={COLOR.BLUE}
                      width="15"
                      height="15"
                    />{" "}
                    <span>All Requirement</span>
                  </div>
                </Link>
              </div>
            </div>
          </div>
          <div className="flex flex-col justify-center items-center border w-96 p-3 mx-auto my-auto rounded-xl bg-gray-50">
            <div className="flex flex-row justify-center gap-2 mb-3">
              <label id="start Date"> Start Date : </label>
              <input
                type="date"
                id="start Date"
              
                className=" border rounded-md px-2 text-sm py-1"
                onChange={(e) => setStartDate(new Date(e.target.value))}
              />
            </div>
            <div className="flex flex-row justify-center gap-2 mb-3">
              <label id="End Date"> End Date : </label>
              <input
                id="End Date"
                className=" border rounded-md px-2 text-sm py-1"
              
                type="date"
                onChange={(e) => setEndDate(new Date(e.target.value))}
              />
            </div>
            <div className="flex flex-row justify-center mx-10 gap-5 ">
              <button
                className="bg-green-300  px-6 text-black rounded-lg"
                onClick={fetchSelectedData}
              >
                Submit
              </button>
              <button
                className="bg-green-300  px-6 text-black rounded-lg"
                onClick={() => {
                  fetchData();
                  setStartDate(new Date());
                  setEndDate(new Date());
                }}
              >
                Clear
              </button>
            </div>
          </div>

          <div className="my-6">
            <div className="flex justify-between">
              <div className="bg-blue-100 text-blue-800 p-4 rounded-lg shadow-lg">
                Total New Vendors Added: {TodayCount?.vendor}
              </div>
              <div className="bg-green-100 text-green-800 p-4 rounded-lg shadow-lg">
                Total New Clients Added: {TodayCount?.client}
              </div>
              <div className="bg-blue-100 text-blue-800 p-4 rounded-lg shadow-lg">
                Total New Resource Added: {TodayCount?.resource}
              </div>
              <div className="bg-green-100 text-green-800 p-4 rounded-lg shadow-lg">
                Total New Requirement Added: {TodayCount?.requirement}
              </div>
            </div>
          </div>
          <div className="mb-4">
            <div className="bg-yellow-100 text-yellow-800 p-4 rounded-lg shadow-lg">
              Total Vendors: {count?.vendor}
            </div>
            <div className="bg-pink-100 text-pink-800 p-4 rounded-lg shadow-lg">
              Total Clients: {count?.client}
            </div>
            <div className="bg-green-100 text-green-800 p-4 rounded-lg shadow-lg">
              Total Resource: {count?.resource}
            </div>
            <div className="bg-blue-100 text-blue-800 p-4 rounded-lg shadow-lg">
              Total Requirement: {count?.requirement}
            </div>
            <div className="bg-purple-100 text-purple-800 p-4 rounded-lg shadow-lg">
              Total Users: {count?.user}
            </div>
          </div>
          <table className="min-w-full divide-y divide-gray-300">
            <thead>
              <tr>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  User
                </th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Vendors Count
                </th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Clients Count
                </th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  resource Count
                </th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Requirement Count
                </th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Total Changes
                </th>
              </tr>
            </thead>
            <tbody>
              {Object?.entries(summaryData).map(([userId, data]: any) => (
                <tr key={userId}>
                  <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                    {userId}
                  </td>
                  <td className="px-6 pl-20 py-4 whitespace-nowrap text-sm text-gray-900">
                    {data.vendorsCount || 0}
                  </td>
                  <td className="px-6 pl-20 py-4 whitespace-nowrap text-sm text-gray-900">
                    {data.clientsCount || 0}
                  </td>
                  <td className="px-6 pl-20 py-4 whitespace-nowrap text-sm text-gray-900">
                    {data.resourceCount || 0}
                  </td>
                  <td className="px-6 pl-20 py-4 whitespace-nowrap text-sm text-gray-900">
                    {data.requirementCount || 0}
                  </td>
                  <td className="px-6 pl-20 py-4 whitespace-nowrap text-sm text-gray-900">
                    {parseInt(data.vendorsCount || 0) +
                      parseInt(data.clientsCount || 0) +
                      parseInt(data.resourceCount || 0) +
                      parseInt(data.requirementCount || 0)}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
    </>
  );
};

export default Dashboard;
