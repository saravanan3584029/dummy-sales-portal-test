"use client";
import Icon from "@/lib/svg";
import FormRender from "@/utils/form/formRender";
import { resourceBtnConfig } from "@/feature/resources/config/FormConfig";
import { formLinkConfig, vendorFormConfig } from "../config/vendorFormConfig";
import { vendorValidations } from "../config/validationConfig";
import { hideLoader, showLoader, showToaster } from "@/store/common";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/navigation";
import { useCallback, useEffect, useState } from "react";
import { setUserData } from "@/store/user";
import Notes from "@/shared-components/components/Note";
import { setVendorData } from "@/store/vendor";
import { VendorData } from "../config/types";
import { getItemFromLocalStorage } from "@/utils";

export default function VendorForm({
  columnNum = 3,
  id,
}: {
  columnNum?: number;
  id?: string;
}) {
  const dispatch = useDispatch();
  const router = useRouter();
  const [updateData, setUpdateData] = useState<any>();

  const [currentUserId, setCurrentUserId] = useState<any>();
  const [currentUserName, setCurrentUserName] = useState<any>();
  
  const userStore = useSelector((store: any) => store.user.user);
  const vendorStore = useSelector((store: any) => store.vendor.vendor);
  
  useEffect(() => {
    setCurrentUserId(getItemFromLocalStorage("user_id"));
    setCurrentUserName(getItemFromLocalStorage("user_name"));
    const accountManager = async () => {
      const response = await fetch(`/api/user/`);
      if (response.ok) {
        const data = await response.json();
        dispatch<any>(setUserData(data.response));
      }
    };
    accountManager();
  }, []);

  const fetchSingleUser = useCallback(async () => {
    try {
      const response = await fetch(`/api/vendors/` + id);
      if (response.ok) {
        const data = await response.json();
        setUpdateData(data.response);
      }
    } catch (error) {
      console.error("Fetch Error:", error);
    }
  }, [id]);

  useEffect(() => {
    const fetchData = async () => {
      if (id && vendorStore.length === 0) {
        await fetchSingleUser();
      } else {
        const data = vendorStore.find((item: any) => item._id === id);
        setUpdateData(data);
      }
    };
    fetchData();
  }, [id, vendorStore, fetchSingleUser]);

  const formData = vendorFormConfig(userStore);

  const createVendorHandler = async (data: any) => {
    const formData = new FormData();
    let { ndaFile, ...reminingData } = data;
    formData.append("file", ndaFile);
    formData.append("jsonData", JSON.stringify(reminingData));
    try {
      const response = await fetch("/api/vendors", {
        method: "POST",
        body: formData,
      });
      dispatch<any>(hideLoader());
      if (response.ok) {
        const data = await response.json();
        if (data.success) {
          dispatch<any>(
            showToaster({
              description: data.message,
              timer: 1000,
            })
          );
          router.push("/vendors");
        } else {
          dispatch<any>(
            showToaster({
              description:
                data.message ||
                (Object.values(data.error.errors) as Error[])[0]?.message,
              timer: 3000,
              variant: "red",
            })
          );
        }
      }
    } catch (error) {
      console.log(error);
    }
  };
  const updateVendorHandler = async (data: any) => {
    let updateObject: any = {};
    Object.keys(data).map((ele) => {
      if (ele === "ndaFile") {
        if (data[ele] !== updateData[ele]?.filename) {
          updateObject = { ...updateObject, [ele]: data[ele] };
        }
      } else if (Array.isArray(data[ele])) {
        const hasDifference = data[ele].some((item: any, index: number) => {
          if (typeof item === "string") {
            return item !== updateData[ele][index];
          } else {
            return (
              JSON.stringify(item) !== JSON.stringify(updateData[ele][index])
            );
          }
        });
        if (hasDifference) {
          updateObject = { ...updateObject, [ele]: data[ele] };
        }
      } else if (data[ele] !== updateData[ele]) {
        updateObject = { ...updateObject, [ele]: data[ele] };
      }
    });
    try {
      if (Object.keys(updateObject).length > 0) {
        dispatch(showLoader());
        const form = new FormData();
        const { ndaFile, ...entry } = updateObject;
        const dataToUpdate = { ...entry, _id: id };
        ndaFile && form.append("file", ndaFile);
        form.append("jsonData", JSON.stringify(dataToUpdate));
        const response = await fetch("/api/vendors", {
          method: "PUT",
          body: form,
        });
        dispatch(hideLoader());
        if (response.ok) {
          const data = await response.json();
          if (data.success) {
            dispatch<any>(
              showToaster({
                description: data.message,
                timer: 1000,
              })
            );
            router.push("/vendors");
          } else {
            dispatch<any>(
              showToaster({
                description:
                  data.message ||
                  (Object.values(data.error.errors) as Error[])[0]?.message,
                timer: 3000,
                variant: "red",
              })
            );
          }
        }
      } else {
        dispatch<any>(
          showToaster({
            description: "No entry has been updated in the form.",
            timer: 2000,
            variant: "green",
          })
        );
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleNotesComment = async (data: any, replyId: any) => {
    if (id) {
      if (data) {
        const postComment = { ...data, postedBy: currentUserName };
        try {
          dispatch(showLoader());
          const response = await fetch("/api/vendors/note", {
            method: "POST",
            body: JSON.stringify({
              postComment,
              _id: replyId ? replyId.id : id,
            }),
          });
          if (response.ok) {
            const data = await response.json();
            await fetchSingleUser();
            dispatch(hideLoader());
            if (data.success) {
              dispatch(
                showToaster({
                  timer: 2000,
                  description: data.message,
                })
              );
            }
          }
          dispatch(hideLoader());
        } catch (error) {
          console.log(error);
        }
      } else {
        dispatch(
          showToaster({
            timer: 2000,
            description: "Comment cannot be empty add atleast any comment",
          })
        );
      }
    } else {
      dispatch(
        showToaster({
          timer: 2000,
          description:
            "Prior to vendor creation, commenting functionality shall be restricted.",
        })
      );
    }
  };
  return (
    <div>
      <div className="flex flex-row mr-12 gap-2 items-center mt-4 text-gray-500 cursor-pointer mb-5">
        <Icon type="back" />
        <div className="flex flex-row justify-between items-center text-m">
          <span>Dashboard</span>
          <Icon type="chev_right" />
          <span>Vendors</span>
          <Icon type="chev_right" />
          <span>Create vendors</span>
        </div>
      </div>

      <FormRender
        formHeading={`${id ? "Update Vendor" : "Create Vendor"}`}
        formColumn={columnNum}
        formLink={formLinkConfig("")}
        formLinkStyle={`text-blue-500 font-bold`}
        formLabelStyle="mb-2 "
        formLine={true}
        formData={formData}
        initialStaticData={
          id
            ? {
                vendorName: updateData?.vendorName,
                selectedOptions: updateData?.selectedOptions,
                location: updateData?.location,
                paymentTerms: updateData?.paymentTerms,
                ndaFile: updateData?.ndaFile?.filename,
                accountManager: updateData?.accountManager,
              }
            : {
                vendorName: "",
                selectedOptions: [],
                location: "",
                paymentTerms: "",
                ndaFile: "",
                accountManager: [],
              }
        }
        initialNestedData={{ contactPersons: id && updateData?.contactPersons }}
        validations={vendorValidations}
        submitForm={id ? updateVendorHandler : createVendorHandler}
        btnConfig={resourceBtnConfig(id ? "Update" : "Create")}
        btnStyle={false}
      />

      <Notes handleComment={handleNotesComment} notesData={updateData} />
    </div>
  );
}
