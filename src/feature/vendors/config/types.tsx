export type ContactPersons = {
  _id?: string;
  phoneNumber: number;
  email: string;
  name: string;
  designation: string;
  isPrimary: boolean;
};

export type VendorData = {
  _id?: string;
  vendorName: string;
  contactPersons: ContactPersons[];
  selectedOptions: string[];
  paymentTerms: string;
  location: string;
  ndaFile: { _id?: string } | File;
  accountManager: string[];
};
