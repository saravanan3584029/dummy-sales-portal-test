import { COLOR } from "@/constants/theme";
import Icon from "@/lib/svg";
import { formatNumberToString, getTimeDifference } from "@/utils";

export const vendorCardConfig: any = {
  vendorName: {
    val: "vendorName",
    class: "col-span-5 font-semibold text-base capitalize leading-4",
    icon: {
      type: "vendor",
      fill: "#AAAAAF",
      width: "24",
      height: "24",
      class: "bg-gray-100 rounded-xl p-1",
    },
  },
  selectedOptions: {
    val: "selectedOptions",
    class: "col-span-5 mt-6 mb-2",
    function: (array: any) => {
      const result = array.map((item: string, index: number) => {
        if (item === "Onsite") {
          return (
            <div
              key={index}
              className="border rounded-3xl border-purple-200 text-purple-700 p-2"
            >
              {item}
            </div>
          );
        } else if (item === "Remote") {
          return (
            <div
              key={index}
              className="border rounded-3xl border-pink-200 text-pink-700 p-2"
            >
              {item}
            </div>
          );
        } else {
          return (
            <div
              key={index}
              className="border rounded-3xl border-orange-200 text-orange-700 p-2"
            >
              {item}
            </div>
          );
        }
      });
      return (
        <div className="flex items-center gap-2 text-m font-medium text-center">
          {result}
        </div>
      );
    },
  },

  location: {
    icon: { type: "location" },
    val: "location",
    class: "col-span-3 mt-2 text-gray-500 text-m",
  },

  paymentTerms: {
    icon: { type: "payment", width: "20", height: "20" },
    val: "paymentTerms",
    class: "mt-2 text-gray-500 col-span-2 text-m",
  },
  contactPersons: {
    val: "contactPersons",
    class: "col-span-5 text-gray-500 mt-2 text-m",
    icon: {
      type: "phone",
      fill: "#66666E",
    },
    function: (array: any) => {
      const renderData = array.slice(0, 2);
      const result = renderData.map((entry: any, index: number) => {
        return `${entry.name}${renderData.length === index + 1 ? " " : ","} `;
      });
      array.length > 2 && result.push(<div>+ {array.length - 2}</div>);
      return (
        <div className="flex items-center justify-start gap-x-1 text-m ">
          {result}
        </div>
      );
    },
  },

  resource: {
    val: "resources",
    class: "col-span-5 text-gray-500 mt-2 text-m",
    icon: {
      type: "resource",
      width: "15",
      height: "15",
      fill: "#B1B4BA",
    },
    function: (data: any) => {
      return `${data.length} resources`;
    },
  },

  accountManager: {
    val: "accountManager",
    class: " mt-6 col-span-3 text-gray-500 text-m",
    icon: {
      type: "user-circle",
      fill: "#AAAAAF",
    },
  },
  createdAt: {
    val: "createdAt",
    class: "mt-6 text-gray-400 col-span-2 text-m",
    icon: { type: "clock", width: "15", fill: "#AAAAAF" },
    function: (data: any) => {
      const formattedDate = getTimeDifference(data);
      return `${formattedDate} ago`;
    },
  },
};
