import Icon from "@/lib/svg";
import { formatNumberToCurrency, formatNumberToString } from "@/utils";
import { ContactPersons, VendorData } from "./types";
import { COLOR } from "@/constants/theme";

export const vendorHeaderConfig = () => {
  return {
    "Vendor Name": {
      val: "vendorName",
      class: "col-span-3 text-xl font-semibold my-3 leading-8 capitalize",
    },
    selectedOptions: {
      val: "selectedOptions",
      class: " col-span-3",
      function: (array: any) => {
        const result = array.map((item: string, index: number) => {
          if (item === "Onsite") {
            return (
              <div
                key={index}
                className="border rounded-3xl border-purple-200 text-purple-700 p-2"
              >
                {item}
              </div>
            );
          } else if (item === "Remote") {
            return (
              <div
                key={index}
                className="border rounded-3xl border-pink-200 text-pink-700 p-2"
              >
                {item}
              </div>
            );
          } else {
            return (
              <div
                key={index}
                className="border rounded-3xl border-orange-200 text-orange-700 p-2"
              >
                {item}
              </div>
            );
          }
        });
        return (
          <div className="flex items-center gap-2 text-m font-medium text-center ">
            {result}
          </div>
        );
      },
    },
  };
};

export const vendorResourceConfig = () => {
  return {
    Name: {
      val: "name",
      class: "col-span-5 font-semibold text-base capitalize leading-5",
    },

    status: {
      val: "status",
      class:
        "rounded-xl font-medium bg-green-100 text-center text-green-800 text-m p-1 mr-2 mt-2",
    },
    experience: {
      icon: { type: "dot", fill: "#AAAAAF" },
      val: "experience",
      class: " text-gray-500 text-m mt-3",
      function: (data: number) => {
        return (
          <div className="flex items-center gap-1">
            <Icon type="work-duration" width="16" height="16" fill="#66666E" />
            {data} yrs
          </div>
        );
      },
    },
    costing: {
      icon: { type: "dot", fill: "#AAAAAF" },
      val: "costing",
      class: " text-gray-500 text-m mt-3",
      function: (data: number) => {
        const number = formatNumberToString(data);
        return (
          <div className="flex items-center gap-1">
            <Icon type="rupee" width="16" height="16" fill="#66666E" />
            {number}
          </div>
        );
      },
    },

    location: {
      icon: { type: "location" },
      val: "location",
      class: "text-gray-500 text-m mt-3",
    },
    workMode: {
      icon: { type: "dot", fill: "#AAAAAF" },
      val: "workMode",
      class: "text-gray-500 text-m mt-3",
    },
    technology: {
      val: "technology",
      class: "col-span-5 text-m mt-3",
      function: (data: any) => {
       
        const result = data.map((entry: any, index: number) => {
          return (
            <div
              key={index}
              className={`flex items-center gap-2 text-center `}
            >
           { index!==0 && <Icon type="dot" fill="#AAAAAF"/>}  {entry.techStack}
             
            </div>
          );
        });
        return (
          <div className="flex items-center flex-row flex-wrap justify-start gap-x-2 text-gray-500">
            {result}
          </div>
        );
      },
    },
  };
};
export const vendorDetailTabConfig = () => {
  return {
    Location: {
      val: "location",
      class: "my-2 text-m",
      value: "col-span-2",
    },
    "Payment Terms": {
      val: "paymentTerms",
      class: "my-2 text-m",
      value: "col-span-2",
    },
    "Total Resources": {
      val: "resources",
      class: "my-2 text-m",
      value: "col-span-2",
      function: (array: any) => {
        return array.length;
      },
    },
    Contacts: {
      val: "contactPersons",
      class: "my-2 text-m",
      value: "col-span-2",
      line: true,
      function: (array: ContactPersons[]) => {
        const result = array.map((item, index) => {
          return (
            <div key={item._id} className="mb-6 text-gray-600">
              <div className="flex items-center gap-x-3 font-medium my-2">
                <div>{item.name}</div>
                <div className=" text-gray-400">{item.designation}</div>
              </div>
              <div className="flex items-center gap-2 my-2">
                <Icon type="phone" fill="#84848C" />
                {item.phoneNumber}
              </div>
              <div className="flex items-center gap-1">
                <span className="text-gray-400">@</span> {item.email}
              </div>
            </div>
          );
        });
        return result;
      },
    },

    Technology: {
      val: "resources",
      key: "col-span-3 text-m",
      value: "col-span-3",
      function: (data: any) => {
        const uniqueArray: string[] = Array.from(
          new Set(
            data?.flatMap((item: any) =>
              item.technology.map((entry: any) => entry.techStack)
            )
          )
        );
        const result = uniqueArray.slice(0, 15).map((item, index) => {
          return (
            <div
              key={index}
              className=" border-2 text-gray-800 font-medium border-gray-150 rounded-lg border-b-4 text-center p-1 bg-gray-100"
            >
              {item}
            </div>
          );
        });
        uniqueArray.length > 15 &&
          result.push(
            <div className=" border-2 text-gray-800 font-medium border-gray-150 rounded-lg border-b-4 text-center p-1 bg-gray-100">
              + {uniqueArray.length - 15}
            </div>
          );
        return <div className="flex flex-wrap gap-2 py-2">{result}</div>;
      },
    },
    NDA: {
      class: "my-2 text-m",
      val: "ndaFile",
      key: "col-span-3",
      value: "col-span-3 mt-2",
      function: (data: any, handleExpand: any, download: any) => {
        return (
          <>
          { data&& <div className="flex justify-between items-center border-2 rounded-lg px-2 py-1">
              <div className="flex items-center gap-2">
                <span className="bg-blue-100 rounded-xl p-2">
                  <Icon
                    type="attachment"
                    width="20"
                    height="20"
                    fill="#1A68F3"
                  />
                </span>
                {data.filename}
              </div>
              <div className="flex items-center gap-5">
                <Icon
                  type="expand"
                  width="18"
                  height="15"
                  onClick={() => handleExpand(data._id)}
                />
                <Icon
                  type="download"
                  width="18"
                  height="15"
                  onClick={() => download(data._id)}
                />
              </div>
            </div>}
          </>
        );
      },
    },
  };
};

export const vendorTabsConfig = [
  {
    name: "Details",
  },
  {
    name: "Resources",
  },
  {
    name: "Notes",
  },
];
