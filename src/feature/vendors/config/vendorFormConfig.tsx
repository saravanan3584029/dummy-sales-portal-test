// import { accountManager, states } from "@/feature/resources/constant";
import { paymentArray } from "@/feature/clients/constant/index";
import FormRender from "@/utils/form/formRender";
import { BUTTON } from "@/constants/theme";
import { uniqueStatesAndDistricts, technologies, workMode } from "@/feature/resources/constant";

export const formLinkConfig = (id: string) => {
  return {
    "All Resource": {
      id: 1,
      path: `/resources`,
      labelName: "All Resource",
      icon: {
        type: "resource",
        width: "16",
        height: "16",
        fill: "#1A68F3",
      },
    },
    "My Vendors": {
      id: 2,
      path: `/vendors/${id}`,
      labelName: "My Vendors",
    },
    "All Vendors": {
      id: 3,
      path: "/vendors",
      labelName: "All Vendors",
      icon: {
        type: "vendor",
        width: "16",
        height: "16",
        fill: "#1A68F3",
      },
    },
  };
};

export const vendorFormConfig = (user: any) => {
  return {
    section1: {
      class: "mt-5",
      config: {
        Name: {
          elementType: "FormInput",
          placeholder: "Enter Vendor Name",
          disablePaste: false,
          name: "vendorName",
        },
        Type: {
          elementType: "FormSelectMultipleItem",
          options: workMode,
          disablePaste: false,
          inputStyle: true,
          name: "selectedOptions",
          val: "value",
        },
      },
    },

    section2: {
      id: 2,
      class: "mt-5",
      config: {
        Details: {
          className: "row-span-2",
        },
        Location: {
          elementType: "FormDropdown",
          disablePaste: true,
          placeholder: "Select",
          options: uniqueStatesAndDistricts,
          height: "200",
          name: "location",
          val: "value",
          icon: {
            type: "location",
          },
          searchBar: {
            placeholder: "Search here",
          },
        },
        "Payment Terms": {
          elementType: "FormDropdown",
          disablePaste: true,
          placeholder: "Select",
          options: paymentArray,
          name: "paymentTerms",
          val: "value",
        },

        NDA: {
          elementType: "FormUploadInput",
          placeholder: "Upload Attachment",
          name: "ndaFile",
          accept: "application/pdf,.doc,.docx",
        },
      },
    },
    section3: {
      id: 3,
      class: "mt-5",
      config: {
        "Contact person": {
          className: "row-span-2",
        },
        contactPersons: {
          elementType: "NestedForm",
          name: "contactPersons",
          className: "col-span-2 mr-28",
          content: [
            {
              elementType: "FormInput",
              placeholder: "Name",
              key: "Name",
              disablePaste: false,
              name: "name",
              disabled: false,
              className: "col-span-2",
              wrapper: "nestedForm",
            },
            {
              elementType: "FormInput",
              key: "Phone Number",
              disablePaste: false,
              name: "phoneNumber",
              disabled: false,
              className: "col-span-2",
              wrapper: "nestedForm",
              icon: {
                label: "+91",
              },
            },
            {
              elementType: "FormInput",
              placeholder: "Email",
              key: "Email Address",
              disablePaste: false,
              name: "email",
              disabled: false,
              className: "col-span-2",
              wrapper: "nestedForm",
            },
            {
              elementType: "FormInput",
              placeholder: "Designation",
              disablePaste: false,
              key: "Designation Optional",
              name: "designation",
              disabled: false,
              className: "col-span-2",
              wrapper: "nestedForm",
            },
            {
              elementType: "FormToggleSwitch",
              name: "isPrimary",
              label: "Primary contact",
              type: "checkbox",
              className: "col-span-4",
              wrapper: "nestedForm",
            },
          ],
          btnConfig: [
            {
              elementType: "FormButton",
              type: "button",
              goal: "ADD",
              variant: "primary",
              className: "",
              children: "Add",
            },
            {
              elementType: "FormButton",
              type: "button",
              goal: "REMOVE",
              variant: "disable",
              className: "",
              children: "cancel",
            },
          ],
        },
      },
    },

    section4: {
      id: 4,
      class: "mt-5",
      config: {
        "Account Manager ": {},
        "Account Manager": {
          elementType: "FormSelectMultipleItem",
          options: user,
          val: "name",
          disablePaste: false,
          name: "accountManager",
          className: "col-span-2",
          width: "300",
          icon: {
            image: "/images/profile.png",
          },
        },
      },
    },
  };
};

export const nestedFormBtnConfig = (btnName: string) => [
  {
    id: 1,
    type: "button",
    size: "medium",
    children: btnName,
    variant: "primary",
    classname: "",
  },
  {
    id: 2,
    type: "reset",
    size: "medium",
    varient: "secondary",
    children: "Cancel",
    className: "border rounded-lg",
  },
];
