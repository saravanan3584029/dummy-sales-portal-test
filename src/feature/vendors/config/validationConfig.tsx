import validationMessages from "@/utils/validation/validationMessages";

export const vendorValidations = {
  vendorName: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
    pattern: {
      type: "name",
      message: validationMessages.isText,
    },
  },
  selectedOptions: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  location: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  paymentTerms: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  ndaFile: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },

  accountManager: {
    required: {
      value: true,
      message: validationMessages.isEmpty,
    },
  },
  contactPersons: {
    required: {
      value: true,
      message: validationMessages.isNotPrimaryContact, 
    },
  },
};
