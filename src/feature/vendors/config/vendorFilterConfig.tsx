import Icon from "@/lib/svg";
import {
  uniqueStatesAndDistricts,
  technologies,
  workMode,
} from "@/feature/resources/constant/index";
import { paymentArray } from "@/feature/clients/constant";

const vendorFilterConfig = (accountManager: any) => {
  return {
    "Work Type": {
      id: 1,
      val: "selectedOptions",
      options: workMode,
      optionValue: "value",
    },
    "Payment Terms": {
      id: 2,
      val: "paymentTerms",
      options: paymentArray,
      optionValue: "value",
    },

    Location: {
      id: 3,
      val: "location",
      options: uniqueStatesAndDistricts,
      optionValue: "value",
      searchBar: {
        placeHolder: "Search Location",
      },
    },
    Technology: {
      id: 4,
      val: "technology",
      options: technologies,
      optionValue: "value",
      searchBar: {
        placeHolder: "Search technology",
      },
    },
    Manager: {
      val: "accountManager",
      id: 5,
      options: accountManager,
      optionValue: "name",
    },
  };
};

export default vendorFilterConfig;
