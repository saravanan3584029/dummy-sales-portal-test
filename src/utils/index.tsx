import Router from "next/router";
import {
  format,
  formatDistance,
  formatDistanceStrict,
  intervalToDuration,
  parse,
} from "date-fns";
import { SyntheticEvent } from "react";
import Icon from "@/lib/svg";

export const customizeEvent = (
  event: SyntheticEvent | Event,
  name: string,
  value: string | boolean | any[],
  text?: any
): void => {
  Object.defineProperty(event, "target", {
    writable: true,
    value: {
      name: name,
      value: text?.icon ? (
        <div
          className={`flex items-center gap-2`}
          style={{ color: `${text?.className}` }}
        >
          <Icon type={text?.icon?.type} fill={text?.icon?.fill} />
          {value}
        </div>
      ) : (
        value
      ),
    },
  });
};

export const isArrayOfObjects = (array: Array<any>) => {
  const isObject =
    Array.isArray(array) &&
    array?.every((el) => {
      if (typeof el === "object" && el !== null) {
        return true;
      } else {
        return false;
      }
    });
  return isObject;
};

let count = 0;

export const getError = (
  statusCode: number,
  message = ""
): Record<any, any> => {
  let errorMessage;
  let toasterCb;
  switch (statusCode) {
    case 500:
      errorMessage =
        "Oops! Something went wrong. Please try again after some time.";
      break;
    case 502:
    case 503:
    case 504:
      errorMessage =
        "We're currently experiencing technical difficulties. Please try again later.";
      break;
    case 400:
      errorMessage = "Bad Request";
      break;
    case 401:
      errorMessage = "Your session timed out. Please re-login.";
      toasterCb = () => {
        // logout(ROUTES.LOGIN.path);
      };
      break;
    case 404:
      errorMessage =
        "Sorry! No policy data was found for the entered details. Try again.";
      break;
    default:
      errorMessage =
        message ||
        "Oops! Something went wrong. Please try again after some time.";
  }
  return { errorMessage, toasterCb };
};

// export const fetchStrapiData: any = async (query, slug?: string) => {
//     const { data } = await client.query({
//         query: query,
//         variables: {
//             slug: slug,
//         },
//     });
//     return data;
// };

export const noOperation = (e?: Event | SyntheticEvent): void => {
  // no operation;
  if (e && typeof e.preventDefault === "function") e.preventDefault();
};

export const getFirstName = (name: string): string => {
  const regex = /(Mr|Mr.|MR|MR.|Ms|Ms.|Miss|Mrs|Dr|Sir)(\.?)\s/;
  const match = regex.exec(name);
  let n = "";
  if (match !== null) {
    n = name.replace(match[0], "").split(" ")[0];
  } else {
    n = name;
  }
  return n;
};

export const getNameInitials = (name: string): string => {
  let initials: any;
  const names = name?.split(" ");
  const regex = /Mr|Mr.|MR|MR.|Ms|Ms.|Miss|Mrs|Mrs.|Dr|Sir\s/;
  if (names) {
    const salutation = regex.exec(names[0]);
    const firstName = names[0] && salutation ? names[1] : names[0];
    const lastName = names.length > 1 ? names[names.length - 1] : "";
    initials =
      firstName && lastName
        ? firstName.charAt(0) + lastName.charAt(0)
        : firstName.charAt(0);
  }
  return initials;
};

export const getCurrentDevice = (): string => {
  if (window?.screen) {
    if (window.screen.width >= 1280) return "desktop";
    if (window.screen.width >= 768) return "laplet";
    return "mobile";
  }
  return "mobile";
};

export const calculateFullAge = (
  date: string,
  dateFormat?: string
): Record<any, any> => {
  const dateFmt = dateFormat || "dd/MM/yyyy";
  const birthDate = parse(date, dateFmt, new Date());
  const { years, months, days } = intervalToDuration({
    start: birthDate,
    end: new Date(),
  });
  return { years, months, days };
};

export const getSplitDate = (date: string, dateFormat = "dd/MM/yyyy"): any => {
  const dateVal = parse(date, dateFormat, new Date());
  const dateNum = `${format(dateVal, "do")}`;
  const month = `${format(dateVal, "MMM")}`;
  const year = `${format(dateVal, "yy")}`;
  return `${dateNum} ${month}' ${year}`;
};

export const isAsyncFn = (fn: Record<any, any>): boolean => {
  return (
    fn instanceof
    (async () => {
      /** * */
    }).constructor
  );
};

export const getFileExtension = (filename: any) => {
  return filename.split(".").pop();
};

export const repositionArrayElement = (
  arrayToUpdate: any[],
  elementToReposition: any,
  newIndex: number
): any[] => {
  if (
    arrayToUpdate.length === 1 ||
    arrayToUpdate.indexOf(elementToReposition) === newIndex ||
    newIndex > arrayToUpdate.length - 1 ||
    !arrayToUpdate.includes(elementToReposition)
  ) {
    return arrayToUpdate;
  }
  const array = [...arrayToUpdate];
  array.splice(array.indexOf(elementToReposition), 1);
  array.splice(newIndex, 0, elementToReposition);
  return array;
};

export const getMonthName = (monthNum: number): string => {
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  return months[monthNum];
};

export const dateAndTimeFormatter = (dateToFormat: Date): Record<any, any> => {
  const date = new Date(dateToFormat).getDate();
  const finalDate = date > 10 ? date : `0${date}`;
  const month = new Date(dateToFormat).getMonth();
  const formattedMonth = getMonthName(month);
  const year = new Date(dateToFormat).getFullYear();
  const formattedDate = `${finalDate} ${formattedMonth} ${year}`;
  let hours = new Date(dateToFormat).getHours();
  const mins = new Date(dateToFormat).getMinutes();
  const time = hours >= 12 ? "PM" : "AM";
  hours %= 12;
  const formattedHours = hours.toString().padStart(2, "0") || 12;
  const formattedMins = mins.toString().padStart(2, "0");
  const formattedTime = `${formattedHours}:${formattedMins} ${time}`;
  return { formattedTime, formattedDate };
};

export const getTimeDifference = (sourceDate: string) => {
  const daysDiff = formatDistanceStrict(new Date(sourceDate), new Date());
  return daysDiff;
};

export function formatNumberToString(n: number) {
  if (n >= 1000000) {
    return (n / 1000000).toFixed(1) + " M";
  } else if (n >= 100000) {
    return (n / 100000).toFixed(1) + " L";
  } else if (n >= 1000) {
    return (n / 1000).toFixed(1) + " k";
  } else {
    return n;
  }
}
export function formatNumberToCurrency(n: number) {
  const numString = n.toString();
  let formattedNumber = numString.replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
  return formattedNumber;
}

export const applyFiltersToData = (array: any, condition: any) => {
  let filterdData: any;

  condition.forEach((filter: any) => {
    filterdData = array?.filter((user: any) => {      
      if (Array.isArray(user[filter.key])) {
        const matches = user[filter.key].filter((entry: any) => {
          if (typeof entry === "object") {
            return entry.techStack.toLowerCase() === filter.value.toLowerCase();
          } else {
            return entry.includes(filter.value);
          }
        });
        return matches.length > 0;
      } else if (typeof user[filter.key] === "number") {
        return (
          user[filter.key] >= filter.value.minimum &&
          user[filter.key] <= filter.value.maximum
        );
      }else if(typeof user[filter.key] === "object"){        
        return user[filter.key][filter.key].toLowerCase() === filter.value.toLowerCase();
      }
      else {
        return user[filter.key].toLowerCase() === filter.value.toLowerCase();
      }
    });
  });
  return filterdData;
};

export const base64ToBlob = (base64: string, contentType: string) => {
  const byteCharacters = atob(base64);
  const byteNumbers = new Array(byteCharacters.length);
  for (let i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i);
  }
  const byteArray = new Uint8Array(byteNumbers);
  return new Blob([byteArray], { type: contentType });
};

export const downloadFiles = (blob: any, fileName: any) => {
  const blobUrl = URL.createObjectURL(blob);
  const link = document.createElement("a");
  link.href = blobUrl;
  link.download = fileName;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

//initial letter will get get Capital
export const letterFormater = (value: any) => {
  let result = value?.charAt(0)?.toUpperCase() + value?.slice(1) || "";
  return result;
};

export const areEqual = (data1: any, data2: any) => {
  return JSON.stringify(data1) === JSON.stringify(data2);
};

export const getItemFromLocalStorage = (key: string) => {
  let item;
  item = typeof window !== "undefined" ? localStorage.getItem(key) : "";
  return item ? item : "";
};

export const clearLocalStorage = () => {
  if (typeof window !== "undefined") {
    localStorage.clear();
  }
};
