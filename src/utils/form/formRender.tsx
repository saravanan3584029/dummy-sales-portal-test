"use client";
import { useEffect, useState } from "react";
import Form from "../../shared-components/ui/Form";
import { useValidation } from "../validation/hooks";
import Button from "@/shared-components/ui/Button";
import { useDispatch } from "react-redux";
import Link from "next/link";
import Icon from "@/lib/svg";
import { NestedForm } from "@/shared-components/components/NestedForm";

const FormRender = ({
  formHeading,
  formLink,
  formData,
  formColumn,
  formLabelStyle = "text-base mb-4",
  headingStyle,
  formStyle,
  btnConfig,
  btnStyle,
  formLinkStyle,
  initialStaticData,
  initialNestedData,
  validations,
  updatedState,
  customUpdatedState,
  autoCalculatedVals,
  resetFormHandler,
  submitForm,
  className,
  showNoLabel = false,
  formLine = false,
}: {
  formHeading?: string;
  formLink?: any;
  formColumn?: number;
  formLinkStyle?: any;
  formData: any;
  formLabelStyle?: string;
  formStyle?: string;
  headingStyle?: string;
  initialStaticData?: any;
  initialNestedData?: any;
  btnStyle?: boolean;
  btnConfig?: any;
  formKeyDependent?: string;
  validations?: any;
  updatedState?: any;
  customUpdatedState?: any;
  autoCalculatedVals?: any;
  acknowledgementData?: any;
  formResponse?: any;
  editForm?: boolean;
  submitForm?: any;
  resetFormHandler?: () => any;
  getFormVals?: (_: any) => any;
  showNoLabel?: boolean;
  formLine?: boolean;
  className?: string;
}) => {
  const [updatedData, setUpdatedData] = useState<any>(formData);
  const [nestedFormValues, setNestedFormValues] = useState<any>();
  const [nestedFormData, setNestedFormData] = useState<any>([]);

  const dispatch = useDispatch();

  useEffect(() => {
    setUpdatedData(formData);
  }, [formData]);

  useEffect(() => {
    if (initialNestedData?.contactPersons) {
      setNestedFormData(initialNestedData?.contactPersons);
    }
  }, [updatedData]);

  const visibilityArr: any = [];

  Object.values(updatedData).forEach((input: any) => {
    if (input.elementType !== "ComponentFormButton") {
      if (input.visible === false) {
        visibilityArr.push(input.name);
      }
    }
  });
  let updatedValues;
  const [
    errors,
    values,
    touched,
    fieldDomRefs,
    markAllTouched,
    handleChange,
    handleBlur,
    resetForm,
    validateFormData,
    setValues,
    setErrors,
    updateFormErrors,
  ] = useValidation(
    initialStaticData,
    validations,
    visibilityArr,
    autoCalculatedVals,
    setNestedFormValues,
    nestedFormValues,
    setNestedFormData,
    nestedFormData
  );

  // const markFieldsTouched = () => {
  //     const touchedData = {};
  //     setFieldTouched(touchedData);
  // };

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    setErrors({});
    markAllTouched();
    // markFieldsTouched();
    let isValid = await validateFormData();
    if (!isValid) {
      return;
    }
    if (submitForm) {
      let res;
      const filteredValues = Object.fromEntries(
        Object.entries(values).filter(
          ([key, value]) =>
            value !== "" && !(Array.isArray(value) && value.length === 0)
        )
      );
      if (nestedFormData?.length > 0) {
        updatedValues = { ...filteredValues, contactPersons: nestedFormData };
        res = await submitForm(updatedValues);
      } else {
        res = await submitForm(filteredValues);
      }
      if (!res?.errors) {
        resetForm();
      }
    }
  };

  const resetDataHandler = async () => {
    if (resetFormHandler) {
      await resetFormHandler();
      await resetForm();
    } else {
      await resetForm();
    }
  };

  const formRender = (
    formDataList: any,
    formColumn: Number,
    formFieldsLabel: String,
    index: number
  ) => {
    const visibilityHandler = () => {
      if (formDataList?.dependencies) {
        formDataList.dependencies.forEach((item: any) => {
          let filterField: any = () =>
            Object.values(updatedData).filter((el: any) => {
              return el.name === item.field;
            });
          const fieldToBeHidden = filterField();
          if (
            values[formDataList?.name] &&
            Array.isArray(item?.values) &&
            !item.values.includes(values[formDataList.name])
          ) {
            fieldToBeHidden[0].visible = false;
            visibilityArr.push(fieldToBeHidden[0].name);
          }
        });
      }
    };
    visibilityHandler();

    let form = (
      <div className={`grid-style`}>
        {Object.keys(formDataList).map((formEle: any, index: any) => {
          if (formDataList[formEle].elementType == "NestedForm") {
            return (
              <div key={index} className={`${formDataList[formEle].className}`}>
                <NestedForm
                  formData={formDataList[formEle].content}
                  btnConfig={formDataList[formEle].btnConfig}
                  handleChange={handleChange}
                  formValues={values}
                  touched={touched}
                  handleBlur={handleBlur}
                  errors={errors}
                  updatedData={formData}
                  setUpdatedData={setUpdatedData}
                  fieldDomRefs={fieldDomRefs}
                  handleSubmit={handleSubmit}
                  nestedFormValues={nestedFormValues}
                  nestedFormData={nestedFormData}
                  setNestedFormData={setNestedFormData}
                />
              </div>
            );
          } else {
            return (
              <div key={index} className={`${formDataList[formEle].className}`}>
                <div> {formEle}</div>
                {!formDataList[formEle].Feild ? (
                  <Form
                    formData={formDataList[formEle]}
                    handleChange={handleChange}
                    formValues={values}
                    touched={touched}
                    handleBlur={handleBlur}
                    errors={errors}
                    updatedData={formData}
                    setUpdatedData={setUpdatedData}
                    fieldDomRefs={fieldDomRefs}
                  />
                ) : (
                  <>
                    {formDataList[formEle]?.Feild.startText ? (
                      <div className="flex flex-row gap-2 items-center text-sm">
                        {formDataList[formEle]?.Feild.startText}
                        <div className="border-2 rounded-lg p-1 border-b-4">
                          <Icon type="star" />
                        </div>
                        {formDataList[formEle]?.Feild.endText}
                      </div>
                    ) : (
                      <div className="flex items-center">
                        {Object.keys(formDataList[formEle]?.Feild).map(
                          (ele: any, index: any) => {
                            return (
                              <div key={index} className="mr-2">
                                <div> {ele}</div>
                                <div className="flex items-center justify-center">
                                  <Form
                                    formData={formDataList[formEle]?.Feild[ele]}
                                    handleChange={handleChange}
                                    formValues={values}
                                    touched={touched}
                                    handleBlur={handleBlur}
                                    errors={errors}
                                    updatedData={formData}
                                    setUpdatedData={setUpdatedData}
                                    fieldDomRefs={fieldDomRefs}
                                  />
                                </div>
                              </div>
                            );
                          }
                        )}
                      </div>
                    )}
                  </>
                )}
              </div>
            );
          }
        })}
      </div>
    );

    return (
      <div
        className={`${formDataList?.visible === false && "hidden"}  mt-10`}
        key={index}
      >
        {form}
      </div>
    );
  };

  return (
    <div className="flex flex-col">
      <div style={{ order: btnStyle ? 2 : 0 }}>
        {(formHeading || formLink || btnConfig) && (
          <div className="flex justify-between items-center">
            <div className={`flex justify-between items-center`}>
              <div className="flex gap-12 items-center">
                {formHeading && (
                  <div className={`font-bold text-xl ${headingStyle}`}>
                    {formHeading}
                  </div>
                )}

                {formLink && (
                  <div
                    className={`flex flex-row justify-evenly items-center gap-4 text-m font-medium`}
                  >
                    {Object.values(formLink).map((link: any, index) => {
                      return (
                        <div key={index} className={`${formLinkStyle}`}>
                          <Link
                            href={link.path}
                            className=" flex justify-evenly items-center gap-2 "
                          >
                            {link.icon && (
                              <Icon
                                type={link.icon.type}
                                width={link.icon.width}
                                height={link.icon.height}
                                fill={link.icon.fill}
                              />
                            )}
                            <div>{link.labelName}</div>
                          </Link>
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
            </div>
            <div className="flex justify-around gap-3">
              {btnConfig?.map((btn: any, index: number) => (
                <Button
                  type={btn.type}
                  size={btn.size}
                  onClick={
                    btn.type === "reset"
                      ? resetDataHandler
                      : btn.onClick || handleSubmit
                  }
                  variant={btn.variant}
                  className={btn.className}
                  precedingText={btn.precedingText}
                  loading={btn.loading}
                  disabled={btn.disabled}
                  key={index}
                >
                  {btn.children}
                </Button>
              ))}
            </div>
          </div>
        )}
      </div>
      {formLine && <hr className="mt-10" />}

      <form
        noValidate
        className={`${formStyle}`}
        style={{ order: btnStyle ? 0 : 1 }}
      >
        {Object.keys(updatedData).map((formFieldsLabel, index) => {
          return (
            <div key={index}>
              {formRender(
                updatedData[formFieldsLabel]?.config,
                updatedData[formFieldsLabel]?.column,
                formFieldsLabel,
                index
              )}
              <hr className={`${updatedData[formFieldsLabel]?.class}`} />
            </div>
          );
        })}
      </form>
      <style>{`
      .grid-style{
        display: grid;
        grid-template-columns:repeat(${formColumn || 1}, 1fr);
        grid-column-gap: 4.5rem;
        grid-row-gap: ${formColumn && (formColumn > 1 ? "2rem" : "1rem")};

      }
    
      .grid-style:nth-child(n-1){
        margin-bottom:${formColumn && formColumn > 1 && "2rem"};
      }

      @media screen and (max-width: 768px) {
        .grid-style {
          grid-template-columns: repeat(1, 1fr);
        }`}</style>
    </div>
  );
};

export default FormRender;
