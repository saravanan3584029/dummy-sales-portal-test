import { multiselect } from "@/feature/resources/config/types";
import validationUtils from "./validationUtils";

const errorObj = (key: string, message: string, formErrors: any) => {
  formErrors[key] = message;
};

export const validationLogic = (
  key: string,
  value: string,
  validationRules: any,
  formErrors: any,
  values: any
) => {
  if (
    validationRules &&
    validationRules[key]?.required &&
    validationUtils.isEmpty(value)
  ) {
    errorObj(key, validationRules[key]?.required.message, formErrors);
  }

  if (
    !validationUtils.isEmpty(value) &&
    validationRules &&
    validationRules[key]?.pattern
  ) {
    switch (validationRules[key]?.pattern.type) {
      case "email":
        if (!validationUtils.isEmail(value)) {
          errorObj(key, validationRules[key]?.pattern.message, formErrors);
        }
        break;
      case "name":
        if (!validationUtils.isName(value)) {
          errorObj(key, validationRules[key]?.pattern.message, formErrors);
        }
        break;
      case "mobile":
        if (!validationUtils.isMobile(value)) {
          errorObj(key, validationRules[key]?.pattern.message, formErrors);
        }
        break;
      case "positiveNonzeroFloat":
        if (!validationUtils.isPositiveNonzeroFloat(value)) {
          errorObj(key, validationRules[key]?.pattern.message, formErrors);
        }
        break;
      case "positiveFloat":
        if (!validationUtils.isPositiveFloat(value)) {
          errorObj(key, validationRules[key]?.pattern.message, formErrors);
        }

        break;

      default:
        return null;
    }
  }
  if (
    !validationUtils.isEmpty(value) &&
    validationRules &&
    validationRules[key]?.customValidation
  ) {
    switch (validationRules[key]?.customValidation.type) {
      case "isDuplicate":
        if (
          !validationUtils.isDuplicate(
            value,
            values[validationRules[key]?.customValidation.fieldToBeCompared]
          )
        ) {
          errorObj(
            key,
            validationRules[key]?.customValidation.message,
            formErrors
          );
        }
        break;
      default:
        return null;
    }
  }
};

export const validateFormField = (
  fieldName: string,
  fieldValue: any,
  validationRules: any,
  values?: any
) => {
  const fieldErrors: any = {};
  if (validationRules && validationRules?.required) {
    if (validationUtils.isEmpty(fieldValue)) {
      fieldErrors[fieldName] = validationRules.required.message;
    }
  }
  if (
    !validationUtils.isEmpty(fieldValue) &&
    validationRules &&
    validationRules?.pattern
  ) {
    switch (validationRules?.pattern.type) {
      case "email":
        if (!validationUtils.isEmail(fieldValue)) {
          fieldErrors[fieldName] = validationRules.pattern.message;
        }
        break;
      case "name":
        if (!validationUtils.isName(fieldValue)) {
          fieldErrors[fieldName] = validationRules.pattern.message;
        }
        break;
      case "mobile":
        if (!validationUtils.isMobile(fieldValue)) {
          fieldErrors[fieldName] = validationRules.pattern.message;
        }
        break;
      case "positiveNonzeroFloat":
        if (!validationUtils.isPositiveNonzeroFloat(fieldValue)) {
          fieldErrors[fieldName] = validationRules.pattern.message;
        }
        break;
      case "positiveFloat":
        if (!validationUtils.isPositiveFloat(fieldValue)) {
          fieldErrors[fieldName] = validationRules.pattern.message;
        }
      case "isPrimary":
        if (!validationUtils.isPrimaryField(fieldValue)) {
          fieldErrors[fieldName] = validationRules.pattern.message;
        }
        break;
      default:
        return null;
    }
  }
  if (
    !validationUtils.isEmpty(fieldValue) &&
    validationRules &&
    validationRules?.customValidation
  ) {
    // switch (validationRules?.customValidation.type) {
    //   case "isDependent":
    //     if (
    //       !validationUtils.isDependentFieldFilled(
    //         validationRules?.customValidation.dependentOn,
    //         values[validationRules?.customValidation.dependentOn]
    //       )
    //     ) {
    //       fieldErrors[fieldName] = validationRules.customValidation.message;
    //     }
    //     break;
    //   case "notFutureDate":
    //     if (!validationUtils.isNotFutureDate(fieldValue)) {
    //       fieldErrors[fieldName] = validationRules.customValidation.message;
    //     }
    //     break;
    //   case "isDuplicate":
    //     if (
    //       !validationUtils.isDuplicate(
    //         fieldValue,
    //         values[validationRules?.customValidation.fieldToBeCompared]
    //       )
    //     ) {
    //       fieldErrors[fieldName] = validationRules.customValidation.message;
    //     }
    //     break;
    //   case "isAfterStartDate":
    //     if (
    //       validationRules?.customValidation.isStartDate &&
    //       values[validationRules?.customValidation.dateToCompare]
    //     ) {
    //       if (
    //         !validationUtils.isAfterStartDate(
    //           fieldValue,
    //           values[validationRules?.customValidation.dateToCompare]
    //         )
    //       ) {
    //         fieldErrors[validationRules?.customValidation.dateToCompare] =
    //           validationRules.customValidation.message;
    //       } else {
    //         fieldErrors[validationRules?.customValidation.dateToCompare] = "";
    //       }
    //     } else if (values[validationRules?.customValidation.dateToCompare]) {
    //       if (
    //         !validationUtils.isAfterStartDate(
    //           values[validationRules?.customValidation.dateToCompare],
    //           fieldValue
    //         )
    //       ) {
    //         fieldErrors[fieldName] = validationRules.customValidation.message;
    //       }
    //     }
    //     break;
    //   default:
    //     return null;
    // }
  }
  return fieldErrors;
};

export const validateForm = (
  values: any,
  validationRules: any,
  visibilityArr: any
) => {
  let formErrors: any = {};
  Object.keys(values).map((key) => {
    if (!visibilityArr?.includes(key)) {
      const value = values[key];
      let arr = [];
      const valueArr: Array<any> = [];
      let fieldErr: any = {};
      if (Object.keys(fieldErr).length > 0) {
        Object.keys(fieldErr).map((el) => {
          const trueValuesArr = valueArr.filter((value) => value === true);
          const arrIndex = values[key].length - 1;
          if (trueValuesArr.length > 1) {
            formErrors[key][arrIndex][el] = fieldErr[el].duplicateTrue;
          } else if (trueValuesArr.length === 0) {
            formErrors[key][arrIndex][el] = fieldErr[el].allFalse;
          }
        });
      }
      validationLogic(key, value, validationRules, formErrors, values);
    }
  });
  const isEmpty = Object.keys(formErrors).every((key) => {
    return formErrors[key] === "";
  });
  if (isEmpty) {
    formErrors = {};
  }
  return formErrors;
};
