const validationMessages: Record<string, string> = {
  isEmpty: "This field is required",
  isEmail: "Please enter a valid email id",
  isNumber: "Please enter positive numeric values only",
  isNonzeroNumber: "Please enter numeric values greater than zero",
  isText: "Please enter characters only",
  isMobile: "Please enter a valid mobile number",
  isOrganisation: "Please enter a valid name",
  isEmployeeId: "Please enter a valid employee id",
  isPassword: "Please enter a valid password",
  isNotFutureDate: "Please enter a present or past date",
  isDuplicate: "Both fields should have the same value",
  isAfterStartDate: "The end date should come after start date",
  isNotPrimary: "Please mark atleast one primary skills",
  isNotPrimaryContact: "Please create atleast one primary contact",
};

export const isDependentFieldFilledMessage = (fieldName: string): string => {
  return `Please fill ${fieldName} first`;
};
export default validationMessages;
