import Joi from "joi";
import {
  MOBILE,
  PASSWORD,
  TEXT,
  ID
} from "./ValidationUtils";
export {};

const search = Joi.string().trim().default("").allow("").optional();



const requiredId = Joi.string().trim().hex().length(ID.LENGTH).required();

const optionalId = Joi.string()
  .trim()
  .hex()
  .length(ID.LENGTH)
  .default(null)
  .optional();

const pass = Joi.string()
  .regex(PASSWORD.REGEXP)
  .message(PASSWORD.MSG)
  .min(PASSWORD.MINCHAR)
  .required();


const emailValidate = Joi.string()
  .email()
  .trim()
  .lowercase()
  .options({ convert: true })
  .required();

const mobile = Joi.string()
  .regex(MOBILE.REGEXP)
  .message(MOBILE.MSG)
  .length(MOBILE.LENGTH)
  .required();

const text = Joi.string().regex(TEXT.REGEXP).message(TEXT.MSG).required();


export {
  search,
  requiredId,
  optionalId,
  pass,
  emailValidate,
  mobile,
  text,
};
