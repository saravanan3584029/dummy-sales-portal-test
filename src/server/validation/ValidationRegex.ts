const VALIDATION_REGEX = {
  EMAIL_REGEX: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,}$/i,
  PASSWORD_REGEX:
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
  TEXT_REGEX: /^[a-zA-Z\s]+$/,
  EMPTY_STRING_REGEX: /^\s*$/,
  MOBILE_REGEX: /^\d{10}$/,
  FLOAT_REGEX: /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/,
  NUMBER_REGEX: /^(0|[1-9][0-9]*)$/,

};

export default VALIDATION_REGEX;
