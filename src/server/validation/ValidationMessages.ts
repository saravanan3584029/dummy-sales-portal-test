export const VALIDATION_MESSAGE = {
  NAME_REQUIRED: "Resource name is required.",
  VENDOR_NAME_REQUIRED: "Vendor name is required.",
  STATUS_REQUIRED: "Status is required.",
  PASSWORD_INVALID:
    "password must be minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.",
  EMAIL_REQUIRED: "email is required.",
  EMAIL_INVALID: "please enter a valid email.",
  PHONE_INVALID: "Phone number must be a valid 10-digit number.",
  TEXT_INVALID: "This field must only contain letters and spaces.",
  NUMBER_INVALID: "Please provide the information in a numeric format.",
  COSTING_REQUIRED: "Costing is required.",
  EXPERIENCE_REQUIRED: "Experience is required.",
  ACCOUNT_MANAGER_REQUIRED: "Account manager is required.",
  LOCATION_REQUIRED: "Location is required.",
  WORK_MODE_REQUIRED: "Work mode is required",
  CLIENT_NAME_REQUIRED: "Client name is required.",
  BUDGET_MIN_MAX_REQUIRED:
    "Please specify a both minimum and maximum Budget for a requirement. ",
  EXPERIENCE_MIN_MAX_REQUIRED:
    "Please specify a both minimum and maximum experience of a requirement ",
  CONTRACT_DURATION_REQUIRED: "Contract Duration is required ",
  CONTACT_PERSON_REQUIRED: "Contact person name  is required ",
  NUMBER_OF_POSITIONS_REQUIRED: "Number of position opening is required",
  TYPE_REQUIRED: "Type is required",
};

export const SUCCESS_MESSAGE = {
  COMMENT_ADDED: "Comment has added successfully",
  RESOURCE_CREATED: "Resource has been created successfully",
  VENDOR_CREATED: "Resource has been created successfully",
  VENDOR_UPDATED: "Vendor has been updated successfully",
  VENDOR_DELETED: "Vendor has been deleted successfully",
  RESOURCE_DELETED: "Resource has been deleted successfully",
  RESOURCE_UPDATED: "Resource has been updated successfully",
  REQUIREMENT_CREATED: "Requirement has been added successfully",
  REQUIREMENT_UPDATED: "Requirement has been updated successfully",
  RECORD_DELETED: "Record has been deleted successfully",
  CLIENT_CREATED: "Client has been created successfully",
  CLIENT_DELETED: "Client has been deleted successfully",
  CLIENT_UPDATED: "Client has been updated successfully",
};
