import mongoose from "mongoose";
import FileUpload from "../models/fileUpload";
import ndaFile from "@/server/models/ndaFile";
import Requirement from "../models/requirement";
import { NextResponse } from "next/server";

function getContentTypeFromExtension(extension: String) {
  switch (extension) {
    case "pdf":
      return "application/pdf";
    case "docx":
      return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    case "doc":
      return "application/msword";
    case "jpg":
    case "jpeg":
      return "image/jpeg";
    case "png":
      return "image/png";
    case "gif":
      return "image/gif";
    default:
      return null;
  }
}

const generateUniquetId = async (Modal: any, baseId: string) => {
  try {
    let number = 1;
    let uniqueId = `${baseId}${String(number).padStart(3, "0")}`;
    while (await Modal.exists({ reqId: uniqueId })) {
      number++;
      uniqueId = `${baseId}${String(number).padStart(3, "0")}`;
    }

    return uniqueId;
  } catch (error) {}
};

const fileUploadToGridFs = async (Modal: any, file: any, feildName: string) => {
  try {
    let fileupload = new Modal({
      filename: "",
      [feildName]: null,
      size: 0,
    });
    const bytes = await file.arrayBuffer();
    const buffer = Buffer.from(bytes);
    fileupload.filename = file.name;
    fileupload.size = file.size;
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db);
    const uploadStream = gfs.openUploadStream(file.name);
    const uploadPromise = new Promise((resolve, reject) => {
      uploadStream.end(buffer, (error, result) => {
        if (error) {
          reject(error);
        } else {
          resolve(result ? result._id.toString() : "");
        }
      });
    });
    const fileReference = await uploadPromise;
    fileupload[feildName] = fileReference;
    const fileInfo = await fileupload.save();
    return fileInfo;
  } catch (error) {
    return NextResponse.json({ error, status: 500 });
  }
};

export { getContentTypeFromExtension, generateUniquetId, fileUploadToGridFs };
