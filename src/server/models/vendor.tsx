import { PAYMENT_TERMS } from "@/constants/constant";
import { VALIDATION_MESSAGE } from "../validation/ValidationMessages";
import { ERROR_MESSAGE } from "@/constants/errorMessage";
import Resource from "./resource";

const mongoose = require("mongoose");

const vendorSchema = new mongoose.Schema({
  vendorName: {
    type: String,
    required: [true, VALIDATION_MESSAGE.NAME_REQUIRED],
  },
  selectedOptions: {
    type: [String],
    require: [true, VALIDATION_MESSAGE.TYPE_REQUIRED],
  },
  location: {
    type: String,
    require: [true, VALIDATION_MESSAGE.LOCATION_REQUIRED],
  },
  resourceCount: {
    type: Number,
    default: 0, // Set the default value to 0
  },
  paymentTerms: {
    type: String,
    require: [true, VALIDATION_MESSAGE.STATUS_REQUIRED],
    enum: {
      values: Object.values(PAYMENT_TERMS),
      message: ERROR_MESSAGE.PAYMENT_TERMS_INVALID,
    },
  },
  ndaFile: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "NdaFile",
    require: true,
  },
  contactPersons: {
    type: [
      {
        name: String,
        phoneNumber: String,
        email: String,
        designation: String,
        isPrimary: Boolean,
      },
    ],
    default: [],
  },
  accountManager: {
    type: [String],
    require: [true, VALIDATION_MESSAGE.ACCOUNT_MANAGER_REQUIRED],
  },
  notes: [
    {
      message: String,
      publishedBy: String,
      publishedAt: Date,
      reply: [{ message: String, repliedBy: String, repliedAt: Date }],
    },
  ],
  resources: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: Resource.modelName, // Reference to the Resource model
    },
  ],
});

vendorSchema.set("timestamps", true);
const Vendor = mongoose.models.Vendor || mongoose.model("Vendor", vendorSchema);

export default Vendor;
