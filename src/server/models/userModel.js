const mongoose = require("mongoose");
mongoose.models = {};
mongoose.modelSchemas = {};


const ClientDetailsSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    index: true,
    unique: true,
    dropDups: true,
  },
  email: { type: String, default: "", required: true },
  password: { type: String, default: "", required: true },
  userType: { type: String, default: "user" },
  vendorList: [
    {
      searchId: { type: mongoose.Schema.Types.ObjectId, ref: "searchs" },
      vendorName:{type: mongoose.Schema.Types.String, ref: "searchs" },
      createdAt:{type: Date, default:Date.now},
      updatedAt:{type: Date, default:Date.now},
    },
  ],
  resourceList: [
    {
      searchId: { type: mongoose.Schema.Types.ObjectId, ref: "searchs" },
      resourceName:{type: mongoose.Schema.Types.String, ref: "searchs" },
      createdAt:{type: Date, default:Date.now},
      updatedAt:{type: Date, default:Date.now},
    },
  ],
  clientList: [
    {
      searchId: { type: mongoose.Schema.Types.ObjectId, ref: "searchs" },
      clientName:{type: mongoose.Schema.Types.String, ref: "searchs" },
      createdAt:{type: Date, default:Date.now},
      updatedAt:{type: Date, default:Date.now},
    },
  ],
  requirementList: [
    {
      searchId: { type: mongoose.Schema.Types.ObjectId, ref: "searchs" },
      requirementName:{type: mongoose.Schema.Types.String, ref: "searchs" },
      createdAt:{type: Date, default:Date.now},
      updatedAt:{type: Date, default:Date.now},
    },
  ],
  
});

ClientDetailsSchema.set("timestamps", true);
module.exports = mongoose.model("user", ClientDetailsSchema);
