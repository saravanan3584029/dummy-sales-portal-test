import { ERROR_MESSAGE } from "@/constants/errorMessage";
import mongoose, { Schema } from "mongoose";
import { STATUS, WORK_MODE } from "@/constants/constant";
import { emailValidate, mobile, text } from "@/server/validation/common";
import { VALIDATION_MESSAGE } from "@/server/validation/ValidationMessages";

const resourceSchema = new Schema({
  name: {
    type: String,
    required: [true, VALIDATION_MESSAGE.NAME_REQUIRED],
    validate: {
      validator: function (value: string) {
        const { error } = text.validate(value);
        if (error) {
          throw new Error(error.message);
        }
      },
    },
  },
  vendorName: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Vendor", // Reference to the Vendor model
  },

  status: {
    type: String,
    require: [true, VALIDATION_MESSAGE.STATUS_REQUIRED],
    enum: {
      values: Object.values(STATUS),
      message: ERROR_MESSAGE.STATUS_INVALID,
    },
  },
  phone: {
    type: String,
    unique: true,
    require: [true, VALIDATION_MESSAGE.PASSWORD_INVALID],
    validate: {
      validator: (value: String) => {
        const { error } = mobile.validate(value);
        if (error) {
          throw new Error(error.message);
        }
      },
    },
  },
  email: {
    type: String,
    unique: true,
    require: [true, VALIDATION_MESSAGE.EMAIL_INVALID],
    validate: {
      validator: (value: String) => {
        const { error } = emailValidate.validate(value);
        if (error) {
          throw new Error(error.message);
        }
      },
    },
  },
  costing: {
    type: Number,
    require: [true, VALIDATION_MESSAGE.COSTING_REQUIRED],
  },
  experience: {
    type: Number,
    require: [true, VALIDATION_MESSAGE.EXPERIENCE_REQUIRED],
  },
  location: {
    type: String,
    require: [true, VALIDATION_MESSAGE.LOCATION_REQUIRED],
  },
  workMode: {
    type: String,
    require: [true, VALIDATION_MESSAGE.WORK_MODE_REQUIRED],
    enum: {
      values: Object.values(WORK_MODE),
      message: ERROR_MESSAGE.WORK_MODE_INVALID,
    },
  },
  resume: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "FileUpload",
  },
  technology: [
    {
      techStack: String,
      isPrimary: Boolean,
    },
  ],
  accountManager: {
    type: [String],
    require: [true, VALIDATION_MESSAGE.ACCOUNT_MANAGER_REQUIRED],
  },

  notes: [
    {
      message: String,
      publishedBy: String,
      publishedAt: Date,
      reply: [{ message: String, repliedBy: String, repliedAt: Date }],
    },
  ],
});
resourceSchema.set("timestamps", true);

const Resource =
  mongoose.models.Resource || mongoose.model("Resource", resourceSchema);

export default Resource;
