import Requirement from "./requirement";

const mongoose = require("mongoose");
mongoose.models = {};
mongoose.modelSchemas = {};

const clientSchema = new mongoose.Schema({
  clientName: String,
  location: String,
  resourceCount: {
    type: Number,
    default: 0, // Set the default value to 0
  },
  paymentTerms: String,
  ndaFile: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "NdaFile", // Reference to the NdaFile model
  },
  contactPersons: {
    type: [
      {
        name: String,
        phoneNumber: String,
        email: String,
        designation: String,
      },
    ],
    default: [],
  },
  accountManager: [String],
  requirements: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: Requirement.modelName, // Reference to the Resource model
    },
  ],

  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user", // Reference to the User model
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  notes: [
    {
      message: String,
      publishedBy: String,
      publishedAt: Date,
      reply: [{ message: String, repliedBy: String, repliedAt: Date }],
    },
  ],
});

const Client = mongoose.models.Client || mongoose.model("Client", clientSchema);

export default Client;
