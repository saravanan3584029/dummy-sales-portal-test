import { ERROR_MESSAGE } from "@/constants/errorMessage";
import mongoose, { Schema } from "mongoose";
import { WORK_MODE } from "@/constants/constant";
import { VALIDATION_MESSAGE } from "@/server/validation/ValidationMessages";

const requirementSchema = new Schema({
  reqId: {
    type: String,
    required: true,
    unique: true,
  },
  clientName: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client", // Reference to the Client model
  },
  description: {
    type: String,
  },
  status: {
    type: String,
    default: "Open",
  },
  priority: {
    type: String,
    default: "No Priority",
  },

  budget: {
    minimum: {
      type: Number,
      require: [true, VALIDATION_MESSAGE.BUDGET_MIN_MAX_REQUIRED],
    },
    maximum: {
      type: Number,
      require: [true, VALIDATION_MESSAGE.BUDGET_MIN_MAX_REQUIRED],
    },
  },

  experience: {
    minimum: {
      type: Number,
      require: [true, VALIDATION_MESSAGE.EXPERIENCE_MIN_MAX_REQUIRED],
    },
    maximum: {
      type: Number,
      require: [true, VALIDATION_MESSAGE.EXPERIENCE_MIN_MAX_REQUIRED],
    },
  },
  contractDuration: {
    type: String,
    require: [true, VALIDATION_MESSAGE.CONTRACT_DURATION_REQUIRED],
  },
  openPositions: {
    type: Number,
    require: [true, VALIDATION_MESSAGE.NUMBER_OF_POSITIONS_REQUIRED],
  },

  location: {
    type: String,
    require: [true, VALIDATION_MESSAGE.LOCATION_REQUIRED],
  },

  workMode: {
    type: String,
    require: [true, VALIDATION_MESSAGE.WORK_MODE_REQUIRED],
    enum: {
      values: Object.values(WORK_MODE),
      message: ERROR_MESSAGE.WORK_MODE_INVALID,
    },
  },
  attachment: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "FileUpload",
  },
  technology: [
    {
      techStack: String,
      isPrimary: Boolean,
    },
  ],
  accountManager: {
    type: [String],
    require: [true, VALIDATION_MESSAGE.ACCOUNT_MANAGER_REQUIRED],
  },
  contactPerson: {
    type: [String],
    require: [true, VALIDATION_MESSAGE.CONTACT_PERSON_REQUIRED],
  },

  notes: [
    {
      message: String,
      publishedBy: String,
      publishedAt: Date,
      reply: [{ message: String, repliedBy: String, repliedAt: Date }],
    },
  ],
});
requirementSchema.set("timestamps", true);

const Requirement =
  mongoose.models.Requirement ||
  mongoose.model("Requirement", requirementSchema);

export default Requirement;
