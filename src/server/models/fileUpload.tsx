import mongoose from "mongoose";

const fileUploadSchema = new mongoose.Schema({
  filename: String,
  fileReference: String,
  size: {
    type: Number,
    default: 0,
  },
});

fileUploadSchema.set("timestamps", true);
const FileUpload = mongoose.models.FileUpload || mongoose.model("FileUpload", fileUploadSchema);
export default FileUpload;
