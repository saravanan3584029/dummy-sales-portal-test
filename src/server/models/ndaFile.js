const mongoose = require("mongoose");
mongoose.models = {};
mongoose.modelSchemas = {};


const NdaFileSchema = new mongoose.Schema({
    filename: String,
    ndaFileReference: String,
    size:{
        type: Number,
        default: 0
    } // Store the file reference ID
    // Add any other fields related to the NDA file here
  });
  
  // Define a model for the NDA file

  NdaFileSchema.set("timestamps", true);
module.exports = mongoose.model('NdaFile', NdaFileSchema);
