import type { Config } from "tailwindcss";

const config: Config = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      textColor: {
        primary: "var(--color-text-primary)",
      },
      fontSize: {
        "2xs": ["var(--size-font-2xs)", "var(--line-height-2xs)"],
        xs: ["var(--size-font-xs)", "var(--line-height-xs)"],
        sm: ["var(--size-font-sm)", "var(--line-height-sm)"],
        m: ["var(--size-font-m)", "var(--line-height-m)"],
        base: ["var(--size-font-base)", "var(--line-height-base)"],
        lg: ["var(--size-font-lg)", "var(--line-height-lg)"],
        xl: ["var(--size-font-xl)", "var(--line-height-xl)"],
        "2xl": ["var(--size-font-2xl)", "var(--line-height-2xl)"],
      },
      lineHeight: {
        "2xs": "var(--line-height-2xs)",
        xs: "var(--line-height-xs)",
        sm: "var(--line-height-sm)",
        base: "var(--line-height-base)",
        lg: "var(--line-height-lg)",
        xl: "var(--line-height-xl)",
        "2xl": "var(--line-height-2xl)",
      },
      fontFamily: {
        "family-Greycliff": "var(--font-family-Greycliff)",
        "family-Inter": "var(--font-family-Inter)",
      },
    },
  },
  variants: {},
  plugins: [],
};
export default config;
